// eslint-disable-next-line @typescript-eslint/no-var-requires
const babel = require('@babel/core');
const cwd = process.cwd();
// eslint-disable-next-line @typescript-eslint/no-var-requires
const presetEnv = require('@babel/preset-env');

const replaceResolveAlias = function () {
    return {
        visitor: {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            ImportDeclaration: function (path, source) {
                var importDeclareValue = path.node.source.value;
                path.node.source.value = String(
                    importDeclareValue == null ? '' : importDeclareValue,
                ).replace(/[~@]\//, '/');
            },
        },
    };
};
const toOptions = function (options) {
    const envPreset = [
        [
            presetEnv,
            {
                targets: { chrome: 73 },
                // modules: false, // "amd" | "umd" | "systemjs" | "commonjs" | "cjs" | "auto" | false
                include: ['@babel/plugin-transform-computed-properties'],
            },
        ],
    ];
    options = {
        ...options,
        // 开发工具已经实现 app.json resolveAlias 别名配置,所以这里不做处理
        // plugins: [[hook.replaceResolveAlias], ...(options?.plugins || [])],
        // 语法兼容处理
        babelrc: true,
        // configFile
    };
    if (!options.presets) {
        options.presets = envPreset;
    } else {
        const preset = options.presets.find((preset) => preset && preset[0] === presetEnv);
        if (!preset) {
            options.presets.unshift(envPreset[0]);
        } else {
            preset[1] = Object.assign(preset[1] || {}, { targets: { chrome: 73 } });
        }
    }

    return options;
};
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const transform = function (code, options, callback) {
    if (typeof options === 'function') {
        return babel.transform(code, options);
    }

    // eslint-disable-next-line @typescript-eslint/no-var-requires
    return babel.transform(
        code,
        // code?.replace(
        //     /((import \s*|from \s*|import\(|require\()["'])[~@]\//g,
        //     (_, code) => code + '/',
        // ),
        hook.toOptions(options),
        callback,
    );
};

const transformFromAstSync = function (ast, code, options) {
    return babel.transformFromAstSync(ast, code, hook.toOptions(options));
};
const transformSync = function (code, options) {
    return babel.transformSync(code, hook.toOptions(options));
};
const transformAsync = function (code, options) {
    return babel.transformAsync(code, hook.toOptions(options));
};
const _hook = {
    replaceResolveAlias,
    toOptions,
    transform,
    transformSync,
    transformAsync,
    transformFromAstSync,
};
const _hook_ = (() => {
    try {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        return require(cwd + '/transform.hook.js')({ cwd, babel, presetEnv, hook: _hook });
    } catch (e) {
        console.error(e);
    }
})();
const hook = (_hook_ && Object.assign(_hook_, _hook, _hook_)) || _hook;
module.exports = hook;
// 文件放 /code/package.nw/js/common/miniprogram-builder/babel目录
// 修改 /code/package.nw/js/common/miniprogram-builder/common/code-analyse/index.js
//  require('@babel/core')(...) 改为 require('../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/summer/plugins/typescript.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/summer/plugins/enhance.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/summer/plugins/base/es6module.js
// require('@babel/core')(...) 改为 require('../../../../../babel/core')(...)
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/original/js/enhance.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/original/js/generateMap.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)

// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/original/js/workletCompile.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/utils/babel_plugin_worklet.js
//  require('@babel/core')(...) 改为 require('../babel/core')(...)
// 修改 /code/package.nw/js/common/miniprogram-builder/modules/corecompiler/summer/plugins/worklet.js
// require('@babel/core')(...) 改为 require('../../../../babel/core')(...)
