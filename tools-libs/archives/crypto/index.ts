/**
 * @author oiguoga Fa
 * @copyright oiguoga 2017
 */
import CryptoJS from 'crypto-js';

import RSA from './RSA';
export const crypto = {
    base: CryptoJS,
    MD5: function (content: string) {
        return CryptoJS.MD5(content).toString();
    },
    encryptDES: function (message: string, key: string, iv: string, mode: number) {
        return CryptoJS.DES.encrypt(
            CryptoJS.enc.Utf8.parse(message),
            CryptoJS.enc.Utf8.parse(key),
            {
                iv: CryptoJS.enc.Hex.parse(iv),
                mode: mode || CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7,
            },
        ).toString();
    },

    decryptDES: function (cipherText: string, key: string, iv: string, mode: number) {
        CryptoJS.DES.decrypt(cipherText, CryptoJS.enc.Utf8.parse(key), {
            iv: CryptoJS.enc.Hex.parse(iv),
            mode: mode || CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
        }).toString(CryptoJS.enc.Utf8);
    },

    encrypt3DES: (message: string, key: string, iv: string, mode: number) =>
        CryptoJS.TripleDES.encrypt(CryptoJS.enc.Utf8.parse(message), CryptoJS.enc.Utf8.parse(key), {
            iv: CryptoJS.enc.Hex.parse(iv),
            mode: mode || CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
        }).toString(),

    decrypt3DES: (cipherText: string, key: string, iv: string, mode: number) =>
        CryptoJS.TripleDES.decrypt(cipherText, CryptoJS.enc.Utf8.parse(key), {
            iv: CryptoJS.enc.Hex.parse(iv),
            mode: mode || CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
        }).toString(CryptoJS.enc.Utf8),

    encryptAES: (word: string, key: string, iv: string, mode: number) =>
        CryptoJS.AES.encrypt(
            word,
            CryptoJS.enc.Utf8.parse(key), // 16位
            {
                iv: CryptoJS.enc.Utf8.parse(iv),
                mode: mode || CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7,
            },
        ).toString(), // encrypted.ciphertext.toString()

    decryptAES: (cipherText: string, key: string, iv: string, mode: number) =>
        CryptoJS.AES.decrypt(cipherText, CryptoJS.enc.Utf8.parse(key), {
            iv: CryptoJS.enc.Utf8.parse(iv),
            mode: mode || CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
        }).toString(CryptoJS.enc.Utf8),

    encryptRSA: (plaintext: string, N: string | null, E: string | null) =>
        RSA.lineBrk(new RSA().setPublic(N, E).encrypt(plaintext), 64),
    decryptRSA: (
        cipherText: string,
        N: string | null,
        E: string | null,
        D: number,
        P: number,
        Q: number,
        DP: number,
        DQ: number,
        C: number,
    ) => new RSA().setPrivateEx(N, E, D, P, Q, DP, DQ, C).decrypt(cipherText),
    generateRSA: (bits: number, E: number) => {
        const rsa = new RSA();
        rsa.generate(bits | 0, E);
        const lineBrk = RSA.lineBrk;
        return {
            n: lineBrk(rsa.n.toString(16), 64),
            d: lineBrk(rsa.d.toString(16), 64),
            p: lineBrk(rsa.p.toString(16), 64),
            dp: lineBrk(rsa.dmp1.toString(16), 64),
            dq: lineBrk(rsa.dmq1.toString(16), 64),
            cf: lineBrk(rsa.coeff.toString(16), 64),
        };
    },
};
