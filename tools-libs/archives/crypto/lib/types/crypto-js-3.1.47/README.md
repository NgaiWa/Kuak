# Installation
> `npm install --save @types/crypto-js`

# Summary
This package contains type definitions for crypto-js (https://github.com/evanvosberg/crypto-js).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/crypto-js.

### Additional Details
 * Last updated: Sat, 23 May 2020 15:07:37 GMT
 * Dependencies: none
 * Global values: `CryptoJS`

# Credits
These definitions were written by [Michael Zabka](https://github.com/misak113), [Max Lysenko](https://github.com/maximlysenko), and [Brendan Early](https://github.com/mymindstorm).
