// index.ts
(class Home extends Page.Base {
    data!: TData<{ clickTip: string; count: number }>;
    static dataObservers = {
        count(this: Home, count: number) {
            this.setData({ clickTip: `点击了${count}次` });
        },
    };
    onCreate(_options: any, restoredPage: Home) {
        if (
            !this.restoreInitPage(restoredPage, {
                clickTip: Date(),
                count: 0,
                time: new Date().toTimeString(),
            })
        ) {
            console.log('没有旧页面要恢复');
        }
    }
    onLoad() {
        this.addEventListener('addClick', console.log);
    }
    onShow() {
        console.log('show');
    }
    onBack() {
        this.setData({ count: this.data.count + 1 });
        this.router.navigateBack();
        console.log(this.router.navHistory.currentLength);
    }
    onGo() {
        this.setData({ count: this.data.count + 1 });
        this.router.navigateTo('../next/index');
    }
    onForward() {
        this.setData({ count: this.data.count + 1 });
        this.router.navHistory.forward();
    }
    onUnload() {
        console.error(2323);
    }
}).register();
