/**
 * @author Fa
 * @copyright Fa 2017
 */
export default class Counter {
    value = 0;

    plus(factor: number) {
        this.value += factor;
    }

    minus(factor: number) {
        this.value -= factor;
    }

    isFirst() {
        return this.value <= 1;
    }

    isInitial() {
        if (this.value < 0) {
            this.value = 0;
        }
        return this.value <= 0;
    }
}
