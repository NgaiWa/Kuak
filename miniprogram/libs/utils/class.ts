/**
 * @author Fa
 * @copyright Fa 2017
 */
import { unionPush } from './array';
import { setDescByKey } from './object';

/**
 * @author Fa
 * @copyright Fa 2017
 */
const _getBaseProto = function (
    instance: any,
    deep: -1 | number,
    current: number,
    preventInfiniteLoops: any[],
    stop?: any,
    until?: any,
    refs?: any[],
): any {
    if (instance == null) {
        return instance;
    }
    const proto = Object.getPrototypeOf(instance);
    if (
        proto === until ||
        proto === Object ||
        proto === Object.prototype ||
        proto === Function ||
        proto === Function.prototype
    ) {
        return instance;
    }
    unionPush(refs!, proto);
    if (
        (deep >= 0 && current > deep) ||
        proto === instance ||
        proto === stop ||
        preventInfiniteLoops.includes(proto)
    ) {
        return proto;
    }
    preventInfiniteLoops.push(instance);
    return _getBaseProto(proto, deep, current + 1, preventInfiniteLoops, stop, until);
};
export const getBaseProto = function (
    instance: any,
    deep?: boolean | -1 | number,
    stop?: any,
    until?: any,
    refs?: any[],
): any {
    unionPush(refs!, instance);
    return _getBaseProto(
        instance,
        deep === true ? -1 : Number(deep) || 0,
        1,
        [],
        stop,
        until,
        refs,
    );
};
const getConstructor = function (proto: any) {
    return proto != null && typeof proto.constructor === 'function' ? proto.constructor : null;
};
const _getBaseClass = function (
    instance: any,
    deep: -1 | number,
    current: number,
    preventInfiniteLoops: any[],
    Stop?: IConstructor,
    Until?: IConstructor,
    refs?: IConstructor[],
): IConstructor | null {
    if (instance == null) {
        return null;
    }
    const proto = getBaseProto(instance);
    if (proto === null || proto === Until?.prototype || typeof proto.constructor !== 'function') {
        return getConstructor(instance);
    }
    const Class = proto.constructor;

    if (
        (deep >= 0 && current > deep) ||
        proto === instance ||
        Class === Stop ||
        preventInfiniteLoops.includes(Class)
    ) {
        return Class;
    }
    preventInfiniteLoops.push(Class);
    return _getBaseClass(proto, deep, current + 1, preventInfiniteLoops, Stop, Until, refs);
};
export const getBaseClass = function (
    instance: any,
    deep?: boolean | number,
    Stop?: IConstructor,
    Until?: IConstructor,
    refs?: IConstructor[],
): IConstructor | null {
    if (refs) {
        unionPush(refs, getConstructor(instance));
    }
    return _getBaseClass(
        instance,
        deep === true ? -1 : Number(deep) || 0,
        1,
        [],
        Stop,
        Until,
        refs,
    );
};

export const mixins: MixinsFunc = function (target: any, ...mixins: any[]) {
    let names = Object.getOwnPropertyNames(target);
    let _target = target;
    mixins.forEach((mixin) => {
        Object.getOwnPropertyNames(mixin).forEach((name) => {
            if (names.includes(name)) {
                return;
            }
            setDescByKey(_target, mixin, name, { configurable: true });
        });
        mixin = null;
    });
    _target = names = null as any;
    return target;
};

export const CMixins: CMixinsFunc = function (
    BaseClass: IConstructor,
    proto: any,
    ...prototypes: any[]
) {
    mixins(BaseClass.prototype, proto, ...prototypes);
    return BaseClass as any;
};
export const MultiInherit: MultiInheritFunc = function (
    Class: IConstructor,
    Class1: IConstructor,
    ...Classes: IConstructor[]
) {
    return CMixins(
        mixins(class extends Class {}, Class1, ...Classes),
        Class1.prototype,
        Classes.map((C) => C.prototype),
    );
} as any;
