/**
 * @author Fa
 * @copyright Fa 2017
 */
import EventTarget from '../polyfill/EventTarget';
import { toResult } from './function';
import { oHasOwn, pick, setValueByKeys } from './object';
import { localStorageLib, toSetData } from './StorageLib';
import { generateKey } from './string';
import { isArray } from './type';
export class Storage extends EventTarget implements IStorage {
    protected lib: typeof localStorageLib = localStorageLib;
    get<N extends string | string[] | true = string, R extends boolean = false>(
        name: N,
        returnMap?: R,
    ): R extends true
        ? IResult<{ [n: string]: any }>
        : N extends string[]
          ? IResult<any[]>
          : IResult {
        if (isArray(name)) {
            const nameArr = name as string[];
            const values = returnMap ? {} : [];
            let state: boolean = true;
            let error: Error = null as any;
            for (let i = 0, l = nameArr.length, n; i < l; i++) {
                n = nameArr[i];
                try {
                    (values as any)[returnMap ? n : i] = this.lib.get(n);
                    state = state && true;
                } catch (e) {
                    state = false;
                    error = e as Error;
                }
            }
            return toResult(state, values, error) as any;
        }
        try {
            const value = this.lib.get(name as string);
            return toResult(true, returnMap ? { [name as string]: value } : value) as any;
        } catch (error) {
            return toResult(false, returnMap ? {} : null, error) as any;
        }
    }
    set(name: string | AnyObject, value?: any): IResult {
        const data = this.lib.toSetData(name, value);
        let state: boolean = true;
        let error: Error = null as any;
        let hasChange = false;
        for (const key in data) {
            if (oHasOwn(data, key)) {
                try {
                    this.lib.set(key, data[key]);
                    hasChange = true;
                    state = state && true;
                } catch (e) {
                    state = false;
                    error = e as Error;
                }
            }
        }
        if (state && hasChange) {
            this.dispatchEvent('change', { data });
        }
        return toResult(state, undefined, error);
    }
    remove(name: string | string[]): IResult {
        const nameArr: string[] = this.lib.toNameList(name);
        let state: boolean = true;
        let error: Error = null as any;
        for (let i = 0, l = nameArr.length; i < l; i++) {
            try {
                this.lib.remove(nameArr[i]);
                state = true;
            } catch (e) {
                state = false;
                error = e as Error;
            }
        }
        if (state && nameArr.length) {
            this.dispatchEvent('change', {
                isRemove: true,
                keys: nameArr,
                data: setValueByKeys({}, nameArr, undefined),
            });
        }
        return toResult(state, nameArr, error);
    }
    clear(): IResult {
        try {
            const keys = this.getInfo().value?.keys;
            const ret = toResult(true, this.lib.clear());
            if (keys.length) {
                this.dispatchEvent('change', {
                    isClear: true,
                    keys: keys,
                    data: setValueByKeys({}, keys, undefined),
                });
            }
            return ret;
        } catch (e) {
            return toResult(false, undefined, e as Error);
        }
    }

    getInfo(): IResult<{ keys: string[]; currentSize: number; limitSize: number }> {
        try {
            return toResult(true, this.lib.getInfo());
        } catch (e) {
            return toResult(false, undefined as any, e as Error);
        }
    }
    isSafeSize(inputSize?: number): boolean {
        return this.lib.isSafeSize(this.getInfo().value, inputSize);
    }
    asyncGet(name: string | string[]): Promise<any> {
        return this.lib.execAsyncGet(this.lib.asyncGetByName, name);
    }
    getDecrypt(name: string | string[]): Promise<any> {
        return this.lib.execAsyncGet(this.lib.getDecrypt, name);
    }
    asyncSet(name: string | AnyObject, value?: any): Promise<any> {
        const data = this.lib.toSetData(name, value);
        const keys = Object.keys(data);
        return keys.length
            ? this.lib.execAsyncSet(data).then((res) => {
                  this.dispatchEvent('change', {
                      isSet: true,
                      data: data,
                  });
                  return res;
              })
            : Promise.resolve(toResult(true));
    }
    setEncrypt(name: string | AnyObject, value?: any): Promise<any> {
        const data = this.lib.toSetData(name, value);
        const keys = Object.keys(data);
        return keys.length
            ? this.lib.execAsyncSet(data, null, true).then((res) => {
                  this.dispatchEvent('change', {
                      isSet: true,
                      keys: keys,
                      data: data,
                  });
                  return res;
              })
            : Promise.resolve(toResult(true));
    }
    asyncRemove(name: string | string[]): Promise<any> {
        const nameArr = this.lib.toNameList(name);
        return nameArr.length
            ? Promise.all(nameArr.map(this.lib.asyncRemoveByName)).then((res) => {
                  this.dispatchEvent('change', {
                      isRemove: true,
                      keys: nameArr,
                      data: setValueByKeys({}, nameArr, undefined),
                  });
                  return res;
              })
            : Promise.resolve([]);
    }
    asyncClear(): Promise<any> {
        const keys = this.getInfo().value?.keys;
        const result = this.lib.asyncClear();
        if (!keys?.length) {
            return result;
        }
        return result.then((res) => {
            const keys = this.getInfo().value?.keys;
            if (keys?.length) {
                this.dispatchEvent('change', {
                    isClear: true,
                    keys: keys,
                    data: setValueByKeys({}, keys, undefined),
                });
            }
            return res;
        });
    }
    asyncGetInfo(): Promise<IStorageInfo> {
        return this.lib.asyncGetInfo();
    }
    asyncIsSafeSize(inputSize?: number): Promise<boolean> {
        const infoAsync = this.lib.asyncGetInfo();
        return inputSize
            ? infoAsync
                  .then((info) => this.lib.isSafeSize(info, inputSize))
                  .finally(() => {
                      inputSize = null as any;
                  })
            : infoAsync.then(this.lib.isSafeSize);
    }
}
export const localeStorage: Storage = new Storage();

export class Store<Data extends AnyObject = AnyObject> extends EventTarget implements IStore<Data> {
    readonly id: string;
    readonly options: IStore<Data>['options'];
    readonly storageKeys?: string[];
    protected readonly isUseSave: boolean;
    readonly data!: Data;
    private __last_save_data__!: AnyObject;
    static create<Data = AnyObject>(options: IStore<Data>['options']) {
        return new this<Data>(options);
    }
    constructor(options?: IStore<Data>['options']) {
        super();
        this.id = generateKey();
        this.options = options! || ({} as AnyObject);
        const storage = this.options.storage;
        this.isUseSave = storage != null;
        this.restore();
        if (storage != null) {
            storage.addEventListener('change', (e) => {
                let data = e.detail.data;
                const {
                    __last_save_data__,
                    options: { name, storageKeys },
                } = this;
                if (!data || __last_save_data__ === data || (name && !data[name])) {
                    return;
                }
                if (name) {
                    data = data[name];
                } else if (storageKeys) {
                    data = pick(data, storageKeys);
                }
                const keys = Object.keys(data);
                if (keys.length) {
                    Object.assign(this.data, data);
                    this.dispatchEvent('change', { keys, data });
                }
            });
        }
    }
    protected init(data: AnyObject) {
        const { read, init } = this.options;
        data = data || {};
        data = read?.(this, data) || data;
        // @ts-expect-error
        this.data = init?.(this, data) || data;
        this.save(this.data);
    }
    restore() {
        const {
            data,
            isUseSave,
            options: { storage, name, storageKeys },
        } = this;
        this.init(
            Object.assign(
                data || {},
                isUseSave &&
                    (name
                        ? storage!.get(name)
                        : storage!.get(storageKeys || storage?.getInfo().value?.keys || [], true)
                    ).value,
            ),
        );
    }
    reset(data: AnyObject) {
        this.init(data);
    }
    protected save(data: AnyObject) {
        const keys = Object.keys(data);
        if (!keys.length) {
            return;
        }
        if (this.isUseSave) {
            const { storage, storageKeys, write, name } = this.options;
            let _data = storageKeys ? pick(data, storageKeys) : data;
            _data = write?.(this, _data) || _data;
            if (name) {
                storage!.set(name, _data);
            } else {
                storage!.set(_data);
            }
        }
        this.__last_save_data__ = data;

        this.dispatchEvent('change', { keys, data });
    }
    set(name: string | AnyObject, value?: any) {
        const data: AnyObject = toSetData(name, value);
        Object.assign(this.data as any, data);
        this.save(data);
    }
    get(name: string | string[], returnMap?: boolean): any {
        if (isArray(name)) {
            return name.reduce(
                (ret, name, index) => {
                    ret.values[ret.returnMap ? name : index] = ret.data[name];
                    return ret;
                },
                {
                    values: (returnMap ? {} : []) as any,
                    returnMap: returnMap,
                    data: this.data,
                },
            ).values;
        }
        const value = this.data[name as string];
        return returnMap ? { [name as string]: value } : value;
    }
}
