/**
 * @author Fa
 * @copyright Fa 2017
 */

export default class DelayActuator<T = any> {
    // 延迟毫秒
    delayTime: number;

    // 延迟id
    private _sid: any;

    private _bound!: (...args: any[]) => ReturnType<DelayActuator['_resetDelay']>;

    private _args!: any[];

    private _usePromise: any;

    private _promise!: Promise<T>;

    private _promiseCall: any;

    actuator?: (...args: any[]) => void;

    constructor(
        delayTime: DelayActuator['delayTime'],
        actuator?: DelayActuator['actuator'],
        usePromise?: true | false,
    ) {
        this.delayTime = delayTime;
        this._exec = this._exec.bind(this);
        this.actuator = actuator;
        this._usePromise = usePromise;
    }

    private _exec() {
        try {
            const value = this.actuator!(...this._args);
            if (this._promiseCall) {
                this._promiseCall.resolve(value);
            }
        } catch (e) {
            if (this._promiseCall) {
                this._promiseCall.redirect(e);
            }
        }
        this._promiseCall = null;
        this._sid = null as any;
        this._args = null as any;
    }

    clear() {
        this.stop();
        this.actuator = null as any;
        this._promise = null as any;
    }

    // 延迟器调用绑定
    get bound() {
        if (!this._bound) {
            this._bound = this.delay.bind(this);
        }
        return this._bound;
    }

    update(actuator?: DelayActuator['actuator']) {
        if (actuator) {
            this.actuator = actuator;
        }
        return this;
    }

    delay(...args: any[]) {
        return this._resetDelay(args, false);
    }

    stop() {
        clearTimeout(this._sid);
        if (this._promiseCall) {
            this._promiseCall.redirect(false);
        }
        this._promiseCall = null;
        this._sid = null;
        this._args = null as any;
    }

    private _resetDelay(
        args: any[],
        replace: boolean,
    ): DelayActuator['_usePromise'] extends true ? DelayActuator['_promise'] : this {
        this._args = args;
        if (replace || !this._sid) {
            this.stop();
            this._args = args;
            this._sid = setTimeout(this._exec, this.delayTime);
            if (this._usePromise) {
                this._promise = new Promise((resolve, redirect) => {
                    this._promiseCall = { resolve, redirect };
                });
            }
        }
        if (this._usePromise) {
            return this._promise;
        }
        return this;
    }

    resetDelay(...args: any[]) {
        return this._resetDelay(args, true);
    }
}

export const threadSleep = function (milliseconds: any) {
    return new Promise(function (resolve) {
        setTimeout(resolve, milliseconds);
    });
};
