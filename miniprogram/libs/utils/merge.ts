/**
 * @author Fa
 * @copyright Fa 2017
 */

import { setDescByKey, setValue } from './object';
import { isObject } from './type';

const _merge = function (
    target: any,
    source: any,
    set: (target: any, key: any, value: any, source: any, options: any) => void,
    setOptions?: any,
    deep?: boolean,
    preventInfiniteLoops?: any[],
) {
    if (!isObject(source) || preventInfiniteLoops?.includes(source)) {
        return target;
    }
    preventInfiniteLoops?.push(source);
    const keys = Object.keys(source);
    for (let i = 0, l = keys.length, key, value, targetValue; i < l; i++) {
        key = keys[i];
        value = source[key];
        targetValue = target[key];
        if (deep && isObject(value) && isObject(targetValue)) {
            _merge(targetValue, value, set, setOptions, deep, preventInfiniteLoops || []);
        } else {
            set(target, key, value, source, setOptions);
        }
    }
    return target;
};
const setDesc = function (target: any, key: any, _value: any, source: any, extendDesc: any) {
    setDescByKey(target, source, key, extendDesc);
};
const _mergeSources = function (
    target: any,
    sources: any[],
    deep?: boolean,
    isDesc?: boolean,
    extendDesc?: any,
) {
    if (!sources.length || !isObject(target)) {
        return target;
    }
    const set = isDesc ? setDesc : setValue;
    for (let i = 0, l = sources.length; i < l; i++) {
        _merge(target, sources[i], set, extendDesc, deep);
    }
    return target;
};
export const mergeSources = function (
    target: any,
    sources: any[],
    isDesc?: boolean,
    extendDesc?: any,
) {
    return _mergeSources(target, sources, true, isDesc, extendDesc);
};
export const merge: typeof Object.assign = function (target: any, ...sources: any[]) {
    return _mergeSources(target, sources, true);
};
export const mergeDesc: typeof Object.assign = function (target: any, ...sources: any[]) {
    return _mergeSources(target, sources, true, true);
};
export const assignSources = function (
    target: any,
    sources: any[],
    isDesc?: boolean,
    extendDesc?: any,
) {
    return _mergeSources(target, sources, false, isDesc, extendDesc);
};
export const assign: typeof Object.assign = function (target: any, ...sources: any[]) {
    return assignSources(target, sources, false);
};
export const assignDesc: typeof Object.assign = function (target: any, ...sources: any[]) {
    return assignSources(target, sources, true);
};
