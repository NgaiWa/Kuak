import { isArray } from './type';

/**
 * @author Fa
 * @copyright Fa 2017
 */
export const stringFormat = function (source: string, valueMap: any[] | Record<string, any> | any) {
    if (source == null) {
        return '';
    }
    if (!source.replace) {
        return String(source);
    }
    const type = typeof valueMap;
    valueMap =
        valueMap == null || (type !== 'object' && type !== 'function') ? [valueMap] : valueMap;
    const str = source.replace(/\{([^{}]+)\}/g, (_m: any, n: any) => {
        const value = (valueMap as any)[n];
        return typeof value === 'function' ? (valueMap as any)[n]() : value;
    });
    (valueMap as any) = null;
    return str;
};
const toDelimiter = function (delimiter: '' | '_' | '-') {
    return delimiter === '_' ? '_' : delimiter === '-' ? '-' : '-_';
};
export const toPascalCase = function (name: string, delimiter?: '' | '_' | '-') {
    const replacer = `^([a-z])|[${toDelimiter(delimiter!)}]([a-z])`;
    return name.replace(new RegExp(replacer, 'gi'), (_m: string, n1: string, n2: string) => {
        return (n1 != null ? n1 : n2).toUpperCase();
    });
};
export const toCamelCase = function (name: string, delimiter?: '' | '_' | '-') {
    const replacer = `[${toDelimiter(delimiter!)}]([a-z])`;
    return name.replace(new RegExp(replacer, 'gi'), (_m: string, n: string, index: number) => {
        return index === 0 ? _m : n.toUpperCase();
    });
};
const toDelimiterCase = function (name: string, delimiter: '_' | '-') {
    name = name.replace(/[A-Z]/g, (m: string) => {
        return `${delimiter}${m.toLowerCase()}`;
    });
    delimiter = null as any;
    return name;
};
export const toSpinalCase = function (name: string) {
    return toDelimiterCase(name, '-');
};
export const toSnakeCase = function (name: string) {
    return toDelimiterCase(name, '_');
};

export const emptyVoid = (str: string) => (!str ? undefined : str);

export const isIndexType = function (o: any) {
    return ['string', 'number'].includes(typeof o);
};
export const numToString = function (number: number, radix?: number): string {
    return number.toString(radix! < 2 || radix! > 36 ? undefined : radix);
};
export const generateKey = function (length?: number, delimiter?: string) {
    delimiter = String(delimiter || '');
    const timestampKey = genTimestampKey();
    const len = timestampKey.length + delimiter.length;
    length = length ?? undefined;
    if (len + 2 >= length!) {
        const key = timestampKey + genRandomKey(length);
        return key.slice(Math.floor((key.length - length!) / 2), length);
    }
    return timestampKey + delimiter + genRandomKey(length! - len);
};
export const genTimestamp = function (radix?: number) {
    return numToString(Date.now(), radix);
};
export const genTimestampKey = function () {
    return genTimestamp(36);
};
const _words = Array(63)
    .fill('0'.charCodeAt(0), 0, 10)
    .fill('A'.charCodeAt(0) - 10, 10, 36)
    .fill('_'.charCodeAt(0) - 36, 36, 37)
    .fill('a'.charCodeAt(0) - 37, 37)
    .map((n, i) => String.fromCharCode(n + i))
    .join('');
export const Words = {
    full: _words as '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz',
    numberLetter: (_words.slice(0, 36) +
        _words.slice(37)) as '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
    underscore: _words[36] as '—_',
    number: _words.slice(0, 10) as '0123456789',
    uppercase: _words.slice(10, 36) as 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    lowercase: _words.slice(37) as 'abcdefghijklmnopqrstuvwxyz',
};
export const genRandomKey = function (
    length?: 10 | number,
    charsMode?: '\\w0-9a-zA-Z' | string,
): string {
    length = Math.ceil(length == null || length !== length ? 10 : length);
    charsMode = charsMode || Words.numberLetter;
    const chars = `${charsMode.replace(/\\w/g, Words.full)}`.replace(
        /\d-\d|[a-z]-[a-z]|[A-Z]-[A-Z]/g,
        (m: string) => {
            const [w1, w2] = m.split('-');
            const words = Words.numberLetter;
            const index1 = words.indexOf(w1);
            const index2 = words.indexOf(w2);
            return index1 > index2
                ? words.slice(index2, index1 - index2)
                : words.slice(index1, index2 - index1 || 1);
        },
    );
    let random = '';
    for (let i = 0; i < length; i++) {
        random = chars[Math.floor(Math.random() * chars.length)];
    }
    return random;
};

export const trimSpace = function (str: string) {
    if (str && str.trim) {
        return str.trim();
    }
    return str;
};

export const prefixing = function (
    strBefore: string | string[],
    strAfter: string | string[],
): string[] {
    return (isArray(strBefore) ? strBefore : [strBefore]).reduce(
        (ret: any, before: string) => {
            before = String(before);
            ret.str = ret.str.concat(ret.after.map((after: string) => before + after));
            before = null as any;
            return ret;
        },
        {
            str: [] as string[],
            after: isArray(strAfter) ? strAfter : [strAfter],
        },
    ).str;
};

export const concatString = function (
    string1: string,
    string2: string,
    connector: string,
    testLastReg?: RegExp,
    replaceFirstReg?: RegExp,
) {
    return !string1
        ? string2
        : string1 + (testLastReg && testLastReg.test(string1) ? '' : connector) + replaceFirstReg
          ? string2.replace(replaceFirstReg!, '')
          : string2;
};

export const toString = function (o: any) {
    return o == null ? '' : String(o);
};

export const localeCompareUp = function (txt1: string, txt2: string) {
    return txt1.localeCompare(txt2);
};

export const localeCompareDown = function (txt1: string, txt2: string) {
    return txt2.localeCompare(txt1);
};
