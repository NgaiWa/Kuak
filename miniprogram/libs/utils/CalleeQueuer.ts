/**
 * @author Fa
 * @copyright Fa 2017
 */

import { apply, boundThis, Callee, createCallQueue } from './function';
import { isArray } from './type';

const paramsHandler = function <T = any>(param: any, optionIndex: number, options: AnyObject): T {
    const params = isArray(param) ? param.slice(0, optionIndex + 1) : [param];
    if (params[optionIndex] === undefined) {
        params[optionIndex] = options;
    }
    return params as any;
};
export class CalleeQueuer implements ICalleeQueuer {
    static!: ICalleeQueuerConstructor;
    static createQueue = createCallQueue;
    static createItem(callee: ICallee, options?: AnyObject): ICalleeQueueItem {
        return { callee, ...options };
    }
    static createItems(params: CalleeQueueCalleeParams[], options?: AnyObject): ICalleeQueueItem[] {
        const items: ICalleeQueueItem[] = [];
        for (let i = 0, l = params.length; i < l; i++) {
            items.push(
                this.createItem(...paramsHandler<CalleeQueueCalleeParams>(params[i], 1, options!)),
            );
        }
        return items;
    }
    static createFuncItem(
        func: ICallee['func'],
        args?: any[],
        once?: boolean,
        options?: AnyObject,
    ): ICalleeQueueItem {
        return this.createItem(Callee.create(null, func, args, once), options);
    }
    static createFuncItems(
        params: CalleeQueueFuncItemParams[],
        options?: AnyObject,
    ): ICalleeQueueItem[] {
        const items: ICalleeQueueItem[] = [];
        for (let i = 0, l = params.length; i < l; i++) {
            items.push(
                this.createFuncItem(
                    ...paramsHandler<CalleeQueueFuncItemParams>(params[i], 1, options!),
                ),
            );
        }
        return items;
    }
    static addItem(queue: ICalleeQueue, ...items: ICalleeQueueItem[]) {
        this.addItems(queue, items);
    }
    static addItems(queue: ICalleeQueue, items: ICalleeQueueItem[]) {
        for (let i = 0, l = items.length, item; i < l; i++) {
            item = items[i];
            if (item && queue.indexOf(item) === -1) {
                queue.push(item);
            }
        }
    }
    static addCallee(queue: ICalleeQueue, callee: ICallee, options?: AnyObject) {
        this.addItem(queue, this.createItem(callee, options));
    }
    static addCallees(queue: ICalleeQueue, params: CalleeQueueCalleeParams[], options?: AnyObject) {
        this.addItems(queue, this.createItems(params, options));
    }
    static addFunc(
        queue: ICalleeQueue,
        func: ICallee['func'],
        args?: any[],
        once?: boolean,
        options?: AnyObject,
    ) {
        this.addItem(queue, this.createFuncItem(func, args, once, options));
    }
    static addFuncs(queue: ICalleeQueue, params: CalleeQueueFuncItemParams[], options?: AnyObject) {
        this.addItems(queue, this.createFuncItems(params, options));
    }
    static execQueue(queue: ICalleeQueue, ...args: any[]) {
        this.applyQueue(queue, args);
    }
    static applyQueue(queue: ICalleeQueue, args: any[]) {
        const queueCpy = queue.concat();
        for (let i = 0, l = queueCpy.length; i < l; i++) {
            apply(queueCpy[i].callee, args);
        }
        for (let di = 0, callee; di < queue.length; di++) {
            callee = queue[di];
            if (!callee || (callee.once && callee.called)) {
                queue.splice(di--, 1);
            }
        }
    }
    // @ts-expect-error
    private _disposeBound: any;
    // @ts-expect-error
    private _destroyExec: any;
    queue: ICalleeQueue;
    constructor() {
        this.queue = [];
    }
    exec(...args: any) {
        this.apply(args);
    }
    apply(args: any[]) {
        this.static.applyQueue(this.queue, args);
    }
    dispose() {
        this.queue = [];
        this._destroyExec = null;
        this._disposeBound = null;
    }
    boundExec(): ICalleeQueuer['exec'] {
        return boundThis(this, 'exec');
    }
    boundDispose(): ICalleeQueuer['dispose'] {
        return boundThis(this, 'dispose');
    }
    addItem(...items: ICalleeQueueItem[]) {
        this.addItems(items);
    }
    addItems(items: ICalleeQueueItem[]) {
        this.static.addItems(this.queue, items);
    }
    addCallee(callee: ICallee, options?: AnyObject) {
        this.static.addCallee(this.queue, callee, options);
    }
    addCallees(params: CalleeQueueCalleeParams[], options: AnyObject) {
        this.static.addCallees(this.queue, params, options);
    }
    addFunc(func: ICallee['func'], args?: any[], once?: boolean, options?: AnyObject) {
        this.static.addFunc(this.queue, func, args, once, options);
    }
    addFuncs(params: CalleeQueueFuncItemParams[], options?: AnyObject) {
        this.static.addFuncs(this.queue, params, options);
    }
}
Object.defineProperty(CalleeQueuer.prototype, 'static', { value: CalleeQueuer });
