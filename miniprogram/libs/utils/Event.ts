/**
 * @author Fa
 * @copyright Fa 2017
 */

export const offEvent = function (event: IEventListenInfo) {
    event.target.removeEventListenerById(event.id);
};
export const offEvents = function (events: IEventMap | IEventListenInfo[]) {
    if (events) {
        Object.values(events).forEach(offEvent);
    }
};
