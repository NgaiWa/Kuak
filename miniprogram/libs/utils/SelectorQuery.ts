/**
 * @author Fa
 * @copyright Fa 2017
 */
import { isObject } from './type';

/**
 * @typedef SelectorQuery
 */
export class SelectorQuery {
    query: Node.SelectorQuery;
    nodes: Node.NodesRef;
    constructor(component?: Kuak.Page | Kuak.Component) {
        if (component?.createSelectorQuery) {
            this.query = component.createSelectorQuery();
        } else {
            this.query = App.lib.createSelectorQuery();
            if (component) {
                this.in(component as Program.Component.Instance<any, any, any>);
            }
        }
        this.nodes = null as any;
    }

    /**
     * @return {SelectorQuery}
     */
    in(component: any) {
        if (isObject(component)) {
            this.query.in(component);
        }
        return this;
    }

    /**
     * @return {SelectorQuery}
     */
    select(selector: string) {
        this.nodes = this.query.select(selector);
        return this;
    }

    /**
     * @return {SelectorQuery}
     */
    selectAll(selector: string) {
        this.nodes = this.query.selectAll(selector);
        return this;
    }

    /**
     * @return {SelectorQuery}
     */
    selectViewport() {
        this.nodes = this.query.selectViewport();
        return this;
    }

    /**
     * @return {SelectorQuery}
     */
    exec(callback?: (res: any[], ...args: any[]) => any) {
        this.query.exec(callback);
        return this;

        // 防止真机不回调
        // if (!callback) {
        //     this.query.exec();
        //     return this;
        // }
        // const _callback = function (...args: any[]) {
        //     if (!callback) {
        //         return;
        //     }
        //     const _callback: AnyFunction = callback;
        //     callback = null as any;
        //     _callback(...args);
        // };
        // this.query.exec(_callback);
        // App.lib.nextTick(_callback);
        // return this;
    }
    asyncExec(): Promise<any[] | null> {
        return new Promise((resolve) => {
            this.query.exec(resolve);
            // 防止真机不回调
            // App.lib.nextTick(resolve);
        });
    }

    /**
     * @return {SelectorQuery}
     */
    fields(fields: Node.Fields, callback?: Node.FieldsCallback) {
        if (this.nodes) {
            this.nodes.fields(fields, callback);
        }
        return this;
    }
    asyncFields(fields: Node.Fields): Promise<Record<string, any> | null> {
        if (this.nodes) {
            return new Promise((resolve) => {
                this.nodes.fields(fields, resolve);
                fields = null as any;
            });
        }
        return Promise.resolve(null);
    }

    /**
     * @return {SelectorQuery}
     */
    scrollOffset(callback: Node.ScrollOffsetCallback) {
        if (this.nodes) {
            this.nodes.scrollOffset(callback);
        }
        return this;
    }
    asyncScrollOffset(): Promise<Node.ScrollOffsetCallbackResult | null> {
        if (this.nodes) {
            return new Promise((resolve) => {
                this.nodes.scrollOffset(resolve);
            });
        }
        return Promise.resolve(null);
    }
    /**
     * @return {SelectorQuery}
     */
    boundingClientRect(callback: Node.BoundingClientRectCallback) {
        if (this.nodes) {
            this.nodes.boundingClientRect(callback);
        }
        return this;
    }
    asyncBoundingClientRect(): Promise<Node.BoundingClientRectCallbackResult | null> {
        if (this.nodes) {
            return new Promise((resolve) => {
                this.nodes.boundingClientRect(resolve);
            });
        }
        return Promise.resolve(null);
    }
    /**
     * @return {SelectorQuery}
     */
    context(callback: Node.ContextCallback) {
        if (this.nodes) {
            this.nodes.context(callback);
        }
        return this;
    }
    asyncContext(): Promise<Node.ContextCallbackResult | null> {
        if (this.nodes) {
            return new Promise((resolve) => {
                this.nodes.context(resolve);
            });
        }
        return Promise.resolve(null);
    }
}

export default SelectorQuery;
