/**
 * @author Fa
 * @copyright Fa 2017
 */

import { AsyncTask } from '../AsyncTask';
import { getKeys } from '../object';
import Promiser from '../Promise';
import { isArray } from '../type';
const onFnMap = {
    headersReceived: 'onHeadersReceived',
    chunkReceived: 'onChunkReceived',
    progressUpdate: 'onProgressUpdate',
};
const offFnMap = {
    headersReceived: 'offHeadersReceived',
    chunkReceived: 'offChunkReceived',
    progressUpdate: 'offProgressUpdate',
};
const fns = Object.values(onFnMap).concat(Object.values(offFnMap));

export const getByName = function (name: string, res: any) {
    return res?.[name];
};
export const getData = function (res: any) {
    return res?.data;
};
const _getTask = function (res: any) {
    return res?.task;
};
const _off = (res: any, name: string) => {
    if (!res) {
        return;
    }
    const listener = res.config?.on?.[name];
    if (listener) {
        res.task?.[(offFnMap as any)[name]]?.(res.config.on?.[name]);
    }
};
const _offAll = (res: any) => {
    if (!res) {
        return;
    }
    const keys = getKeys(res.config?.on);
    for (let i = 0; i < keys.length; i++) {
        _off(res, keys[i]);
    }
};

export const getTask = function (res: any) {
    return res == null ? undefined : isArray(res) ? res.find(_getTask) : _getTask(res);
};

export const off = (res: any, name: string) => {
    if (isArray(res)) {
        res.forEach((res) => {
            return _off(res, name);
        });
        name = null as any;
    } else {
        _off(res, name);
    }
};
export const offAll = (res: any) => {
    if (isArray(res)) {
        res.forEach(_offAll);
    } else {
        _offAll(res);
    }
};
export const getTargetTask = function (target: { res: any }) {
    return getTask(target.res);
};
export const targetOff = function (target: { res: any }, name: string) {
    return off(target.res, name);
};
export const targetOffAll = function (target: { res: any }) {
    return offAll(target.res);
};
export enum RequestTaskStatus {
    UnRequest = 0,
    Requesting = 1,
    Requested = 2,
    Cancel = 3,
}
export class AsyncRequestTask
    extends AsyncTask
    implements IRequestTask, IUploadTask, IDownloadTask
{
    get status() {
        if (this.__ref) {
            return this.__ref.status;
        }
        return RequestTaskStatus.UnRequest;
    }
    onHeadersReceived!: (listener: AnyFunction) => void;
    onChunkReceived!: (listener: AnyFunction) => void;
    onProgressUpdate!: (listener: AnyFunction) => void;
    offHeadersReceived!: (listener: AnyFunction) => void;
    offChunkReceived!: (listener: AnyFunction) => void;
    offProgressUpdate!: (listener: AnyFunction) => void;
}
Object.assign(
    AsyncRequestTask.prototype,
    fns.reduce((proto: any, name: string) => {
        proto[name] = function (this: AsyncRequestTask, listener: AnyFunction) {
            this._exec(name, listener);
        };
        return proto;
    }, {} as any),
);
export class RequestPromise<
        Response = IResponse,
        Config extends IRequestConfig = IRequestConfig,
        Task extends IRequestBaseTask = IRequestBaseTask,
        R extends IRequestPromiseRes<Config, Task> = IRequestPromiseRes<Config, Task>,
    >
    extends Promiser<Response, R>
    implements IRequestPromise<Response, Config, Task, R>
{
    get task(): Task {
        return this.res.task;
    }
    off(name: string) {
        _off(this.res, name);
    }
    offAll() {
        _offAll(this.res);
    }
    get<Name extends string>(
        name: Name,
    ): IRequestPromise<GetResponseValueByName<Response, Name>, Config, Task, R> {
        return this.then((res: Response) => {
            return (res as any)?.[name] as any;
        }) as any;
    }
    getData(): IRequestPromise<GetResponseData<Response>, Config, Task, R> {
        return this.then(getData) as any;
    }
    static getTask<T = IRequestTask>(promise: IRequestPromise): T {
        return getTargetTask(promise);
    }
    static off(promise: IRequestPromise, name: string) {
        targetOff(promise, name);
    }
    static offAll(promise: IRequestPromise) {
        targetOffAll(promise);
    }
}
