/**
 * @author Fa
 * @copyright Fa 2017
 */
const { request, downloadFile, uploadFile } = App.lib;
import { returnArg } from '../function';
import { genTimestamp } from '../string';
import { appendSearch, concatBaseUrl, parseAliasUrl } from '../url';
import { AsyncRequestTask, RequestPromise } from './Promise';

export enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    HEAD = 'HEAD',
    OPTIONS = 'OPTIONS',
    TRACE = 'TRACE',
    CONNECT = 'CONNECT',
}
export enum ContentTypes {
    Json = 'application/json;charset=UTF-8',
    FormUrlEncoded = 'application/x-www-form-urlencoded;charset=UTF-8',
    FormData = 'multipart/form-data;charset=UTF-8',
}
export interface RequestManager {
    count: number;
    max: number;
    queue: IFuncQueue;
    dataName: string;
    request(config: IRequestConfig): IRequestTask;
    requestHooks: IDefaultRequestHooks;
    responseHooks: IDefaultResponseHooks;
}

export const checkQueue = function (manager: { count: number; max: number; queue: IFuncQueue }) {
    const { count, max, queue } = manager;
    const length = max - count;
    if (length > 0 && queue.length > 0) {
        const callQueue = queue.splice(0, 2);
        for (let i = 0; i < callQueue.length; i++) {
            callQueue[i]();
        }
    }
};

export const toRequestTask = function (task: any): IRequestBaseTask {
    task.cancel = task.abort;
    return task;
};

const hooksHandler = function (
    target: any,
    hookKeys: readonly string[],
    defaultHooks: any,
    hooks: any,
) {
    hooks = hooks || {};
    for (let i = 0, key, dHook: AnyFunction, hook: AnyFunction; i < hookKeys.length; i++) {
        key = hookKeys[i];
        hook = hooks[key]!;
        dHook = defaultHooks[key];
        if (i === 0) {
            target = hook ? hook(target, dHook) : dHook(target);
        } else {
            target[key] = hook ? hook(target, dHook) : dHook(target);
        }
    }
    return target;
};
const requestHookKeys = ['config', 'header', 'params', 'data', 'url'] as const;
const responseHookKeys = [
    'response',
    'data',
    'header',
    'status',
    'code',
    'successful',
    'message',
] as const;
const callAsync = function (manager: RequestManager, config: IRequestConfig) {
    config = hooksHandler(config, requestHookKeys, manager.requestHooks, config.requestHooks);
    let promise: RequestPromise;
    if (+manager.count > manager.max) {
        promise = new RequestPromise((resolve, reject) => {
            let taskResolve: AnyFunction;
            const task = new Promise((resolve) => {
                taskResolve = resolve;
            });
            let request = manager.request;
            manager.queue.push(() => {
                const task = request({
                    ...config,
                    success: resolve as any,
                    fail: reject,
                });
                taskResolve(task);
                config = request = taskResolve = resolve = reject = null as any;
            });
            return { id: genTimestamp(), config, task: new AsyncRequestTask(task) };
        });
    } else {
        promise = new RequestPromise((resolve, reject) => {
            const res = {
                id: genTimestamp(),
                config,
                task: toRequestTask(
                    manager.request({
                        ...config,
                        success: resolve as any,
                        fail: reject,
                    }),
                ),
            };
            config = null as any;
            return res;
        });
    }
    let task = promise.res.task;
    return promise
        .catch((res: any) => {
            res.isError = true;
            res.isCancel = task.status === RequestTaskStatus.Cancel;
            return res;
        })
        .then((res: any) => {
            res.task = (task as any).__ref || task;
            res.config = config;
            res = hooksHandler(res, responseHookKeys, manager.responseHooks, config.responseHooks);
        })
        .finally(() => {
            manager.count--;
            const { count, max, queue } = manager;
            manager = task = null as any;
            checkQueue({ count, max, queue });
        });
};
const requestManager: RequestManager = {
    count: 0,
    max: 10,
    queue: [] as IFuncQueue,
    dataName: 'data',
    request: request as AnyFunction,
    requestHooks: {
        config: returnArg,
        header(header: IRequestHeader, config: IRequestConfig) {
            const { auth, contentType } = config;
            const presetHeader = {
                Authorization: auth ? `${auth.scheme} ${auth.getToken()}` : undefined,
                'Content-Type': contentType || ContentTypes.Json,
            };
            return { ...presetHeader, ...header };
        },
        data: returnArg,
        params: returnArg,
        url(url: string, config: IRequestConfig) {
            config.urlAlias;
            return appendSearch(
                concatBaseUrl(config.baseUrl!, parseAliasUrl(url, config.urlAlias!)),
                config.params!,
            );
        },
    },
    responseHooks: {
        response: returnArg,
        header: returnArg,
        data: returnArg,
        status: returnArg,
        code: returnArg,
        message: returnArg,
        successful: returnArg,
    },
};

const asyncRequest = function (config: IRequestConfig): any {
    return callAsync(requestManager, config);
};
const downloadManager = {
    count: 0,
    max: 10,
    queue: [] as IFuncQueue,
    dataName: 'data',
    request: downloadFile as AnyFunction,
    requestHooks: {
        ...requestManager.requestHooks,
    },
    responseHooks: {
        ...requestManager.responseHooks,
    },
};
const asyncDownload = function (config: IRequestConfig): any {
    config.isDownload = true;
    return callAsync(downloadManager, config);
};
const uploadManager = {
    count: 0,
    max: 10,
    queue: [] as IFuncQueue,
    dataName: 'formData',
    request: uploadFile as AnyFunction,
    requestHooks: {
        ...requestManager.requestHooks,
    },
    responseHooks: {
        ...requestManager.responseHooks,
    },
};
const asyncUpload = function (config: IRequestConfig): any {
    config.isUpload = true;
    return callAsync(uploadManager, config);
};
const toConfig = function <Config extends IRequestConfig = IRequestConfig>(
    config: string | IRequestConfig,
) {
    return (typeof config === 'string' ? { url: config as string } : config) as Config;
};
const mergeConfigProp = function (
    defaultConfig: IRequestPartialConfig,
    config: IRequestConfig,
    propName: string,
) {
    if (defaultConfig[propName] || config[propName]) {
        config[propName] = { ...defaultConfig[propName], ...config[propName] };
    }
};
const mergeConfig = function <Config extends IRequestConfig = IRequestConfig>(
    defaultConfig: IRequestPartialConfig,
    config: IRequestConfig,
    defaultHttpMethod?: HttpMethod,
): Config {
    config = { ...defaultConfig, ...config };
    config.method = toHttpMethod(config, defaultHttpMethod);
    mergeConfigProp(defaultConfig, config, 'on');
    mergeConfigProp(defaultConfig, config, 'requestHooks');
    mergeConfigProp(defaultConfig, config, 'responseHooks');
    mergeConfigProp(defaultConfig, config, 'actions');
    return config as Config;
};
const toHttpMethod = function (config: IRequestConfig, defaultHttpMethod?: HttpMethod) {
    return (config.method?.toUpperCase() as HttpMethod) || defaultHttpMethod || HttpMethod.GET;
};
export class Requester<DefaultConfig extends IRequestPartialConfig>
    implements IRequester<DefaultConfig>
{
    config: DefaultConfig;
    constructor(defaultConfig: DefaultConfig) {
        this.config = defaultConfig;
    }
    request<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config> {
        return asyncRequest(mergeConfig(this.config, config));
    }
    get<
        Response = unknown,
        Config extends string | IRequestConfig = string | IRequestConfig,
        RequestConfig extends IRequestConfig = Config extends string ? { url: string } : Config,
    >(config: Config): IRequestPromise<Response, DefaultConfig & RequestConfig> {
        return this.request<Response, RequestConfig>({
            ...toConfig<RequestConfig>(config),
            method: HttpMethod.GET,
        });
    }
    post<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config> {
        return this.request({ ...config, method: HttpMethod.POST });
    }
    put<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config> {
        return this.request({ ...config, method: HttpMethod.PUT });
    }
    delete<
        Response = unknown,
        Config extends string | IRequestConfig = string | IRequestConfig,
        RequestConfig extends IRequestConfig = Config extends string ? { url: string } : Config,
    >(config: Config): IRequestPromise<Response, DefaultConfig & RequestConfig> {
        return this.request<Response, RequestConfig>({
            ...toConfig<RequestConfig>(config),
            method: HttpMethod.DELETE,
        });
    }
    upload<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config, IUploadTask> {
        return asyncUpload(mergeConfig(this.config, config, HttpMethod.POST));
    }
    download<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config, IDownloadTask> {
        return asyncDownload(mergeConfig(this.config, config, HttpMethod.POST));
    }
}
