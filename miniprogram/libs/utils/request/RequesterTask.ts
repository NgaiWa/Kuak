/**
 * @author Fa
 * @copyright Fa 2017
 */
import { clone } from '../clone';
import DelayActuator from '../DelayActuator';
import { noop } from '../function';
import { promiseApply } from '../Promise.fn';
import { getTargetTask, targetOff, targetOffAll } from './Promise';
const noneLastTime = -1;
export const CloneMode: IRequesterTaskCloneMode = {
    none: 0,
    caughtBefore: 1,
    caughtAfter: 2,
};
const _cloneHandler = function (value: any) {
    if (value instanceof Error) {
        return value;
    }
    return clone(value);
};

export class RequesterTask<
    Request extends (task: IRequesterTaskCanceler, ...args: any) => any = AnyFunction,
    Config extends IRequesterTaskConfig = IRequesterTaskConfig,
> implements IRequesterTask<Request, Config>
{
    protected _request: Request;
    protected _actuator?: DelayActuator;
    protected _actuatorPromise?: Promise<any>;
    protected _promise: any;
    protected _args: any;
    res: any;
    config: Config;
    cache?: Promise<any>;
    lastTime: number;
    constructor(request: Request, config: Config) {
        this._request = request;
        this.config = { one: true, ...config };
        this.lastTime = noneLastTime;
        if (this.config.delay! > 0) {
            this._actuator = new DelayActuator(this.config.delay!, this._delayRequest.bind(this));
        }
    }
    protected _delayRequest(resolve: any, args: any, lastTime: any) {
        if (lastTime === this.lastTime || this.lastTime === noneLastTime) {
            this._actuatorPromise = null as any;
        }
        resolve(this._request(this, ...args));
    }
    request(...args: Parameters<Request>): ReturnType<Request> {
        const { cache, one, isKeepArgs } = this.config;
        const { lastTime } = this;
        const now = Date.now();
        if (isKeepArgs) {
            this._args = args;
        }
        if (cache) {
            if (typeof cache === 'number' && cache < now - lastTime) {
                this.cache = null as any;
            }
            if (this.cache) {
                return this.cache as any;
            }
        }
        if (one) {
            if ((one as IRequesterTaskCanceler).cancel != null) {
                (one as IRequesterTaskCanceler).cancel(this.config, this as any);
                this._cancel();
            } else if (one === true && this._promise) {
                return this._promise as any;
            }
        }
        this.lastTime = now;
        let promise: Promise<any>;
        let requester = this;

        if (this._actuator) {
            if (this._actuatorPromise) {
                return this._actuatorPromise as any;
            }
            promise = this._actuatorPromise = new Promise(function (resolve) {
                requester._actuator!.delay(resolve, args, requester.lastTime);
                requester = args = null as any;
            });
        } else {
            promise = this._request(this, ...args);
        }

        const returnPromise = this.returnHandler(promise);
        if (this._actuatorPromise) {
            this._actuatorPromise = returnPromise;
        }
        this._setPromise(promise, returnPromise);
        return returnPromise;
    }

    returnHandler(promise: Promise<any>): any {
        const { caught, cloneMode, clone: cloneHandler } = this.config;
        const clone = cloneHandler || _cloneHandler;
        let returnPromise = promise;
        if (cloneMode === CloneMode.caughtBefore) {
            returnPromise = returnPromise.then(clone);
        }
        returnPromise = caught ? returnPromise.catch(caught) : returnPromise;
        if (cloneMode === CloneMode.caughtAfter) {
            returnPromise = returnPromise.then(clone);
        }
        return returnPromise;
    }
    protected _setPromise(rawPromise: any, promise?: any) {
        promise = promise || rawPromise;
        let requester = this;
        requester._promise = promise;
        if (requester.config.cache) {
            requester._setCache(rawPromise, promise);
        }
        rawPromise.catch(noop).finally(() => {
            if (promise === requester._promise) {
                requester._promise = null;
            }
            requester = promise = null as any;
        });
    }
    protected _setCache(rawPromise: any, promise?: any) {
        promise = promise || rawPromise;
        let requester = this;
        this.cache = promise;
        rawPromise
            .catch(() => {
                if (promise === requester.cache) {
                    requester.cache = null as any;
                }
            })
            .finally(() => {
                requester = promise = null as any;
            });
    }

    requestBind<P extends Partial<_P>, _P extends any[] = [...Parameters<Request>]>(
        ...args: P
    ): (..._args: SliceParams<_P, P>) => ReturnType<Request> {
        return (this.request as any).bind(this, ...args);
    }
    protected _cancelerCall(funcName: string) {
        if (this.config.canceler) {
            this.config.canceler[funcName](this.config, this);
        }
        this.res?.task?.[funcName](this.config, this);
    }
    protected _cancel() {
        this._cancelerCall('cancel');
    }
    cancel() {
        this._cancel();
        return this.clear();
    }
    abort() {
        this._cancelerCall('abort');
        return this.clear();
    }
    destroy() {
        this.clear();
        this.config = this._actuator = this._args = null as any;
    }
    refresh(doRequest?: boolean | null) {
        const hasCache = doRequest || this.cache;
        this.clear();
        if (doRequest === false || !hasCache) {
            return;
        }
        if (this._args) {
            return this.request(...this._args);
        }
        return (this as any).request();
    }
    clear() {
        this.res = this._promise = this._actuatorPromise = null as any;
        if (this.config.clear) {
            this.config.clear(this.config, this as any);
        }
        return this.clearCache();
    }
    clearCache() {
        this.cache = null as any;
        this.lastTime = noneLastTime;
        if (this.config.clearCache) {
            this.config.clearCache(this.config, this as any);
        }
        return this;
    }
    isExpired(time?: number) {
        if ((time != null && time !== this.lastTime) || this.lastTime === noneLastTime) {
            return true;
        }
        const { cache } = this.config || {};
        return typeof cache === 'number' && cache < Date.now() - this.lastTime;
    }
    checkExpiredClear(time?: number) {
        const isExpired = this.isExpired(time);
        if (isExpired && this.cache) {
            this.clearCache();
        }
        return isExpired;
    }
    getTask<T = IRequestTask>(): Config['delay'] extends number ? Promise<T> : T {
        if (this._actuator) {
            return promiseApply(this._actuatorPromise, getTargetTask, [this]);
        }
        return getTargetTask(this);
    }
    off(name: string) {
        if (this._actuatorPromise) {
            promiseApply(this._actuatorPromise, targetOff, [this, name]);
            return;
        }
        targetOff(this, name);
    }
    offAll() {
        if (this._actuatorPromise) {
            promiseApply(this._actuatorPromise, targetOffAll, [this]);
            return;
        }
        targetOffAll(this);
    }
}
