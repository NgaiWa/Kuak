/**
 * @author Fa
 * @copyright Fa 2017
 */
import { secondsToMs } from './date';

export default class Timer {
    callback: AnyFunction | null;

    intervalTime: number;

    state: boolean;

    lastTime: number;

    option: any;

    private _sid: number;

    constructor(callback: AnyFunction, intervalTime?: number, option?: any) {
        intervalTime ||= 0;
        this.intervalTime = intervalTime > 0 ? intervalTime : secondsToMs(1);
        this.callback = callback;
        this.option = option;
        this.state = false;
        this._sid = 0;
        this.lastTime = 0;
    }

    static _call(timer: Timer) {
        timer._run(true);
    }

    private _run(immediate: boolean) {
        const { callback } = this;
        if (!callback) {
            return;
        }
        if (immediate) {
            const now: number = Date.now();
            const { lastTime } = this;
            this.lastTime = now;
            const diffTime = now - lastTime;
            if (callback(diffTime, now, lastTime, this.option, this) === false) {
                this.stop();
                return;
            }
        }
        this._sid = <any>setTimeout(Timer._call, this.intervalTime, this);
    }

    start(immediate = true) {
        this.state = true;
        clearTimeout(this._sid);
        this.lastTime = Date.now();
        this._run(immediate);
    }

    stop() {
        this.state = false;
        clearTimeout(this._sid);
    }

    dispose(beforeDispose?: AnyFunction) {
        if (beforeDispose) {
            beforeDispose(this);
        }
        this.callback = this.option = null;
    }
}
