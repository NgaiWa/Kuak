/**
 * @author Fa
 * @copyright Fa 2017
 */

export const isFunction = (f: any) => typeof f === 'function';
export const isObject = (o: any) => o != null && typeof o === 'object';
export const isArray = Array.isArray;
export const isArrayLike = (a: any) => {
    return isString(a) || isArray(a) || (isObject(a) && isInt(a.length) && isPositive(a.length));
};
export const notArrayObject = (o: any) => isObject(o) && !isArray(o);
export const isString = (s: any) => typeof s === 'string';
export const notEmptyString = (s: any) => typeof s === 'string' && s !== '';
export const isBool = (b: any) => typeof b === 'boolean';
export const isNumber = (n: any) => typeof n === 'number';
export const isNumeral = (n: any) => isNumber(n) && isFinite(n);
export const isInt = (n: any) => isNumeral(n) && Math.abs(n) === n;
export const isFloat = (n: any) => isNumeral(n) && Math.abs(n) !== n;
export const isPositive = (n: any) => n >= 0;
export const isNegative = (n: any) => n < 0;
export const isUndefine = (o: any) => o === undefined;
export const isNull = (o: any) => o === null;
export const asNull = (o: any) => o == null;
export const isEmpty = (o: any) => o == null || o === '';
export const isNil = (o: any) => isEmpty(o) || o === 0 || o !== o;

export const isTypes = function (o: any, types: string[]) {
    return o != null && isTypesOf(o, types);
};
export const typeOf = function (o: any) {
    return o == null ? 'null' : typeof o;
};
const oToString = Object.prototype.toString;
export const TypeOf = function (o: any): string {
    return oToString.call(o).slice(8, -1);
};
export const isTypesOf = function (o: any, types: string[]) {
    return types.indexOf(typeOf(o)) !== -1;
};
export const exactTypeOf = function (o: any) {
    const type = o == null ? 'null' : typeof o;
    return type === 'number'
        ? Number.isNaN(o)
            ? 'nan'
            : Number.isFinite(o)
              ? 'infinity'
              : type
        : type;
};
export const isExactTypesOf = function (o: any, types: string[]) {
    return types.indexOf(exactTypeOf(o)) !== -1;
};
export const isObjOrFunc = function (o: any) {
    return isTypes(o, ['object', 'function']);
};
export const isStrOrNum = function (o: any) {
    return isTypes(o, ['string', 'number']);
};
export const Type = {
    isFunction: isFunction as (f: any) => f is AnyFunction,
    isObject: isObject as (o: any) => o is object,
    isString: isString as (s: any) => s is string,
    isNumber: isNumber as (n: any) => n is number,
    isBool: isBool as (b: any) => b is boolean,
    isArray: isArray as (a: any) => a is any[],
};
