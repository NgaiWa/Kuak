/**
 * @author Fa
 * @copyright Fa 2017
 */
export class WordMap {
    bytes: number[];
    source: string;
    constructor(bytes?: number[], source?: string) {
        this.bytes = bytes || [];
        this.source = source || '';
    }
    toString() {
        return bytesToString(this.bytes);
    }
    toHex() {
        return bytesToHex(this.bytes);
    }
}
export const bytesToString = function (bytes: number[]) {
    let string = '';
    const fromCharCode = String.fromCharCode;
    for (let i = 0; i < bytes.length; i++) {
        string += fromCharCode(bytes[i]);
    }
    return string;
};
export const bytesToHex = function (bytes: number[]) {
    const hex: string[] = [];
    for (let i = 0, l = bytes.length; i < l; i++) {
        hex.push(bytes[i].toString(16));
    }
    return hex;
};
export const stringToBytes = function (str: string) {
    const bytes = [];
    for (let i = 0; i < str.length; i++) {
        bytes.push(str.charCodeAt(i));
    }
    return bytes;
};

export const getCodeByString = function (str: string, i: number) {
    return str.charCodeAt(i);
};
export const getCodeByArray = function (arr: number[], i: number) {
    return arr[i];
};
export const HanZiDict = {
    start: 0x4e00,
    end: 0x9fa5,
};
export const PinYinDict = {
    PY1: 'ABCDEFGHJKLMNOPQRSTWXYZ',
    HZ1: '阿丷嚓咑妸发旮哈丌咔垃呣拏喔趴七呥仨鉈屲夕丫帀',
};
export const getFirstPinYin = function (char: string) {
    if (!char) {
        return char;
    }
    char = char.trim().charAt(0);
    const code = char.charCodeAt(0);
    const { start, end } = HanZiDict;
    if (code >= start && code <= end) {
        const HZ = PinYinDict.HZ1;
        for (let i = 0, l = HZ.length; i < l; i++) {
            const a = HZ[i],
                b = HZ[i + 1];
            if (char.localeCompare(a) > -1 && (!b || char.localeCompare(b) === -1)) {
                return PinYinDict.PY1[i];
            }
        }
    }
    return char.toUpperCase();
};

const TextEncoder = typeof globalThis !== 'undefined' ? (globalThis as any).TextEncoder : null;
const TextDecoder = typeof globalThis !== 'undefined' ? (globalThis as any).TextDecoder : null;
export const unicodeToUTF8 = function (source: string, returnString?: boolean) {
    const bytes = [];
    const length = source.length;
    if (length) {
        const getCode = typeof source === 'string' ? getCodeByString : getCodeByArray;
        for (
            let i = 0,
                _0x100_gt_ASCII = 0x100,
                _0x00 = 0x00,
                _0x10000 = 0x10000,
                _0x1000000 = 0x1000000,
                _0x100000000 = 0x100000000,
                _0xff = 0xff,
                _8bit = 8,
                _16bit = 16,
                _24bit = 24,
                code;
            i < length;
            i++
        ) {
            code = getCode(source as any, i);
            if (code < _0x100_gt_ASCII) {
                bytes.push(_0x00, code);
            } else if (code < _0x10000) {
                bytes.push((code >> _8bit) & _0xff, code & _0xff);
            } else if (code < _0x1000000) {
                bytes.push((code >> _16bit) & _0xff, (code >> _8bit) & _0xff, code & _0xff);
            } else if (code < _0x100000000) {
                bytes.push(
                    (code >> _24bit) & _0xff,
                    (code >> _16bit) & _0xff,
                    (code >> _8bit) & _0xff,
                    code & _0xff,
                );
            }
        }
    }
    return returnString ? bytesToString(bytes) : bytes;
};
export const utf8ToUnicode = function (source: string, returnBytes?: boolean) {
    const bytes = [];
    const _8bit = 8;
    const length = source.length;
    if (length) {
        const getCode = typeof source === 'string' ? getCodeByString : getCodeByArray;
        for (let i = 0; i < source.length; i++) {
            // UNICODE is 2-byte encoding that reads two bytes at a time
            bytes.push((getCode(source as any, i) << _8bit) | getCode(source as any, ++i));
        }
    }
    return returnBytes ? bytes : bytesToString(bytes);
};
export const encodeUTF8 = function (source: string, returnString?: boolean) {
    const bytes = [],
        isStr = typeof source === 'string';
    if (isStr && TextEncoder) {
        bytes.push(...new TextEncoder().encode(source));
    } else {
        const getCode = isStr ? getCodeByString : getCodeByArray;
        let u = 0,
            d = 0,
            i = 0;
        const l = source.length,
            _6 = 6,
            _1 = 1,
            _2 = 2,
            _4 = 4,
            _12 = 12,
            _18 = 18,
            _0x7F = 0x7f,
            _0x07FF = 0x07ff,
            _0x1F = 0x1f,
            _0xC0 = 0xc0,
            _0x3F = 0x3f,
            _0x80 = 0x80,
            _0xFFFF = 0xffff,
            _0xD800 = 0xd800,
            _0xDBFF = 0xdbff,
            _0x0F = 0x0f,
            _0x07 = 0x07,
            _0xF0 = 0xf0,
            _0x30 = 0x30,
            _0xF = 0xf,
            _0xE0 = 0xe0,
            _0x10FFFF = 0x10ffff;
        while (i < l) {
            const c = getCode(source as any, i++);
            if (c <= _0x7F) {
                // [1]
                // 00000000 0zzzzzzz
                bytes.push(c); // 0zzz zzzz (1st)
            } else if (c <= _0x07FF) {
                // [2]
                // 00000yyy yyzzzzzz
                bytes.push(
                    ((c >>> _6) & _0x1F) | _0xC0, // 110y yyyy (1st)
                    (c & _0x3F) | _0x80,
                ); // 10zz zzzz (2nd)
            } else if (c <= _0xFFFF) {
                // [3] or [5]
                if (c >= _0xD800 && c <= _0xDBFF) {
                    // [5] Surrogate Pairs
                    // 110110UU UUwwwwxx 110111yy yyzzzzzz
                    d = getCode(source as any, i++);
                    u = ((c >>> _6) & _0x0F) + _1; // 0xUUUU+1 -> 0xuuuuu
                    bytes.push(
                        ((u >>> _2) & _0x07) | _0xF0, // 1111 0uuu (1st)
                        ((u << _4) & _0x30) | _0x80 | ((c >>> _2) & _0xF), // 10uu wwww (2nd)
                        ((c << _4) & _0x30) | _0x80 | ((d >>> _6) & _0xF), // 10xx yyyy (3rd)
                        (d & _0x3F) | _0x80,
                    ); // 10zz zzzz (4th)
                } else {
                    // xxxxyyyy yyzzzzzz
                    bytes.push(
                        ((c >>> _12) & _0x0F) | _0xE0, // 1110 xxxx (1st)
                        ((c >>> _6) & _0x3F) | _0x80, // 10yy yyyy (2nd)
                        (c & _0x3F) | _0x80,
                    ); // 10zz zzzz (3rd)
                }
            } else if (c <= _0x10FFFF) {
                // [4]
                // 000wwwxx xxxxyyyy yyzzzzzz
                bytes.push(
                    ((c >>> _18) & _0x07) | _0xF0, // 1111 0www (1st)
                    ((c >>> _12) & _0x3F) | _0x80, // 10xx xxxx (2nd)
                    ((c >>> _6) & _0x3F) | _0x80, // 10yy yyyy (3rd)
                    (c & _0x3F) | _0x80,
                ); // 10zz zzzz (4th)
            }
        }
    }
    return returnString ? bytesToString(bytes) : bytes;
};
export const decodeUTF8 = function (source: string, returnBytes?: boolean) {
    const bytes = [],
        isStr = typeof source === 'string';

    if (!isStr && !returnBytes && TextDecoder) {
        return new TextDecoder().decode(new Uint8Array(source as any));
    } else {
        let i,
            c,
            d,
            e,
            f,
            u,
            w,
            x,
            y,
            z = (i = c = d = e = f = u = w = x = y = 0);
        const getCode = isStr ? getCodeByString : getCodeByArray,
            l = source.length,
            _0x3F = 0x3f,
            _0x03 = 0x03,
            _0xF0 = 0xf0,
            _0x0F = 0x0f,
            _0x80 = 0x80,
            _0xE0 = 0xe0,
            _0x1F = 0x1f,
            _0xF5 = 0xf5,
            _0x07 = 0x07,
            _0xD8 = 0xd8,
            _0xDC = 0xdc,
            _6 = 6,
            _12 = 12,
            _2 = 2,
            _4 = 4,
            _1 = 1;

        while (i < l) {
            c = getCode(source as any, i++);
            if (c < _0x80) {
                // [1] 0x00 - 0x7F (1 byte)
                bytes.push(c);
            } else if (c < _0xE0) {
                // [2] 0xC2 - 0xDF (2 byte)
                d = getCode(source as any, i++);
                bytes.push(((c & _0x1F) << _6) | (d & _0x3F));
            } else if (c < _0xF0) {
                // [3] 0xE0 - 0xE1, 0xEE - 0xEF (3 bytes)
                d = getCode(source as any, i++);
                e = getCode(source as any, i++);
                bytes.push(((c & _0x0F) << _12) | ((d & _0x3F) << _6) | (e & _0x3F));
            } else if (c < _0xF5) {
                // [4] 0xF0 - 0xF4 (4 bytes)
                d = getCode(source as any, i++);
                e = getCode(source as any, i++);
                f = getCode(source as any, i++);
                u = (((c & _0x07) << _2) | ((d >> _4) & _0x03)) - _1;
                w = d & _0x0F;
                x = (e >> _4) & _0x03;
                z = f & _0x3F;
                bytes.push(_0xD8 | (u << _6) | (w << _2) | x, _0xDC | (y << _4) | z);
            }
        }
    }
    return returnBytes ? bytes : bytesToString(bytes);
};
