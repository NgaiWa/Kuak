/**
 * @author Fa
 * @copyright Fa 2017
 */
import { TypeOf } from './type';

const eachObject = function (
    original: any,
    init: (original: any) => any,
    originalValues: any[],
    cloneValues: any[],
) {
    const cloned = checkExists(original, originalValues, cloneValues);
    if (cloned) {
        return cloned;
    }
    const clone = init(original);
    addToValues(originalValues, original, cloneValues, clone);
    const keys = Object.keys(original);
    for (let i = 0, l = keys.length; i < l; i++) {
        clone[i] = _clone(original[keys[i]], originalValues, cloneValues);
    }
    return clone;
};
const checkExists = function (value: any, originalValues: any[], cloneValues: any[]) {
    return cloneValues[originalValues.indexOf(value)];
};
const addToValues = function (
    originalValues: any[],
    original: any,
    cloneValues: any[],
    clone: any,
) {
    originalValues.push(original);
    cloneValues.push(clone);
    return clone;
};
const shallowHandler = function (original: any, init: (original: any) => any) {
    return Object.assign(init(original), original);
};
const shallowKits = {
    Array: {
        handler: shallowHandler,
        init() {
            return [];
        },
    },
    Date: {
        handler(original: any) {
            return new Date(original);
        },
    },
    Object: {
        handler() {},
        init() {
            return {};
        },
    },
    any: {
        handler(value: any) {
            return value;
        },
    },
};
const kits = {
    ...shallowKits,
    Array: {
        init: shallowKits.Array.init,
        handler: eachObject,
    },
    Object: {
        init: shallowKits.Object.init,
        handler: eachObject,
    },
};
const getKit = (kits: any, source: any) => {
    const kit =
        kits[TypeOf(source) as 'Object'] || kits[typeof source === 'object' ? 'Object' : 'any'];
    return kit;
};
const _clone = function (source: any, originalValues: any[], cloneValues: any[]) {
    const kit = getKit(kits, source);
    return kit.handler(source, kit.init, originalValues, cloneValues);
};
export const clone = function <V>(source: V): V {
    return _clone(source, [], []) as any;
};

export const shallowClone = function <V>(source: V): V {
    const kit = getKit(shallowKits, source);
    return kit.handler(source, kit.init);
};
