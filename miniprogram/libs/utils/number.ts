import { isNumber } from './type';

/**
 * @author Fa
 * @copyright Fa 2017
 */
export const toNumber = function (value: any): number {
    value = isNumber(value) ? value : parseFloat(value);
    return Number.isNaN(value) ? 0 : value;
};

export const add = function (value: number, ...values: number[]) {
    return values.reduce(function (accumulator, currentValue) {
        return accumulator + (+currentValue || 0);
    }, value);
};
export const subtract = function (value: number, ...values: number[]) {
    return values.reduce(function (accumulator, currentValue) {
        return accumulator - (+currentValue || 0);
    }, value);
};

const toDigits = function (name: keyof typeof Math, value: number, digits?: number) {
    const method = Math[name as 'round'];
    if (!digits || !isNumber(digits)) {
        return method(value);
    }
    digits = Math.pow(10, digits);
    return method(value * digits) / digits;
};

export const round = function (value: number, digits?: number) {
    return toDigits('round', value, digits);
};
export const fround = function (value: number, digits?: number) {
    return toDigits('fround', value, digits);
};
export const floor = function (value: number, digits?: number) {
    return toDigits('floor', value, digits);
};
export const ceil = function (value: number, digits?: number) {
    return toDigits('ceil', value, digits);
};
export const getSign = function (value: number) {
    return value < 0 ? '-' : '+';
};
export const toSignString = function (value: number) {
    return (value < 0 ? '' : '+') + value;
};
export const intlFormat = function (
    value: number,
    locales?: string | string[] | undefined,
    options?: Intl.NumberFormatOptions | undefined,
) {
    const nf = new Intl.NumberFormat(locales, options);
    return nf.format(value);
};
