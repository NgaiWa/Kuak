/**
 * @author Fa
 * @copyright Fa 2017
 */
import { noop, returnArg, returnFalse, returnTrue, toFalseResult, toTrueResult } from './function';

export { noop, returnArg, returnFalse, returnTrue, toFalseResult, toTrueResult };

export const caught = <T = any>(value: T) => {
    return Promise.resolve(value).catch<T>(returnArg);
};

export const caughtVoid = () => {
    return Promise.resolve();
};
export const caughtTrue = () => {
    return Promise.resolve(true);
};
export const caughtNull = () => {
    return Promise.resolve(null);
};
export const caughtFalse = () => {
    return Promise.resolve(false);
};
export const consoleLog = <T = any>(value: T) => {
    console.log(value);
    return value;
};
export const consoleError = <T = any>(value: T) => {
    console.error(value);
    return value;
};
export const consoleThen = <T = any>(value: T) => {
    return caught<T>(value).then<T>(consoleLog);
};
export const consoleCatch = <T = any>(value: T) => {
    return uncaught(uncaught(value).catch(consoleError));
};

export const uncaught = <T = any>(value: T) => {
    return Promise.reject(value);
};
export const uncaughtVoid = () => {
    return Promise.reject();
};
export const uncaughtTrue = () => {
    return uncaught(true);
};
export const uncaughtFalse = () => {
    return uncaught(false);
};

export const uncaughtNull = () => {
    return uncaught(null);
};
export const uncaughtResult = <T = any>(value: T) => {
    return uncaught(toFalseResult(value));
};
export const uncaughtError = <E = any>(error: E) => {
    return uncaught({ error });
};

export const rejectFalse = () => uncaught(false);

export const promiseApply = function (
    promise: Promise<any> | any,
    func: AnyFunction,
    args?: any[],
) {
    promise = promise instanceof Promise ? promise : Promise.resolve(Promise);
    promise = promise
        .then(toTrueResult)
        .catch(toFalseResult)
        .then((res: any) => {
            res = args ? func(...args, res) : func(res);
            func = args = null as any;
            return res;
        });
    promise.catch(console.error);
    return promise;
};
export const promiseCall = function (promise: Promise<any>, func: AnyFunction, ...args: any[]) {
    return promiseApply(promise, func, args);
};
