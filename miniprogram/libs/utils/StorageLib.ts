import { lib } from '../app/env';
import EventTarget from '../polyfill/EventTarget';
import { oHasOwn } from './object';
import { isArray, isObject } from './type';

const {
    getStorage: asyncGet,
    setStorage: asyncSet,
    getStorageInfo: asyncGetInfo,
    removeStorage: asyncRemove,
    clearStorage: asyncClear,
    getStorageSync: get,
    setStorageSync: set,
    removeStorageSync: remove,
    clearStorageSync: clear,
    getStorageInfoSync: getInfo,
} = lib;
const asyncGetByName = function (name: string) {
    return asyncGet({ key: name });
};
const asyncRemoveByName = function (name: string) {
    return asyncRemove({ key: name });
};
const getDecrypt = function (name: string) {
    return asyncGet({ key: name, encrypt: true });
};
const execAsyncGet = function (method: AnyFunction, name: string | string[]) {
    return isArray(name) ? Promise.all(name.map(method)) : method(name as string);
};
export const toSetData = function (name: string | AnyObject, value?: any) {
    return isObject(name) ? (name as AnyObject) : ({ [name as string]: value } as AnyObject);
};
const execAsyncSet = function (name: string | AnyObject, value?: any, encrypt?: boolean) {
    const data = toSetData(name, value);
    const asyncArr = [];
    for (const key in data) {
        if (oHasOwn(data, key)) {
            asyncArr.push(asyncSet({ key, data: data[key], encrypt }));
        }
    }
    return Promise.all(asyncArr).then(Boolean);
};
const toNameList = function (name: string | string[]): string[] {
    return isArray(name) ? name : [name];
};
const isSafeSize = function (info: any, inputSize?: number) {
    return info != null && info.limitSize - info.currentSize > (inputSize ?? 0);
};
export const localStorageLib = {
    asyncGet,
    asyncSet,
    asyncGetInfo,
    asyncRemove,
    asyncClear,
    get,
    set,
    getInfo,
    remove,
    clear,
    asyncGetByName,
    asyncRemoveByName,
    getDecrypt,
    execAsyncGet,
    execAsyncSet,
    toSetData,
    toNameList,
    isSafeSize,
    event: null as unknown as EventTarget,
};
// @ts-expect-error
if (typeof window !== 'undefined') {
    localStorageLib.event = new EventTarget();

    // @ts-expect-error
    (window as EventTarget).addEventListener('storage', function (event: any) {
        // @ts-expect-error
        if (event.storageArea === localStorage) {
            localStorageLib.event.dispatchEvent('change', { [event.key]: event.newValue });
        }
        // @ts-expect-error
        else if (event.sessionStorage === sessionStorage) {
            //
        }
    });
}
