/**
 * @author Fa
 * @copyright Fa 2017
 */
import { noop } from './function';

export class AsyncTask {
    private __refPromise: Promise<any>;
    protected __ref!: any;
    constructor(promise: Promise<any>) {
        let _this = this;
        this.__refPromise = promise.catch(noop).then((self) => {
            _this.__ref = self;
            _this = _this.__refPromise = null as any;
        });
    }
    protected _exec(funcName: string, ...args: any[]) {
        if (this.__ref) {
            this.__ref[funcName]?.(...args);
        } else if (this.__refPromise) {
            let _this = this;
            this.__refPromise.finally(() => {
                _this.__ref[funcName]?.(...args);
                _this = args = null as any;
            });
        }
    }
    cancel() {
        this._exec('cancel');
    }
    abort() {
        this._exec('abort');
    }
}
