/**
 * @author Fa
 * @copyright Fa 2017
 */
export default class ExecIntervalDetector {
    // 最后一次执行毫秒时间
    lastTime!: number;

    // 间隔毫秒
    interval: number;

    constructor(interval: number) {
        this.interval = interval;
    }

    get executable() {
        return this.check();
    }

    check() {
        const now = Date.now();
        if (now - this.lastTime >= this.interval) {
            this.lastTime = now;
            return true;
        }
        return false;
    }
}
