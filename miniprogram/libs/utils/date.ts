/**
 * @author Fa
 * @copyright Fa 2017
 */
// 获取常用时间
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';

export const LAST_7_DAYS = [
    dayjs().subtract(7, 'day').format('YYYY-MM-DD'),
    dayjs().subtract(1, 'day').format('YYYY-MM-DD'),
];

export const LAST_30_DAYS = [
    dayjs().subtract(30, 'day').format('YYYY-MM-DD'),
    dayjs().subtract(1, 'day').format('YYYY-MM-DD'),
];

/**
 * 用来处理一些常用的时间计算
 */

export const getNow = function () {
    return dayjs();
};
const parseDate = dayjs;
export { dayjs };

export const secondMs = 1000;
export const secondsToMs = function (seconds: number) {
    return seconds * secondMs;
};
export const minuteMs = secondsToMs(60);
export const minutesToMs = function (minutes: number) {
    return minutes * minuteMs;
};
export const hourMs = minutesToMs(60);
export const hoursToMs = function (hours: number) {
    return hours * hourMs;
};
export const dayMs = hoursToMs(24);
export const daysToMs = function (days: number) {
    return days * dayMs;
};
export const weekMs = dayMs * 7;
export const ssMonthMs = dayMs * 28; // 小小月
export const sMonthMs = dayMs * 29; // 小月
export const monthMs = dayMs * 30; // 通常月
export const bMonthMs = dayMs * 31; // 大月
export const yearMs = dayMs * 365; // 通常年
export const leapYearMs = dayMs * 366; // 闰年

const msToInfo = function (
    ms: number,
    byMs: number,
    name: string,
    to: AnyFunction,
): {
    exactValue: number;
    remainder: number;
    origin: number;
    value: number;
    milliseconds?: number;
    seconds?: number;
    minutes?: number;
    hours?: number;
    days?: number;
    weeks: number;
    months?: number;
    years?: number;
} {
    const exactValue: number = ms / byMs;
    const remainder = ms % byMs;
    return {
        ...to(remainder),
        exactValue,
        remainder,
        origin: ms,
        value: +exactValue.toFixed(2),
        [name]: Math.floor(exactValue),
    };
};

const msToMsInfo = function (ms: number) {
    return { milliseconds: ms };
};
export const msToSecondsInfo = function (ms: number) {
    return msToInfo(ms, secondMs, 'seconds', msToMsInfo);
};
export const msToMinutesInfo = function (ms: number) {
    return msToInfo(ms, minuteMs, 'minutes', msToSecondsInfo);
};
export const msToHoursInfo = function (ms: number) {
    return msToInfo(ms, hourMs, 'hours', msToMinutesInfo);
};
export const msToDaysInfo = function (ms: number) {
    return msToInfo(ms, dayMs, 'days', msToHoursInfo);
};
export const msToMonthsInfo = function (ms: number) {
    return msToInfo(ms, monthMs, 'months', msToDaysInfo);
};
export const msToYearsInfo = function (ms: number) {
    return msToInfo(ms, yearMs, 'years', msToMonthsInfo);
};
export const msToWeeksInfo = function (ms: number) {
    return msToInfo(ms, weekMs, 'weeks', msToDaysInfo);
};
export const getTimeSpan = function (startDate: any, endDate?: any): number {
    if (startDate == null && endDate == null) {
        return 0;
    }
    return (
        parseDate(endDate ?? new Date()).valueOf() - parseDate(startDate ?? new Date()).valueOf()
    );
};

export const dateKit = {
    get nowDate() {
        return new Date();
    },
    get now() {
        return getNow();
    },
    range: {
        get currentMonth() {
            return [getNow().date(1), getNow().add(1, 'month').date(0)];
        },
    },
    parse: parseDate,
};
export default dateKit;

export const toDateJValue = function (value: any): Dayjs | void {
    if (value == null) {
        return parseDate(value);
    }
};
export const toDateValue = function (value: any): Date | void {
    if (value == null) {
        return parseDate(value).toDate();
    }
};

export const toDateString = function (value: any): string {
    return value ? parseDate(value).format('YYYY-MM-DD') : '';
};
export const toTimeString = function (value: any): string {
    return value ? parseDate(value).format('HH:mm:ss') : '';
};
export const toDateTimeString = function (value: any): string {
    return value ? parseDate(value).format('YYYY-MM-DD HH:mm:ss') : '';
};
