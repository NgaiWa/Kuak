/**
 * @author Fa
 * @copyright Fa 2017
 */

export const returnArg = <T = any>(arg: T) => arg;
export const noop: (...args: any) => void = () => {};
export const returnTrue = () => true;
export const returnFalse = () => false;

export const toResult = function <V = any, E = Error, S extends boolean = boolean>(
    state: S,
    value?: V,
    error?: E,
): IResult<V, E, S> {
    return arguments.length > 2 ? { state, value: value!, error } : { state, value: value! };
};
export const toTrueResult = function <V = any>(value: V) {
    return toResult<V>(true, value);
};
export const toFalseResult = function <V = any>(value: V) {
    return toResult<V>(false, value);
};
export const toErrorResult = function <E = Error>(error: E) {
    return toResult<undefined, E>(false, undefined, error);
};
export const checkFunc = function (func: any): AnyFunction | undefined {
    return typeof func === 'function' ? func : undefined;
};
const _getInfoFunc = function (this: any) {
    return this.thisArg?.[this.name];
};
export class Callee implements ICallee {
    func!: AnyFunction;
    constructor(
        thisArg: any,
        func: ICallee['name'] | ICallee['func'],
        args?: any[],
        once?: boolean,
    ) {
        if (func == null) {
            return Object.assign(this, {
                thisArg,
                func: func as any,
                name: undefined,
                args,
                invalid: true,
                once,
            });
        }
        if (typeof func === 'function') {
            return Object.assign(this, { thisArg, func, name: undefined, args, once });
        }
        if (thisArg == null) {
            return Object.assign(this, {
                thisArg,
                args,
                func: undefined,
                name: func,
                invalid: true,
                once,
            });
        }
        return Object.defineProperty(
            Object.assign(this, { thisArg, name: func, args, once }),
            'func',
            {
                get: _getInfoFunc,
                set: noop,
            },
        );
    }
    static create(
        thisArg?: any,
        func?: ICallee['name'] | ICallee['func'],
        args?: any[],
        once?: boolean,
    ) {
        return new this(thisArg, func, args, once);
    }
    static clone(callee: ICallee, assignProps: Partial<ICallee>) {
        return Object.assign(this.create(), callee, assignProps);
    }
}

export const apply = function <V = any>(callee: ICallee | AnyFunction, args?: any[]): V {
    if (!callee) {
        return undefined as unknown as V;
    }
    if (typeof callee === 'function') {
        return callee.apply(args || []);
    }
    if (callee.called && callee.once) {
        return callee.value;
    }
    const func = callee.func;
    const value: V =
        typeof func === 'function'
            ? func.apply(callee.thisArg, (callee.args || []).concat(args))
            : undefined;
    callee.called = true;
    if (callee.once) {
        callee.value = value;
    }
    return value;
};
export const call = function <V = any>(callee: ICallee | AnyFunction, ...args: any[]): V {
    return apply(callee, args);
};
export const tryApply = function <V = any>(callee: ICallee | AnyFunction, args?: any[]) {
    try {
        return toResult<V>(true, apply(callee, args));
    } catch (e) {
        console.error(e);
        return toResult<V>(false, undefined, e as Error);
    }
};
export const tryCall = function <V = any>(callee: ICallee | AnyFunction, ...args: any[]) {
    return tryApply<V>(callee, args);
};
export const applyThisFunc = function <V = any>(thisArg: any, func: AnyFunction, args?: any[]): V {
    return typeof func === 'function'
        ? func.apply(thisArg, args || [])
        : (undefined as unknown as V);
};
export const callThisFunc = function <V = any>(thisArg: any, func: AnyFunction, ...args: any[]) {
    return applyThisFunc<V>(thisArg, func, args);
};
export const applyFunc = function <V = any>(this: any, func: AnyFunction, args?: any[]) {
    return applyThisFunc<V>(this, func, args);
};
export const callFunc = function <V = any>(this: any, func: AnyFunction, ...args: any[]) {
    return applyThisFunc<V>(this, func, args);
};
export const tryApplyThisFunc = function <V = any>(
    thisArg: any,
    func: AnyFunction,
    args?: any[],
): V | void {
    try {
        return func.apply(thisArg, args || []);
    } catch (e) {
        console.error(e);
    }
};
export const tryCallThisFunc = function <V = any>(thisArg: any, func: AnyFunction, ...args: any[]) {
    return tryApplyThisFunc<V>(thisArg, func, args);
};
export const tryApplyFunc = function <V = any>(
    this: any,
    func: AnyFunction,
    args?: any[],
): V | void {
    return tryApplyThisFunc<V>(this, func, args);
};
export const tryCallFunc = function <V = any>(this: any, func: AnyFunction, ...args: any[]) {
    return tryApplyThisFunc<V>(this, func, args);
};

export const checkDeepLoop = function (
    deepNum: -1 | number,
    current: number,
    preventInfiniteLoops?: any[],
    loopValue?: any,
) {
    const hasPrevent = preventInfiniteLoops != null;
    if (
        (deepNum < 0 || current <= deepNum) &&
        (!hasPrevent || !preventInfiniteLoops!.includes(loopValue))
    ) {
        if (hasPrevent) {
            preventInfiniteLoops!.push(loopValue);
        }
        return true;
    }
    return false;
};
export const notInfiniteLoops = function name(preventInfiniteLoops: any[], loopValue: any) {
    return !preventInfiniteLoops || !preventInfiniteLoops.includes(loopValue);
};

export const createCallQueue = function (): ICalleeQueue {
    return [];
};
export const createFuncQueue = function (): IFuncQueue {
    return [];
};

export const boundThis = function <T extends { [n: string]: any }>(
    this: any,
    thisArg: T,
    callName: string,
    boundName?: string,
): (...args: any) => any {
    boundName = boundName || `_${callName}Bound`;
    if (!thisArg[boundName]) {
        (thisArg as any)[boundName] = thisArg[callName].bind(this);
    }
    return thisArg[boundName];
};
