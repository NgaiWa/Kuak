/**
 * @author Fa
 * @copyright Fa 2017
 */

import { isArray } from './type';

export const unionExecValues = <T>(array: T[], methodName: 'push' | 'unshift', values: any[]) => {
    if (array && values) {
        for (let i = 0, value, l = values.length; i < l; i++) {
            value = values[i];
            if (!array.includes(value)) {
                array[methodName](value);
            }
        }
    }
    return array;
};
export const unionExec = <T>(array: T[], methodName: 'push' | 'unshift', ...values: any[]) => {
    return unionExecValues(array, methodName, values);
};
export const unionPush = <T>(array: T[], ...values: T[]): T[] => {
    return unionExecValues(array, 'push', values);
};
export const unionUnshift = <T>(array: T[], ...values: T[]): T[] => {
    return unionExecValues(array, 'unshift', values);
};
export const union = <T>(...arrays: Array<ArrayLike<T> | null | undefined>): T[] => {
    const values: T[] = [];
    for (let i = 0, l = arrays.length; i < l; i++) {
        const array = arrays[i];
        if (isArray(array)) {
            unionPush(values, ...(array as any[]));
        }
    }
    return values;
};

export const add = unionPush;
export const removeValues = function (arr: any[], values: any[]) {
    if (arr && values) {
        for (let i = 0, l = values.length, value: any, index: number; i < l; ) {
            value = values[i];
            index = arr.findIndex((v) => v === value || (v !== v && value !== value));
            value = null;
            if (index !== -1) {
                arr.splice(index, 1);
                i++;
            }
        }
    }
    return arr;
};
export const remove = function (arr: any[], ...values: any[]) {
    return removeValues(arr, values);
};

export const removeAt = function (arr: any[], index: number) {
    if (arr) {
        arr.splice(index < 0 ? arr.length + index : index, 1);
    }
    return arr;
};
export const pad = function (arr: any[], maxLength: number, fillValue?: any, isPadEnd?: boolean) {
    if (arr) {
        const fn = isPadEnd ? 'push' : 'unshift';
        while (arr.length < maxLength) {
            arr[fn](fillValue);
        }
    }
    return arr;
};
export const padStart = function (arr: any[], maxLength: number, fillValue?: any) {
    return pad(arr, maxLength, fillValue);
};
export const padEnd = function (arr: any[], maxLength: number, fillValue?: any) {
    return pad(arr, maxLength, fillValue, true);
};
