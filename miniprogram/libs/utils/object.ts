/**
 * @author Fa
 * @copyright Fa 2017
 */
import '../polyfill/Object';

import { toResult } from './function';
import { isArray, isObject, isObjOrFunc } from './type';

export const oHasOwn = Object.hasOwn;
export const hasOwn = function (o: object, k: PropertyKey): boolean {
    return o != null && oHasOwn(o, k);
};
export const assign: typeof Object.assign = function (target: any, ...sources: any) {
    try {
        Object.assign(target, ...sources);
    } catch (e) {
        console.error(e);
    }
    return target;
};
export const hasProp = function (o: any, propName: string | number | symbol) {
    return o == null ? false : isObjOrFunc(o) ? propName in o : propName in Object(o);
};
export const hasIn = function (o: any, propName: string | number | symbol) {
    return o == null ? false : isObjOrFunc(o) && propName in o;
};
export const setValue = function (o: any, prop: string, value: any) {
    try {
        o[prop] = value;
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};
export const getKeys = (o: any) => {
    return o == null ? [] : Object.keys(o);
};

export const createObject = function (o?: any) {
    return Object.create(o == null ? null : o);
};
export const getByKeys = (o: any, keys: string[]) => {
    const map: { [k: string]: any } = {};
    if (!keys) {
        return map;
    }
    o = o == null ? createObject() : o;
    for (let i = 0, l = keys.length, k; i < l; i++) {
        map[(k = keys[i])] = o[k];
    }
    return map;
};
export const getValuesByKeys = function (o: any, keys: string[]) {
    const values: any[] = [];
    if (!keys) {
        return values;
    }
    o = o == null ? createObject() : o;
    for (let i = 0, l = keys.length; i < l; i++) {
        values[i] = o[keys[i]];
    }
    return values;
};
export const toNormalNamePath = function (namePath: string) {
    return String(namePath ?? '').replace(/\[['"]?(\w+)['"]?\]/g, (_m, n) => '.' + n);
};
export const toNamePathNodes = function (namePath: string) {
    return toNormalNamePath(namePath).split('.');
};
export const inDirPathNodes = function (dirNamePathNodes: string[], namePathNodes: string[]) {
    const result =
        dirNamePathNodes.length > 0 &&
        dirNamePathNodes.every((value, index) => {
            return index < namePathNodes.length && namePathNodes[index] === value;
        });
    namePathNodes = null as any;
    return result;
};
export const inDirPath = function (dirNamePath: string, namePath: string) {
    return inDirPathNodes(toNamePathNodes(dirNamePath), toNamePathNodes(namePath));
};
export const getOwn = function (obj: any, prop: PropertyKey) {
    if (hasOwn(obj, prop)) {
        return obj[prop];
    }
};
export const getValueResultByPathNodes = function (obj: any, namePathNodes: string[]) {
    if (obj == null) {
        return { state: false, value: undefined };
    }
    const names = namePathNodes;
    for (let i = 0, l = names.length, name; i < l; i++) {
        name = names[i];
        obj = obj[name];
        if (obj == null && i < l - 1) {
            return { state: false, value: obj };
        }
    }
    return { state: true, value: obj };
};

export const getValueResultByPath = function (obj: any, namePath: string) {
    return getValueResultByPathNodes(obj, obj == null ? (null as any) : toNamePathNodes(namePath));
};
export const getValueByPath = function (obj: any, namePath: string) {
    return getValueResultByPath(obj, namePath).value;
};
export const getValueByPathNodes = function (obj: any, namePathNodes: string[]) {
    return getValueResultByPathNodes(obj, namePathNodes).value;
};
export const getValuesByPathsNodes = function (obj: any, namePathsNodes: string[][]) {
    const values = (namePathsNodes || []).map((namePathsNodes) =>
        getValueByPathNodes(obj, namePathsNodes),
    );
    obj = null;
    return values;
};
export const getValuesByPaths = function (obj: any, namePaths: string[]) {
    const values = (namePaths || []).map((namePath) => getValueByPath(obj, namePath));
    obj = null;
    return values;
};
export const setValueByPathNodes = function (
    obj: any,
    namePathNodes: string[],
    value: any,
    isMakeToObject?: boolean,
) {
    if (!obj) {
        return;
    }
    isMakeToObject = !!isMakeToObject;
    let node = obj;
    let i = 0;
    for (let l = namePathNodes.length - 1; i < l; i++) {
        node = node[namePathNodes[i]];
        if (node == null || !isObjOrFunc(node)) {
            if (!isMakeToObject) {
                node = null;
                break;
            }
            // obj[nodes[i]] = node = Object(node); // 生成特殊对象
            obj[namePathNodes[i]] = node = {}; // 生成普通的对象
        }
    }
    if (node != null) {
        node[namePathNodes[i]] = value;
    }
};
export const setValueByPath = function (
    obj: any,
    namePath: string,
    value: any,
    isMakeToObject?: boolean,
) {
    if (!obj) {
        return;
    }
    setValueByPathNodes(obj, toNamePathNodes(namePath), value, isMakeToObject);
};
export const setVoid = function (o: any, prop: string): boolean {
    return setValue(o, prop, undefined);
};
export const deleteProps = function (
    o: any,
    props: string | string[] | true,
    nonOwnToVoid?: boolean,
): boolean | void {
    if (o == null || props == null) {
        return;
    }
    if (isObjOrFunc(o)) {
        let state = true;
        const _props: string[] = isArray(props) ? props : props === true ? Object.keys(o) : [props];
        nonOwnToVoid = !!nonOwnToVoid;
        for (
            let i = 0, l = _props.length, prop: string, isOwn: boolean, isAnyIn: boolean;
            i < l;
            i++
        ) {
            prop = _props[i];
            isOwn = oHasOwn(o, prop);
            isAnyIn = prop in o && nonOwnToVoid;
            if (isOwn || isAnyIn) {
                setVoid(o, prop);
            }
            if (isOwn) {
                try {
                    state = delete o[prop] && state;
                } catch (e) {
                    console.log(e);
                }
            }
        }
        return state;
    }
};
export const defineProperty: typeof Object.defineProperty = function (
    o: any,
    prop: PropertyKey,
    desc: any,
) {
    if (desc) {
        try {
            Object.defineProperty(o, prop, desc);
        } catch (e) {
            console.error(e);
        }
    }
    return o;
};
export const getOwnPropNames = function (o: any): string[] {
    return o == null ? [] : Object.getOwnPropertyNames(o);
};
export const getOwnPropKeys = function (o: any): PropertyKey[] {
    if (o == null) {
        return [];
    }
    return [...Object.getOwnPropertyNames(o), ...Object.getOwnPropertySymbols(o)];
};
export const defineProperties: typeof Object.defineProperties = function (
    o: any,
    descMap: PropertyDescriptorMap & ThisType<any>,
) {
    const props: PropertyKey[] = getOwnPropKeys(descMap);
    for (let i = 0, l = props.length, prop; i < l; i++) {
        prop = props[i];
        defineProperty(o, prop, descMap[prop as string]);
    }
    return o;
};
export const getOwnPropDesc = function <UseNilDesc extends boolean = false>(
    o: any,
    prop: PropertyKey,
    useNilDesc?: UseNilDesc,
): UseNilDesc extends true ? PropertyDescriptor : PropertyDescriptor | void {
    return (
        (o != null && Object.getOwnPropertyDescriptor(o, prop)) ||
        (useNilDesc === true ? createObject() : undefined)
    );
};
export const getOwnPropDescMap = function (o: any, props?: PropertyKey[], useNilDesc?: boolean) {
    return (props == null ? getOwnPropKeys(o) : props).reduce(
        (ret, prop) => {
            ret.descriptors[prop as string] = getOwnPropDesc(ret.obj, prop, ret.useNilDesc);
            return ret;
        },
        {
            descriptors: {} as PropertyDescriptorMap & AnyObject,
            obj: o,
            useNilDesc: useNilDesc,
        },
    ).descriptors;
};

export const setDescByKey = (target: any, source: any, key: string, extendDesc?: any) => {
    const desc: any = getOwnPropDesc(source, key, false);
    if (!desc) {
        return target;
    }
    return defineProperty(target, key, extendDesc ? { ...desc, ...extendDesc } : desc);
};
type SetDescByKeys = <T = any, S = any, K extends Partial<keyof S> = keyof S>(
    target: T,
    source: S,
    keys: K,
) => T & { [n in K]: S[n] };
type SetDescByAllKeys = <T = any, S = any>(target: T, source: S) => T & { [n in keyof S]: S[n] };
export const setDescByKeys: SetDescByKeys | SetDescByAllKeys = (
    target: any,
    source: any,
    keys?: string[],
    extendDesc?: any,
) => {
    if (!isObjOrFunc(target) || !isObjOrFunc(source) || keys === null) {
        return target;
    }
    keys = keys ?? Object.keys(source);
    for (let i = 0, l = keys.length; i < l; i++) {
        setDescByKey(target, source, keys[i], extendDesc);
    }
    return target as any;
};
export const setValueByKeys = function (target: AnyObject, keys: PropertyKey[], value: any) {
    if (target && keys) {
        for (let i = 0, l = keys.length; i < l; i++) {
            target[keys[i] as string] = value;
        }
    }
    return target;
};
export const setValuesByKeys = function (target: AnyObject, keys: PropertyKey[], values: any[]) {
    if (target && keys) {
        for (let i = 0, l = keys.length; i < l; i++) {
            target[keys[i] as string] = values[i];
        }
    }
    return target;
};
export const pick = function <T = AnyObject, K extends keyof T = any>(
    source: AnyObject,
    keys: K[],
    doNotCheckIn?: boolean,
): Pick<T, K> {
    const target: any = {};
    if (!isObjOrFunc(source) || !keys) {
        return target;
    }
    doNotCheckIn;
    for (let i = 0, l = keys.length, k: string; i < l; i++) {
        k = keys[i] as string;
        if (hasIn(source, k)) {
            (target as AnyObject)[k] = (source as AnyObject)[k];
        }
    }
    return target;
};
export const pickByAlias = function <S = AnyObject, A extends PickAliasMap = PickAliasMap>(
    source: S,
    aliasMap: A,
    doNotCheckIn?: boolean,
): { [n in keyof A]: any } {
    const target: any = {};
    if (!isObject(aliasMap) || !isObjOrFunc(source)) {
        return target;
    }
    const aliasArr = Object.keys(aliasMap);
    doNotCheckIn = !!doNotCheckIn;
    for (let i = 0, l = aliasArr.length; i < l; i++) {
        const alias = aliasArr[i];
        if (alias == null) {
            continue;
        }
        const key: any = aliasMap[alias];
        const type = typeof alias;
        if (type === 'function') {
            (key as SetAliasBy)(target, alias as string, source as any);
            continue;
        }
        if (type === 'object') {
            if (doNotCheckIn || key.key in (source as AnyObject)) {
                key.toValue((source as any)[key], key, source);
            }
            continue;
        }
        if (doNotCheckIn || key in (source as any)) {
            target[alias] = (source as any)[key];
        }
    }
    return target;
};
export const omit = function <T = AnyObject, K extends keyof T = any>(
    source: T,
    keys: K[],
): Omit<T, K> {
    const target: any = {};
    if (isObjOrFunc(source) && keys) {
        for (const k in source) {
            if ((keys as string[]).indexOf(k) === -1) {
                target[k] = source[k];
            }
        }
    }
    return target;
};
export const setByKeys = function (target: AnyObject, source: AnyObject, keys: PropertyKey[]) {
    if (isObjOrFunc(target) && source && keys) {
        for (let i = 0, l = keys.length, k: string; i < l; i++) {
            k = keys[i] as string;
            (target as AnyObject)[k] = (source as AnyObject)[k];
        }
    }
    return target;
};
export const setByAlias = function (target: AnyObject, source: AnyObject, alias: AliasNameMap) {
    if (isObjOrFunc(target) && source && alias) {
        for (const name in alias) {
            target[name] = source[alias[name] || name];
        }
    }
    return target;
};

export const tryJsonParse = function (
    json: string | object,
    reviver?: (this: any, key: string, value: any) => any,
): any {
    try {
        if (json === '') {
            json = undefined as any;
        }
        return toResult(
            true,
            json == null || typeof json === 'object' ? json : JSON.parse(json, reviver),
        );
    } catch (e) {
        console.error(e);
        return toResult(false, e);
    }
};
export const tryJsonStringify = function (
    json: object,
    replacer?: (this: any, key: string, value: any) => any,
    space?: string | number,
) {
    try {
        return { state: true, value: JSON.stringify(json, replacer, space) };
    } catch (e) {
        return { state: false, error: e, value: '' };
    }
};
export const jsonParse = function (
    json: string | object,
    reviver?: (this: any, key: string, value: any) => any,
) {
    return tryJsonParse(json, reviver).value;
};

export const jsonStringify = function (
    json: object,
    replacer?: (this: any, key: string, value: any) => any,
    space?: string | number,
) {
    return tryJsonStringify(json, replacer, space);
};
