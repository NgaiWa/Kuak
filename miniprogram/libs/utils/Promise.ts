/**
 * @author Fa
 * @copyright Fa 2017
 */
import * as fn from './Promise.fn';
const { noop, returnArg } = fn;
Object.defineProperties(Promise.prototype, {
    caught: {
        value(this: Promise<any>) {
            return this.catch(returnArg);
        },
        configurable: true,
    },
    caughtVoid: {
        value(this: Promise<any>) {
            return this.catch(noop);
        },
        configurable: true,
    },
});

const getPromiseRes = function (value: any, res?: any) {
    return value instanceof Promiser ? value.res : res;
};
const getPromiseResPriority = function (res: any, value: any) {
    if (res !== undefined) {
        return res;
    }
    return getPromiseRes(value);
};
const getPromiseMultiRes = function (values: readonly any[], res?: any) {
    if (res !== undefined) {
        return res;
    }
    const _res = values.map((value) => getPromiseRes(value, res));
    res = null;
    return _res;
};
const assign = Object.assign;
export class Promiser<T = unknown, R = GetPromiseRes<T>>
    extends Promise<T>
    implements IPromise<T, R>
{
    static fn = fn;
    res!: R;
    constructor(
        executor: (
            resolve: (value: T | PromiseLike<T>) => void,
            reject: (reason?: any) => void,
        ) => R,
    ) {
        let _res: R = undefined as unknown as R;
        super((resolve, reject) => {
            _res = executor(resolve, reject);
        });
        this.res = _res;
        _res = executor = null as any;
    }
    then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
    ): this & IPromise<TResult1 | TResult2, R> {
        return assign(super.then(onfulfilled, onrejected), this) as any;
    }
    finally(onfinally?: (() => void) | undefined | null): IPromise<T, R> {
        return assign(super.finally(onfinally), this) as any;
    }
    catch<TResult = never>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
    ): this & IPromise<T | TResult, R> {
        return assign(super.catch(onrejected), this) as any;
    }
    thenR<TResult = T, TR = R>(
        onfulfilled?: ((value: T) => TResult | PromiseLike<TResult>) | undefined | null,
        res?: TR,
    ): this & IPromise<TResult, TR> {
        return assign(super.then(onfulfilled), {
            ...this,
            res: arguments.length > 1 ? res : this.res,
        }) as any;
    }
    catchR<TResult = T, TR = R>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
        res?: TR,
    ): this & IPromise<TResult, TR> {
        return assign(super.then(onrejected), {
            ...this,
            res: arguments.length > 1 ? res : this.res,
        }) as any;
    }

    static allR<T extends readonly unknown[] | readonly [unknown], R = unknown>(
        values: T,
        res?: R,
    ): IPromise<
        {
            -readonly [P in keyof T]: T[P] extends PromiseLike<infer U> ? U : T[P];
        },
        GetPromiseMultiRes<T, R>
    > {
        return assign(super.all(values), { res: getPromiseMultiRes(values, res) }) as any;
    }
    static allSettledR<T extends readonly unknown[] | readonly [unknown], R = unknown>(
        values: T,
        res?: R,
    ): IPromise<
        {
            -readonly [P in keyof T]: PromiseSettledResult<
                T[P] extends PromiseLike<infer U> ? U : T[P]
            >;
        },
        GetPromiseMultiRes<T, R>
    > {
        return assign(super.allSettled(values), {
            res: getPromiseMultiRes(values, res),
        }) as any;
    }
    static raceR<T, R = GetPromiseRes<T>>(
        values: Iterable<T>,
        res?: R,
    ): IPromise<T extends PromiseLike<infer U> ? U : T, R> {
        return assign(super.race(values), {
            res: getPromiseMultiRes(values as any, res),
        }) as any;
    }
    static rejectR<T, R = GetPromiseRes<T>>(reason?: T, res?: R): IPromise<T, R> {
        return assign(super.reject(reason), {
            res: getPromiseResPriority(res, reason),
        }) as any;
    }
    static resolveR<T, R = GetPromiseRes<T>>(value: T, res?: R): IPromise<T, R> {
        return assign(super.resolve(value), {
            res: getPromiseResPriority(res, value),
        }) as any;
    }
}

export default Promiser;
