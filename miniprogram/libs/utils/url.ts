import { concatString, toString } from './string';
import { isObject, isString } from './type';

/**
 * @author Fa
 * @copyright Fa 2017
 */
export const asAbsUrl = function (url: string) {
    return /^([a-z0-9]+:)?[/\\]{2,}/i.test(url);
};
export const isAbsUrl = function (url: string) {
    return /^([a-z0-9]+:)[/\\]{2,}/i.test(url);
};

export const concatUrl = function (url1: string, url2: string) {
    return concatString(url1, url2, '/', /[/\\]+$/, /^[/\\]+/);
};
export const concatBaseUrl = function (baseUrl: string, url: string) {
    return isAbsUrl(url) ? url : concatUrl(baseUrl, url);
};
export const appendSearch = function (url: string, ...params: (AnyObject | string)[]) {
    const index = url.indexOf('?');
    let presetSearch: string;
    if (index === -1) {
        presetSearch = '';
    } else {
        presetSearch = url.slice(index + 1);
        url = url.slice(0, index);
    }
    const search = concatSearch(presetSearch, ...params.map((params) => paramsToSearch(params)));
    return search ? `${url}?${search}` : url;
};
export const concatSearch = function (...searches: string[]) {
    const searchArr: string[] = [];
    const testLastReg = /&+$/;
    const replaceFirstReg = /^[&?]+/;
    const replaceReg = /^&+|&+$/g;

    for (let i = 0; i < searches.length; i++) {
        searchArr.push(
            concatString(searches[i], searches[++i], '&', testLastReg, replaceFirstReg).replace(
                replaceReg,
                '',
            ),
        );
    }
    return searchArr.join('&').replace(/&{2,}/g, '&');
};
export const paramsToSearch = function (params: AnyObject | string): string {
    if (isObject(params)) {
        return Object.entries(params)
            .reduce((search: string[], [name, value]) => {
                if (name === '' || value == null) {
                    return search;
                }
                search.push(`${name}=${value}`.replace(/&/g, '%26'));
                return search;
            }, [])
            .join('&');
    }
    return toString(params);
};
export const searchToParams = function (search: string | AnyObject): AnyObject {
    if (isObject(search)) {
        return search as AnyObject;
    }
    return toString(search)
        .split(/&+/)
        .reduce((params, keyValue) => {
            const [key, ...value] = keyValue.split('=');
            params[key] = value.join('=');
            return params;
        }, {} as AnyObject);
};

export const parseAliasUrl = function (aliasUrl: string, alias: { [n: string]: string }) {
    if (!alias) {
        return aliasUrl;
    }
    aliasUrl = aliasUrl?.replace(/^@(\w+)/, (_m, n) => {
        const url = alias[n];
        return isString(url) ? parseAliasUrl(url, alias) : n;
    });
    alias = null as any;
    return aliasUrl;
};

export const isRelRootUrl = function (url: string) {
    return /^\//.test(url);
};
export const toRelRootUrl = function (url: string, rootDir?: string): string {
    return isRelRootUrl(url) ? url : toRelRootUrl(rootDir || '/') + toString(url);
};

export const toRelRootDir = function (url: string, rootDir?: string) {
    return toDir(toRelRootUrl(url, rootDir));
};
export const toDir = function (url: string) {
    return toPathname(url).replace(/^[/\\]+$/, '');
};
export const toPathname = function (url: string) {
    return toString(url).replace(/[#?].*$/, '');
};
export const toRelRootPathname = function (url: string, rootDir?: string) {
    return toPathname(toRelRootDir(url, rootDir));
};
