/**
 * @author Fa
 * @copyright Fa 2017
 */
import './core';
import './Base';
import './Page';
import './Component';
