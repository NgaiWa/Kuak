/**
 * @author Fa
 * @copyright Fa 2017
 */
import Promise from '../utils/Promise';
import { localeStorage, Store } from '../utils/Store';
import env, { lib } from './env';
import { History, navFnHandler } from './History';

let app: Program.App.Instance<any>;
let launched: boolean = false;

App.get = function () {
    if (app) {
        return app;
    }
    app = getApp({ allowDefault: true });
    return app;
};
App.launch = function (options: any) {
    if (launched) {
        return app;
    }
    launched = true;
    (App as unknown as Program.App.Constructor)(options);
    return this.get();
};
App.Component = Component;
App.Page = Page;
App.Components = Component.Components = {};
App.Pages = Page.Pages = {};
Page.lib = Component.lib = App.lib = Object.assign(lib, { Promise });
App.history = new History({
    skyline: env.isDevtools ? 9 : 100,
    webview: 10,
});
App.router = navFnHandler(App.lib as any);
App.store = new Store({ storage: localeStorage });
if (!console.dir) {
    console.dir = console.log;
}
