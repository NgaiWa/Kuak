import { round } from '../utils/number';
import { env, lib } from './env';
const systemInfo = env.systemInfo;
const pixelRatio = systemInfo.pixelRatio;
const rpxRatio = systemInfo.windowWidth / 750;
/**
 * @author Fa
 * @copyright Fa 2017
 */
class SizeHelper {
    readonly pixelRatio = pixelRatio;
    readonly rpxRatio = rpxRatio;
    readonly remRatio = 20 / 750;
    readonly vwRatio = 7.5;
    private _rootRemSize!: string;
    get rootRemSize() {
        return (
            this._rootRemSize || (this._rootRemSize = round(this.rpxToPx(this.remRatio), 2) + 'px')
        );
    }
    rpxToPx(rpx: number) {
        return rpx * this.rpxRatio || 0;
    }
    pxToRpx(px: number) {
        return px / this.rpxRatio || 0;
    }
    rpxToRem(rpx: number) {
        return rpx / this.remRatio || 0;
    }
    remToPx(rem: number) {
        return rem / this.remRatio || 0;
    }

    vwToRpx(vw: number) {
        return vw * this.vwRatio;
    }
    rpxToVw(rpx: number) {
        return rpx / this.vwRatio;
    }
}
export const sizeHelper = new SizeHelper();

const statusBarHeight = Math.floor(systemInfo.statusBarHeight) || 0;
const sysStatusbarHeight = statusBarHeight % 2 ? statusBarHeight + 1 : statusBarHeight;
const topStatusbarHeight = sysStatusbarHeight;

export const info = {
    env: `${env.envCls} sys-sbr${topStatusbarHeight}`,
    statusBarHeight,
    sysStatusbarHeight,
    topStatusbarHeight,
    safeArea: systemInfo.safeArea,
    menuButton: lib.getMenuButtonBoundingClientRect(),
    screenHeight: systemInfo.screenHeight,
    screenWidth: systemInfo.screenWidth,
    screenTop: systemInfo.screenTop,
    windowHeight: systemInfo.windowHeight,
    windowWidth: systemInfo.windowWidth,
    get rootRemSize() {
        return sizeHelper.rootRemSize;
    },
};
