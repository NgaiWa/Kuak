/**
 * @author Fa
 * @copyright Fa 2017
 */

import { hasOwn } from '../utils/object';
import { generateKey } from '../utils/string';
import {
    Base,
    checkInitAssignData,
    ProtoProperties,
    registerOptionsName,
    setInitialBase,
} from './Base';
import { boundRouter } from './History';
export const ComponentBase = class ComponentBase extends Base implements Partial<Kuak.Component> {
    static setInitialBase(baseProto: any) {
        return setInitialBase(this, ComponentBase, baseProto);
    }
    static properties?: Kuak.ComponentConstructor['properties'];
    static get registerOptions() {
        const Class: any = this;
        if (!hasOwn(Class, registerOptionsName)) {
            Class[registerOptionsName] = {
                properties: Class.properties,
                lifetimes: {
                    created(this: any) {
                        this.created = Class.prototype.created;
                        this.id = this.id || generateKey();
                        Class.bindInstance(this);
                        const router = this.pageRouter || this.router;
                        if (!router.dir) {
                            router.dir = App.history.currentPage.dir;
                        }
                        boundRouter(this, router);
                        try {
                            this.onCreate();
                        } catch (e) {
                            console.error('onCreateError', e);
                        }
                        checkInitAssignData(this);
                    },
                    attached(this: ComponentBase) {
                        this.__canSetData__ = true;
                        try {
                            this.onLoad();
                        } catch (e) {
                            console.error('onCreateError', e);
                        }
                        this.onShow();
                    },
                    ready(this: ComponentBase) {
                        this.onReady();
                    },
                    moved(this: ComponentBase) {
                        this.onMoved();
                    },
                    detached(this: ComponentBase) {
                        this.onHide();
                        this.onUnload();
                    },
                    error(this: ComponentBase, error: Error) {
                        this.onError(error);
                    },
                },
                pageLifetimes: {
                    show(this: ComponentBase) {
                        this.onPageShow();
                    },
                    hide(this: ComponentBase) {
                        this.onPageHide();
                    },
                    resize(this: ComponentBase, sise: AnyObject) {
                        this.onPageResize(sise);
                    },
                    routeDone(this: ComponentBase) {
                        this.onRouteDone();
                    },
                },
            };
        }
        return Class[registerOptionsName] as Kuak.ComponentConstructor['registerOptions'];
    }
    static register() {
        return Component.register(this as any);
    }
} as unknown as Kuak.ComponentConstructor;
ComponentBase.defineProto(ProtoProperties);
Component.Base = ComponentBase;
Component.register = function <T extends Kuak.ComponentConstructor = Kuak.ComponentConstructor>(
    ComponentConstructor: T,
) {
    return Component(ComponentConstructor.registerOptions);
};
