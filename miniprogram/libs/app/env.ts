/**
 * @author Fa
 * @copyright Fa 2017
 */
const udf = 'undefined';
const isWx = typeof wx !== udf;
const isQQ = !isWx && typeof qq !== 'undefined';
const envName = isWx ? 'wx' : 'qq';
export const lib = isWx ? wx : qq;

const pcReg = /Windows|Linux|MacOS|PC/i;

const systemInfo: Program.SystemInfo = lib.getSystemInfoSync() as any;
const baseInfo = lib.getAppBaseInfo();

const isPC = pcReg.test(systemInfo.system);
const isIOS = !isPC && /iOS/i.test(systemInfo.system);
const isAndroid = !isPC && !isIOS;
const isWindowApp = pcReg.test(systemInfo.platform);
const system = isPC
    ? 'pc-os ' +
      (systemInfo.system.match(/Windows|Linux|MacOS/i) || ['other'])[0].toLowerCase() +
      '-os'
    : isIOS
      ? 'ios-os'
      : 'android-os';
const isDevtools = systemInfo.platform === 'devtools';

const model = systemInfo.model;
const isPad = /pad|tablet/i.test(model);
const isPhone = !isPad && (isIOS || isAndroid || !isPC);
const isPhoneX = /iPhone\s*([a-z]+|[0-9]{2,})/i.test(model);
const device = isPad ? 'pad' : isPhone ? 'phone' : 'pc';
const envs: string[] = [];
envs.push(system, `${device}-device`, `env-${envName}`);
if (isPhoneX) {
    envs.push('iPhoneX');
}
if (isDevtools) {
    envs.push('devtools');
}
if (isWindowApp) {
    envs.push('wnd-app');
}
export const env = {
    name: envName,
    envs,
    envCls: envs.join(' '),
    systemInfo,
    baseInfo,
    system,
    device,
    isPC,
    isIOS,
    isAndroid,
    isWindowApp,
    isDevtools,
    isPad,
    isPhone,
    isPhoneX,
    isWx,
    isQQ,
};
export default env;
