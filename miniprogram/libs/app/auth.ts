/**
 * @author Fa
 * @copyright Fa 2017
 */
import EventTarget from '../polyfill/EventTarget';
import { uncaughtError } from '../utils/Promise.fn';

class Authorize extends EventTarget {
    info: AnyObject;
    constructor() {
        super();
        this.info = {};
    }
    updateInfo() {
        return new Promise((success, fail) => {
            App.lib.getSetting({
                success,
                fail,
            });
        })
            .then((res: any) => {
                const authSetting = res.authSetting || {};
                res.authSetting = authSetting;
                const authorizeInfo = this.info;
                for (const name in authSetting) {
                    authorizeInfo[name.replace(/^scope\./i, '')] = authSetting[name];
                }
                const result = { info: authorizeInfo, result: res };
                this.dispatchEvent('updateInfo', result);
                return result;
            })
            .catch(uncaughtError);
    }
    // @ts-expect-error
    addEventListener(
        type: 'updateInfo',
        callback: (e: CustomEvent<{ info: Authorize['info']; result: any }>) => any,
    ): string;
}
const auth = new Authorize();
auth.updateInfo();
export default auth;
