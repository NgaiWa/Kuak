/**
 * @author Fa
 * @copyright Fa 2017
 */
import { noop } from '../utils/function';
import { defineProperties, getOwnPropDescMap, hasOwn } from '../utils/object';
import { generateKey } from '../utils/string';
import { appendSearch, toDir, toRelRootUrl } from '../utils/url';
import { Base, ProtoProperties, registerOptionsName, setInitialBase } from './Base';
import { boundRouter } from './History';

/**
 * @author Fa
 * @copyright Fa 2017
 */

const onUnload = function (this: Kuak.Page) {
    // eslint-disable-next-line prefer-rest-params
    // this.__onUnload__(...(arguments as any));
    this.__h_canUnload__ = true;
    if (this.__onUnload__) {
        this.__onUnload__();
    }
};
const onShow = function (this: Kuak.Page) {
    App.history.onPagesChange({ page: this, isOld: true });
    // eslint-disable-next-line prefer-rest-params
    const args = arguments;
    if (this.__onShow__) {
        this.__onShow__(...args);
    }
    this.__h_onShow__(...args);
};
export const PageBase = class PageBase extends Base implements Partial<Kuak.Page> {
    static readonly isPage = true;
    static setInitialBase(baseProto: any) {
        return setInitialBase(this, PageBase, baseProto);
    }
    static get registerOptions() {
        const Class: any = this;
        if (!hasOwn(Class, registerOptionsName)) {
            Class[registerOptionsName] = {
                onLoad(this: any, options: AnyObject) {
                    this.onLoad = Class.prototype.onLoad;
                    this.pathname = toRelRootUrl(this.route);
                    this.dir = toDir(this.pathname);
                    this.url = appendSearch(this.pathname, this.options);
                    console.log(this);
                    Class.bindInstance(this);
                    let restoredPage: any = App.history.restoredPage;
                    if (
                        restoredPage &&
                        Object.getPrototypeOf(restoredPage) !== Object.getPrototypeOf(this)
                    ) {
                        restoredPage = null;
                    }
                    this.id = restoredPage?.id || generateKey();
                    const router = this.router;
                    router.dir = this.dir;
                    boundRouter(this, router);
                    App.history.onPagesChange({ page: this, isNew: true });
                    try {
                        this.onCreate(options, restoredPage);
                        if (!this.__initDataCalled__) {
                            this.restoreInitPage(restoredPage, this.data);
                        }
                    } catch (e) {
                        console.error('onCreate Error', e);
                    }
                    boundRouter(this, router);
                    try {
                        this.onLoad(options);
                    } catch (e) {
                        console.error('onLoad Error', e);
                    }
                    if (this.onUnload != onUnload) {
                        this.__h_onUnload__ = this.onUnload;
                        this.onUnload = onUnload;
                    }
                    if (this.onShow != onShow) {
                        this.__h_onShow__ = this.onShow;
                        this.onShow = onShow;
                    }
                },
                onUnload: noop,
                onShow: noop,
                onReady: noop,
                onRouteDone: noop,
                onHide: noop,
                onResize: noop,
            };
        }
        return Class[registerOptionsName];
    }
    static register() {
        return Page.register(this as any);
    }
    isRestoredPage!: boolean;
    restoredPageProps!: string[];
    restoreInitPage(
        restoredPage: PageBase,
        newData?: AnyObject,
        options?: { forDataObserver?: true | false },
    ) {
        this.isRestoredPage = !!restoredPage;
        if (this.isRestoredPage) {
            defineProperties(this, getOwnPropDescMap(restoredPage, this.restoredPageProps));
            const thisData = this.data;
            const restoredData = restoredPage.data;
            const data = thisData && thisData !== restoredData ? thisData : restoredData;
            if (newData && newData !== data) {
                Object.assign(newData, data);
            }
            restoredPage?.onRestoredPageDispose();
        }
        this.initData(newData || this.data, options);
        return this.isRestoredPage;
    }
    onRestoredPageDispose() {
        this.onDisposeEvents();
    }
    onShow() {
        return this.onPageShow();
    }
    onHide() {
        return this.onPageHide();
    }
    onResize(options?: Kuak.IPageResizeOption) {
        return this.onPageResize(options) as any;
    }
} as unknown as Kuak.PageConstructor;
PageBase.defineProto(ProtoProperties);
PageBase.extendProto({ restoredPageProps: ['data', 'state'] });
Page.Base = PageBase;
Page.register = function <T extends Kuak.PageConstructor = Kuak.PageConstructor>(
    PageConstructor: T,
) {
    return Page(PageConstructor.registerOptions);
};
