import EventTarget from '../polyfill/EventTarget';

/**
 * @author Fa
 * @copyright Fa 2017
 */
class Network extends EventTarget {
    info: {
        isConnected: true | false;
        isReconnected: true | false;
        prevConnected: true | false;
        prevNetworkType: 'wifi' | string;
        networkType: 'wifi' | string;
    };
    constructor() {
        super();
        this.info = {
            isConnected: true,
            isReconnected: true,
            prevConnected: true,
            prevNetworkType: 'wifi',
            networkType: 'wifi',
        };
    }
    getType() {
        return App.lib
            .getNetworkType()
            .then((networkInfo) => {
                return {
                    success: true,
                    isConnected: networkInfo.networkType !== 'none',
                    ...networkInfo,
                };
            })
            .catch((networkInfo) => {
                networkInfo = networkInfo || {};
                return {
                    success: false,
                    isConnected: networkInfo.networkType !== 'none',
                    ...networkInfo,
                };
            });
    }
    updateInfo() {
        return this.getType().then((networkInfo) => {
            const prevNetworkInfo = this.info;
            const currentNetworkInfo = networkInfo as unknown as Network['info'];
            if (prevNetworkInfo) {
                currentNetworkInfo.prevConnected = prevNetworkInfo.isConnected;
                currentNetworkInfo.prevNetworkType = prevNetworkInfo.networkType;
                if (prevNetworkInfo.networkType === 'unknown') {
                    prevNetworkInfo.networkType = '';
                }
                if (prevNetworkInfo.isConnected !== currentNetworkInfo.isConnected) {
                    this.dispatchEvent('statusChange', currentNetworkInfo);
                }
            } else {
                currentNetworkInfo.prevConnected = networkInfo.isConnected;
                currentNetworkInfo.prevNetworkType = networkInfo.networkType;
            }
            this.info = currentNetworkInfo;
            this.dispatchEvent('updateInfo', currentNetworkInfo);
            return currentNetworkInfo;
        });
    }
    // @ts-expect-error
    addEventListener(
        type: 'statusChange',
        callback: (event: CustomEvent<Network['info']>) => any,
    ): string;
    // @ts-expect-error
    addEventListener(
        type: 'updateInfo',
        callback: (event: CustomEvent<Network['info']>) => any,
    ): string;
}
const network = new Network();
network.updateInfo();
export default network;
