/**
 * @author Fa
 * @copyright Fa 2017
 */
import './core';

import EventTarget, { toEvent } from '../polyfill/EventTarget';
import { getBaseProto } from '../utils/class';
import { offEvents } from '../utils/Event';
import { noop } from '../utils/function';
import { assignDesc, assignSources } from '../utils/merge';
import {
    defineProperties,
    getOwn,
    getValueResultByPath,
    getValuesByPathsNodes,
    inDirPathNodes,
    pickByAlias,
    setValueByPath,
    setValuesByKeys,
    toNamePathNodes,
    toNormalNamePath,
} from '../utils/object';
import { localeCompareUp } from '../utils/string';
import { isArray, isFunction, isObject, isString } from '../utils/type';
import { info } from './ui';

const onStoreChange = function (this: Base, { target, detail }: IStoreEvent) {
    const used = this.externalEvents?.[target!.id];
    if (!used || !detail || !detail.data) {
        return;
    }
    const { alias } = used;
    this.setData(pickByAlias(detail.data, alias, true));
};
interface DataObserver {
    name: string;
    pathsNodes: string[][];
    handler: AnyFunction;
}
interface DataObserverMap {
    [name: string]: DataObserver;
}

const toDataObserverMap = function (
    observers: Record<string, AnyFunction> | undefined,
): DataObserverMap {
    const dataObservers: DataObserverMap = {};
    if (observers == null && !isObject(observers)) {
        return {};
    }
    for (const key in observers) {
        const handler = observers[key];
        if (!isFunction(handler)) {
            continue;
        }
        const names = key
            .split(/\s*,\s*/)
            .filter(Boolean)
            .sort(localeCompareUp)
            .map(toNormalNamePath);
        const name = names.join(',');
        dataObservers[name] = {
            name: name,
            pathsNodes: names.map(toNamePathNodes),
            handler,
        };
    }
    return dataObservers;
};
const toDataObservers = function (observers: Record<string, AnyFunction> | undefined) {
    const map = toDataObserverMap(observers);
    return map && Object.values(map);
};
const getDataObserversByPath = function (dataObservers: DataObserver[], namePath: string) {
    if (!dataObservers) {
        return;
    }
    let namePathNodes = toNamePathNodes(namePath);
    let map: DataObserverMap = undefined as any;
    for (let i = 0, l = dataObservers.length; i < l; i++) {
        const dataObserver = dataObservers[i];
        const pathsNodes = dataObserver.pathsNodes;
        if (
            pathsNodes.some((pathNodes) => {
                return (
                    inDirPathNodes(namePathNodes, pathNodes) ||
                    inDirPathNodes(pathNodes, namePathNodes)
                );
            })
        ) {
            map = map || {};
            map[dataObserver.name] = dataObserver;
        }
    }
    namePathNodes = null as any;
    return map;
};
const getDataObserversByData = function (dataObservers: DataObserver[], data: AnyObject) {
    let map: DataObserverMap = null as any;
    if (dataObservers?.length) {
        for (const key in data) {
            const _map = getDataObserversByPath(dataObservers!, key);
            // canCallDataObservers[key] && getDataObserversByPath(dataObservers!, key);
            if (_map) {
                map = Object.assign(map || {}, _map);
            }
        }
    }
    return map;
};

const callDataObserverMap = function (component: Base, dataObserverMap: DataObserverMap) {
    if (!dataObserverMap) {
        return;
    }
    const data = component.data;
    let result = undefined;
    for (const key in dataObserverMap) {
        try {
            const dataObserver = dataObserverMap[key];
            dataObserver.handler.apply(
                component,
                getValuesByPathsNodes(data, dataObserver.pathsNodes),
            );
        } catch (error) {
            console.error('调用 observers[', key, '] 监听处理出错:', error);
            result = false;
        }
    }
    return result;
};
export const callDataObserverOfData = function (component: Base, data: AnyObject) {
    callDataObserverMap(
        component,
        getDataObserversByData((component as any).__dataObservers__!, data),
    );
};
export const callDataObserver = function (component: Base) {
    callDataObserverOfData(component, component.data);
};
const getDataByPaths = function (source: any, dataPaths: string[]) {
    return dataPaths.reduce(
        (res: any, path: string) => {
            const result = getValueResultByPath(res.source, path);
            if (result.state) {
                res.data[path] = result.value;
            }
            return res;
        },
        { source: source, data: {} },
    ).data;
};
export const checkInitAssignData = function (component: Base) {
    if (!(component as any).__initDataCalled__) {
        assignDataToViewData(component);
        callDataObserver(component);
    }
};
export class Base extends EventTarget implements Kuak.ComponentBase {
    lib!: Program.Lib;
    data!: AnyObject;
    router!: Kuak.NavRouter;
    pageRouter!: Kuak.NavRouter;
    externalEvents: IEventMap;
    static lib = App.lib;
    private __triggerEvent__!: AnyFunction;
    // @ts-expect-error
    private __viewData__!: AnyObject;
    private __dataObservers__?: DataObserver[];
    protected __initDataCalled__?: boolean;
    protected __canSetData__?: boolean;
    constructor() {
        super();
        this.__dataObservers__ = Object.getPrototypeOf(this).constructor.getDataObservers();
        this.externalEvents = {};
    }
    static _AlreadySetInitialBase_ = false;
    static dataObservers: Kuak.ComponentBaseConstructor['dataObservers'];
    static __dataObservers__?: DataObserver[];
    static getDataObservers(reset?: boolean) {
        let map = reset ? null : getOwn(this, '__dataObservers__');
        if (!map) {
            map = this.__dataObservers__ = toDataObservers(this.dataObservers);
        }
        return map;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    static setInitialBase(_baseProto: any) {
        return this;
    }
    static bindInstance<Instance = Kuak.ComponentBase>(instance: Instance) {
        if (!instance) {
            return this;
        }
        const _instance: any = instance;
        const fnInstance = new this();
        const baseProto = getBaseProto(_instance) || _instance;
        if (!Object.hasOwn(_instance, '__triggerEvent__')) {
            const __triggerEvent__ =
                _instance.__triggerEvent__ ||
                _instance.__getTriggerEvent__?.() ||
                _instance.triggerEvent;
            defineProperties(_instance, {
                __triggerEvent__: { value: __triggerEvent__, configurable: false },
                triggerEvent: {
                    value: fnInstance.triggerEvent,
                    configurable: false,
                },
            });
        }
        if (!Object.hasOwn(baseProto, '__setData__')) {
            const __setData__ = baseProto.setData as AnyFunction;
            defineProperties(baseProto, {
                __setData__: { value: __setData__, configurable: false },
                setData: { value: fnInstance.setData, configurable: false },
            });
        }
        if (!Object.hasOwn(_instance as any, '__viewData__')) {
            const __viewData__ = _instance.data as AnyFunction;
            defineProperties(_instance, {
                __viewData__: { value: __viewData__, configurable: false },
            });
        }
        this.setInitialBase(baseProto);
        if ((this as any).isPage) {
            ['onReady', 'onShow', 'onRouteDone', 'onHide', 'onResize', 'onUnload'].reduce(
                (ret, name) => {
                    const fn = ret.fnInstance[name];
                    const on = getOwn(instance, name);
                    if (on || fn) {
                        ret.fnInstance[`__${name}__`] = on;
                        ret.fnInstance[name] = fn;
                    }
                    return ret;
                },
                {
                    instance,
                    fnInstance: fnInstance as any,
                },
            );
        }
        return defineProperties(instance, Object.getOwnPropertyDescriptors(fnInstance));
    }
    useStore(store: IStore, keys: string[] | AliasNameMap, isReuse?: boolean) {
        const alias: AliasNameMap = isArray(keys)
            ? setValuesByKeys({}, keys as string[], keys as string[])
            : { ...keys };
        const used = this.externalEvents[store.id];
        if (!used) {
            this.externalEvents[store.id] = {
                target: store,
                alias,
                id: store.addEventListener('change', onStoreChange.bind(this)),
            };
        }
        if (isReuse) {
            used.alias = alias;
        } else {
            Object.assign(used.alias, alias);
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onCreate(_options?: AnyObject) {}
    onLoad() {}
    onShow() {}
    onReady() {}
    onMoved() {}
    onHide() {}
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onError(_error: Error) {}
    onPageShow() {}
    onPageHide() {}
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onPageResize(_options?: AnyObject) {}
    onRouteDone() {}
    protected __unloaded__!: boolean;
    onUnload() {
        this.__unloaded__ = true;
        this.onDispose();
    }
    onDispose() {
        this.onDisposeEvents();
    }
    onDisposeEvents() {
        if (this.externalEvents) {
            offEvents(this.externalEvents);
            this.externalEvents = null as any;
        }
        this.removeAllEventLListener();
    }
    protected __setData__(data: any, callback?: AnyFunction) {
        if (callback) {
            callback(this, data);
        }
    }
    setData(dataOrPath: AnyObject | string[] | string, callback?: AnyFunction): any {
        if (dataOrPath == null) {
            return;
        }
        const component: any = this;
        if (isString(dataOrPath)) {
            dataOrPath = [dataOrPath];
        }
        if (!isObject(dataOrPath)) {
            return;
        }
        const thisData = this.data;
        const viewData = (this as any).__viewData__;
        const dataObservers = this.__dataObservers__;
        let data: any;
        // const canCallDataObservers: any = dataObservers && {};
        if (isArray(dataOrPath)) {
            data = getDataByPaths(thisData, dataOrPath);
        } else {
            data = dataOrPath;
            // if (canCallDataObservers) {
            //     const paths = Object.keys(data);
            //     const oldData = getDataByPaths(thisData, paths);
            //     for (let i = 0, path; i < paths.length; i++) {
            //         path = paths[i];
            //         canCallDataObservers[path] = oldData[path] !== data[path];
            //     }
            // }
        }
        const canSetData = this.__canSetData__ && this.__setData__ !== this.setData;
        const isCallback = callback != null && isFunction(callback);
        if (canSetData) {
            if (isCallback) {
                if (canSetData) {
                    component.__setData__(data, callback);
                }
            } else {
                component.__setData__(data);
            }
        }
        let map: DataObserverMap = null as any;
        if (thisData !== viewData) {
            const isNewData = thisData !== data;
            for (const key in data) {
                if (isNewData) {
                    setValueByPath(thisData, key, data[key], true);
                }
                const _map = getDataObserversByPath(dataObservers!, key);
                // canCallDataObservers[key] && getDataObserversByPath(dataObservers!, key);
                if (_map) {
                    map = Object.assign(map || {}, _map);
                }
            }
            if (!canSetData) {
                assignDataToViewData(this);
            }
        }
        if (!canSetData && isCallback) {
            callback!(this, data);
        }
        return callDataObserverMap(this, map);
    }
    initData(
        data: AnyObject,
        options?: {
            /**
             * 是否触发数据监听
             * @default true
             */
            forDataObserver?: true | false;
            [n: string]: any;
        },
    ) {
        this.data = data;
        if (!data.ui) {
            data.ui = info;
        }
        return this.__initData__(data, options);
    }
    protected __initData__(
        data: any,
        options?: {
            /**
             * 是否触发数据监听
             * @default true
             */
            forDataObserver?: boolean;
            [n: string]: any;
        },
    ) {
        this.__initDataCalled__ = true;
        assignDataToViewData(this);
        this.__setData__(data);
        if (options?.forDataObserver !== false) {
            callDataObserverOfData(this, data);
        }
        return data;
    }
    dispatchEvent(event: Event | string, detail?: object, options?: TriggerEventOptions): boolean {
        const eventObject = toEvent(event, detail, options);
        try {
            if (this.__triggerEvent__ && this.__triggerEvent__ !== this.triggerEvent) {
                this.__triggerEvent__(eventObject.type, eventObject, options);
            }
        } catch (error) {
            console.error(error);
        }
        return super.dispatchEvent(eventObject);
    }
    triggerEvent() {
        // eslint-disable-next-line prefer-rest-params
        this.dispatchEvent(...(arguments as unknown as [string, object, any]));
    }
    static defineStatic(descMap: any) {
        defineProperties(this, descMap);
        return this;
    }
    static defineProto(descMap: any) {
        defineProperties(this.prototype, descMap);
        return this;
    }
    static extendStatic(...sources: any[]) {
        assignSources(this, sources);
        return this;
    }
    static extendProto(...sources: any[]) {
        assignSources(this.prototype, sources);
        return this;
    }
}
export const ProtoProperties = {
    lib: { value: App.lib },
    useStore: { value: Base.prototype.useStore },
    onUnload: { value: Base.prototype.onUnload, writable: true },
    triggerEvent: { value: Base.prototype.triggerEvent },
    setData: { value: Base.prototype.setData },
};
Base.defineProto({
    triggerEvent: ProtoProperties.triggerEvent,
});
export type TBase = typeof Base;

export const registerOptionsName = '_registerOptions_';
export const setInitialBase = function (Class: TBase, BaseClass: TBase, declaredBaseProto: any) {
    if (!declaredBaseProto) {
        return Class;
    }
    // 程序功能绑定到基础类原型
    if (
        !BaseClass._AlreadySetInitialBase_ &&
        declaredBaseProto !== BaseClass.prototype &&
        declaredBaseProto !== Base.prototype &&
        !(declaredBaseProto instanceof BaseClass) &&
        !(declaredBaseProto instanceof Base)
    ) {
        BaseClass._AlreadySetInitialBase_ = true;
        const originBaseProto = getBaseProto(declaredBaseProto);
        const proto = getBaseProto(BaseClass.prototype, true, null, Base.prototype);

        // [declared]-extends-[origin]
        // [declared]-extends-[custom declared]-extends-[origin]-extends-[custom base]
        if (proto && proto !== originBaseProto) {
            const baseProtoBase = getBaseProto(originBaseProto, true, null, Base.prototype);
            if (baseProtoBase) {
                // [origin]-extends-[custom base]
                Object.defineProperty(baseProtoBase, '__getTriggerEvent__', {
                    value:
                        Object.getOwnPropertyDescriptor(baseProtoBase, 'triggerEvent')?.get || noop,
                });
                Object.setPrototypeOf(baseProtoBase, Base.prototype);
            }
            // [custom declared]-extends-[origin]
            proto.super = originBaseProto;
            Object.setPrototypeOf(proto, originBaseProto);
        }
    }

    // 声明类功能绑定到实例原型
    if (!(declaredBaseProto instanceof Class)) {
        // [declared]-extends-[custom declared]
        Object.setPrototypeOf(declaredBaseProto, Class.prototype);
    }

    return Class;
};

const _assignToData = function (component: Base, type: 0 | 1) {
    const viewData = (component as any).__viewData__;
    const data = component.data;
    if (viewData === data || !viewData || !data) {
        return;
    }
    if (type === 0) {
        assignDesc(viewData, data);
    } else {
        assignDesc(data, viewData);
    }
};
export const assignDataToViewData = function (component: Base) {
    _assignToData(component, 0);
};
export const assignViewDataToData = function (component: Base) {
    _assignToData(component, 1);
};
