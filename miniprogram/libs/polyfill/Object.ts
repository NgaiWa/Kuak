/**
 * @author Fa
 * @copyright Fa 2017
 */

// if (!Object.hasOwn) {
//     const hasOwnProperty = Object.prototype.hasOwnProperty;
//     Object.defineProperty(Object, 'hasOwn', {
//         value: function (o: object, k: PropertyKey) {
//             return hasOwnProperty.call(o, k);
//         },
//     });
// }
// if (!Array.prototype.at) {
//     Object.defineProperty(Array.prototype, 'at', {
//         value: function (index: number) {
//             return this[index < 0 ? this.length + 1 : index];
//         },
//     });
// }

const sameNaN = (o1: any, o2: any) => o1 !== o1 && o2 !== o2;
Object.defineProperties(Object, {
    eq: {
        value(o1: any, o2: any) {
            return o1 == o2 || sameNaN(o1, o2);
        },
    },
    same: {
        value(o1: any, o2: any) {
            return o1 === o2 || sameNaN(o1, o2);
        },
    },
});
