/**
 * @author Fa
 * @copyright Fa 2017
 */

import { Event } from './Event';
const ErrorEventPolyfill = function (): ErrorEventConstructor {
    const _ErrorEvent = (globalThis as unknown as { ErrorEvent: ErrorEventConstructor }).ErrorEvent;
    if (_ErrorEvent) {
        return _ErrorEvent;
    }
    class ErrorEvent extends Event {
        readonly colno: number;
        readonly error: any;
        readonly filename: string;
        readonly lineno: number;
        readonly message: string;
        constructor(type: string, eventInitDict?: ErrorEventInit) {
            super(type, eventInitDict);
            eventInitDict = eventInitDict || {};
            this.message = eventInitDict.message ?? '';
            this.colno = eventInitDict.colno || 0;
            this.lineno = eventInitDict.lineno || 0;
            this.filename = eventInitDict.filename || '';
            this.error = eventInitDict.error;
        }
    }
    return ErrorEvent;
};

export const ErrorEvent = ErrorEventPolyfill();
export default ErrorEvent;

export const createErrorEvent = function (error: any) {
    return new ErrorEvent(
        'error',
        error == null
            ? error
            : typeof error === 'string'
              ? {
                    message: error,
                }
              : {
                    message: error.message,
                    filename: error.fileName,
                    colno: error.columnNumber,
                    lineno: error.lineNumber,
                    error: error,
                },
    );
};
