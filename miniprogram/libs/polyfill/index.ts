/**
 * @author Fa
 * @copyright Fa 2017
 */

import './Object';
import './Event';
import './EventTarget';
import './CustomEvent';
