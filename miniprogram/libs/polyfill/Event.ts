/**
 * @author Fa
 * @copyright Fa 2017
 */

const EventPolyfill = function (): EventConstructor {
    const _Event = (globalThis as unknown as { Event: EventConstructor }).Event;
    if (_Event && (_Event as any).__IsEventPolyfill__) {
        return _Event;
    }
    const immediatePropagationStoppedName = Symbol('immediatePropagationStopped');
    const setIsEventPolyfill = function (EventConstructor: EventConstructor): EventConstructor {
        return Object.defineProperty(EventConstructor, '__IsEventPolyfill__', { value: true });
    };
    if (_Event) {
        const eventStopImmediatePropagation = _Event.prototype.stopImmediatePropagation;
        const stopImmediatePropagationDescriptor = {
            ...Object.getOwnPropertyDescriptor(_Event.prototype, 'stopImmediatePropagation'),
            value: function stopImmediatePropagation() {
                (this as any)[immediatePropagationStoppedName] = true;
                return eventStopImmediatePropagation.call(this);
            },
        };
        Object.defineProperty(
            _Event.prototype,
            'stopImmediatePropagation',
            stopImmediatePropagationDescriptor,
        );
        return setIsEventPolyfill(_Event);
    }
    enum EventPhase {
        NONE = 0,
        CAPTURING_PHASE = 1,
        AT_TARGET = 2,
        BUBBLING_PHASE = 3,
    }

    const propagationStoppedName = Symbol('stoppedPropagation');
    const defaultPreventedName = Symbol('defaultPrevented');
    class Event<Target extends EventTarget = EventTarget> implements Event<Target> {
        constructor(type: string, eventInitDict?: EventInit) {
            Object.assign(this, { bubbles: true, cancelable: true, composed: true }, eventInitDict);
            this.type = type;
            Object.defineProperties(this, {
                [immediatePropagationStoppedName]: {
                    value: false,
                    writable: true,
                    configurable: true,
                },
                [defaultPreventedName]: { value: false, writable: true, configurable: true },
            });
        }
        private [propagationStoppedName]: boolean;
        private [immediatePropagationStoppedName]: boolean;
        private [defaultPreventedName]: boolean;
        readonly bubbles!: boolean;
        get cancelBubble() {
            return this[propagationStoppedName] || this[immediatePropagationStoppedName];
        }
        set cancelBubble(value) {
            this[propagationStoppedName] = value;
        }
        readonly cancelable!: boolean;
        readonly composed!: boolean;
        readonly currentTarget!: Target | null;
        get defaultPrevented() {
            return this[defaultPreventedName];
        }
        get immediatePropagationStopped() {
            return this[immediatePropagationStoppedName];
        }
        readonly eventPhase!: number;
        readonly isTrusted!: boolean;
        get returnValue() {
            return !this.defaultPrevented;
        }
        set returnValue(value) {
            this[defaultPreventedName] = this[defaultPreventedName] || !value;
        }
        readonly srcElement!: Target | null;
        readonly target!: Target | null;
        readonly timeStamp!: DOMHighResTimeStamp;

        readonly type!: string;

        composedPath(): EventTarget[] {
            return [];
        }

        /**
         * @deprecated
         */
        initEvent(type: string, bubbles?: boolean, cancelable?: boolean): void {
            console.error('initEvent is deprecated');
            void type;
            void bubbles;
            void cancelable;
        }

        preventDefault(): void {
            this[defaultPreventedName] = true;
        }
        stopImmediatePropagation(): void {
            this[immediatePropagationStoppedName] = true;
            this.stopPropagation();
        }
        stopPropagation(): void {
            this.cancelBubble = false;
        }
        readonly NONE!: EventPhase.NONE;
        readonly CAPTURING_PHASE!: EventPhase.CAPTURING_PHASE;
        readonly AT_TARGET!: EventPhase.AT_TARGET;
        readonly BUBBLING_PHASE!: EventPhase.BUBBLING_PHASE;
        static readonly NONE: EventPhase.NONE;
        static readonly CAPTURING_PHASE: EventPhase.CAPTURING_PHASE;
        static readonly AT_TARGET: EventPhase.AT_TARGET;
        static readonly BUBBLING_PHASE: EventPhase.BUBBLING_PHASE;
    }
    Object.assign(Event.prototype, EventPhase);
    Object.assign(Event, EventPhase);
    return setIsEventPolyfill(Event);
};

export const Event = EventPolyfill();

export default Event;
