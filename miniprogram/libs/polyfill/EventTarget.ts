/**
 * @author Fa
 * @copyright Fa 2017
 */

import CustomEvent from './CustomEvent';
import { createErrorEvent } from './ErrorEvent';
import Event from './Event';

const isArray = Array.isArray;
export const toEvent = function <T extends Event = CustomEvent>(
    event: Event | string,
    detail?: object,
    options?: TriggerEventOptions,
): T {
    const argumentsLength = arguments.length;
    if (argumentsLength < 1) {
        throw new TypeError(
            `Failed to execute 'dispatchEvent' on 'EventTarget': 1 argument required, but only ${1} present.`,
        );
    }
    const eventObject =
        typeof event === 'string' ? new CustomEvent(event, { ...options, detail }) : event;
    if (!(eventObject instanceof Event)) {
        throw new TypeError(
            `Failed to execute 'dispatchEvent' on 'EventTarget': parameter 1 is not of type 'Event'.`,
        );
    }
    return eventObject as T;
};
const EventTargetPolyfill = function (): EventTargetConstructor {
    const _EventTarget = (globalThis as unknown as { EventTarget: EventTargetConstructor })
        .EventTarget;
    if (_EventTarget && (_EventTarget as any).__IsEventTargetPolyfill__) {
        return _EventTarget;
    }
    const eventListenersName = Symbol('eventListeners');
    interface ListenerInfo {
        id: string;
        isFunction: boolean;
        callback: EventListenerOrEventListenerObject;
        listener: EventListenerOrEventListenerObject;
        once: boolean;
        removed: boolean;
        capture: boolean;
        called: boolean;
        options?: AddEventListenerOptions | boolean;
    }
    const getEventListeners = function (target: EventTarget, type: string, doInit?: boolean) {
        let listeners: ListenerInfo[] = (target as any)[eventListenersName][type];
        if (!listeners || doInit) {
            listeners = (target as any)[eventListenersName][type] = [];
        }
        return listeners;
    };
    const removeCalledOnce = function (listeners: ListenerInfo[]) {
        if (!listeners) {
            return;
        }
        for (let i = 0, listener; i < listeners.length; i++) {
            listener = listeners[i];
            if (listener.once && listener.called) {
                removeListener(listeners, listener, i--);
            }
        }
    };
    const getCaptureOption = function (options?: EventListenerOptions | boolean) {
        return typeof options === 'boolean' ? options : Boolean(options?.capture);
    };
    const addEvent = function (
        target: EventTarget,
        type: string,
        callback: EventListenerParam,
        options?: EventListenerOptionsParam<AddEventListenerOptions>,
        listener?: EventListenerParam,
    ) {
        if (callback == null) {
            return;
        }
        const isFunction = typeof callback === 'function';
        if (!isFunction && typeof (callback as EventListenerObject).handleEvent === 'function') {
            return;
        }
        const listeners = getEventListeners(target, type, true);
        let capture = getCaptureOption(options);
        const listenerInfo = listeners.find((info) => {
            return info.callback == callback && capture === info.capture;
        }) || {
            id: 'EID_' + Date.now().toString(32) + '_' + Math.random().toString(32).slice(2),
            isFunction: isFunction,
            callback: callback,
            listener: listener!,
            options: options,
            capture: capture,
            once: Boolean(options && (options as any).once),
            called: false,
            removed: false,
        };
        capture = null as any;
        listeners.push(listenerInfo);
        return listenerInfo;
    };

    const setIsEventTargetPolyfill = function (
        EventTarget: EventTargetConstructor,
    ): EventTargetConstructor {
        return Object.defineProperty(EventTarget, '__IsEventTargetPolyfill__', { value: true });
    };
    const removeListener = function (
        listeners: ListenerInfo[],
        listener: ListenerInfo,
        index: number,
    ) {
        listener.removed = true;
        listeners.splice(index, 1);
    };
    const callListener = function (listener: ListenerInfo, event: Event) {
        if (
            event.immediatePropagationStopped ||
            (listener.once && listener.called) ||
            listener.removed
        ) {
            return;
        }
        const callback: any = listener.callback;
        if (listener.isFunction) {
            callback(event);
        } else {
            callback.handleEvent(event);
        }
        listener.called = true;
    };
    const removeEvent = function (
        target: EventTarget,
        type: string,
        check: (listener: ListenerInfo) => boolean,
        removeEventListener?: EventTarget['removeEventListener'],
    ) {
        const listeners = getEventListeners(target, type);
        if (!listeners) {
            return;
        }
        const doRemove = removeEventListener != null;
        for (let i = 0, listener; i < listeners.length; i++) {
            if (check((listener = listeners[i]))) {
                if (doRemove) {
                    removeEventListener!(type, listener.listener, listener.capture);
                }
                removeListener(listeners, listener, i--);
            }
        }
    };
    const removeEventByCallback = function (
        target: EventTarget,
        type: string,
        callback: any,
        options: any,
        removeEventListener?: EventTarget['removeEventListener'],
    ) {
        if (!callback) {
            return;
        }
        let capture = getCaptureOption(options);
        removeEvent(
            target,
            type,
            (listener) => listener.callback === callback && listener.capture === capture,
            removeEventListener,
        );
        capture = callback = null as any;
    };
    const removeEventByType = function (
        target: EventTarget,
        type: string | string[],
        options?: EventListenerOptionsParam,
        removeEventListener?: EventTarget['removeEventListener'],
    ) {
        const types: string[] = isArray(type) ? type : [type];
        let isAll = options == null || (options as EventListenerOptions).capture == null;
        let capture = !isAll || getCaptureOption(options);
        const check = (listener: ListenerInfo) => isAll || listener.capture === capture;
        for (let i = 0; i < types.length; i++) {
            removeEvent(target, types[i], check, removeEventListener);
        }
        isAll = capture = null as any;
    };
    const removeEventById = function (
        target: EventTarget,
        idOrAll: string | string[] | true,
        options?: EventListenerOptionsParam,
        removeEventListener?: EventTarget['removeEventListener'],
    ) {
        if (!idOrAll) {
            return;
        }

        let isAll = options == null || (options as EventListenerOptions).capture == null;
        let capture = !isAll || getCaptureOption(options);
        const isAllId: boolean = idOrAll === true;
        let idList: string[];
        let check: (listener: ListenerInfo) => boolean;
        if (isAllId) {
            check = (listener: ListenerInfo) => isAll || capture === listener.capture;
        } else {
            idList = (isArray(idOrAll) ? idOrAll : [idOrAll as string]).filter(Boolean);
            check = (listener: ListenerInfo) =>
                (isAll || capture === listener.capture) && idList.includes(listener.id);
        }
        const eventListenersMap = (target as any)[eventListenersName];
        for (const type in eventListenersMap) {
            removeEvent(target, type, check, removeEventListener);
        }
        isAll = capture = idList = null as any;
    };
    if (_EventTarget) {
        const _addEventListener = _EventTarget.prototype.addEventListener;
        const _dispatchEvent = _EventTarget.prototype.dispatchEvent;
        const _removeEventListener = _EventTarget.prototype.removeEventListener;
        Object.defineProperties(_EventTarget.prototype, {
            addEventListener: {
                ...Object.getOwnPropertyDescriptor(_EventTarget.prototype, 'addEventListener'),
                value: function addEventListener(
                    type: string,
                    callback: EventListenerParam,
                    options?: EventListenerOptionsParam<AddEventListenerOptions>,
                ) {
                    let listenerInfo = addEvent(this, type, callback, options);
                    if (!listenerInfo) {
                        return;
                    }
                    if (!listenerInfo.listener) {
                        listenerInfo.listener = (event: Event) => {
                            const _listener = listenerInfo!;
                            if (!_listener) {
                                return;
                            }
                            callListener(_listener, event);
                            if (_listener.once && _listener.called) {
                                listenerInfo = null as any;
                            }
                        };
                    }
                    _addEventListener.call(this, type, listenerInfo.listener, options);
                    return listenerInfo.id;
                },
            },
            dispatchEvent: {
                ...Object.getOwnPropertyDescriptor(_EventTarget.prototype, 'dispatchEvent'),
                value: function dispatchEvent(
                    event: Event | string,
                    detail?: object,
                    options?: TriggerEventOptions,
                ): boolean {
                    const eventObject = toEvent(event, detail, options);
                    const type = eventObject.type;
                    const returnValue = _dispatchEvent(eventObject);
                    removeCalledOnce(getEventListeners(this, type));
                    return returnValue;
                },
            },
            removeEventListener: {
                ...Object.getOwnPropertyDescriptor(_EventTarget.prototype, 'removeEventListener'),
                value: function removeEventListener(
                    type: string,
                    callback: EventListenerParam,
                    options?: EventListenerOptionsParam,
                ) {
                    _removeEventListener(type, callback, options);
                    removeEventByCallback(this, type, callback, options);
                },
            },
            removeEventListenerByType: {
                value: function removeEventListenerByType(
                    type: string | string[],
                    options?: EventListenerOptionsParam,
                ) {
                    removeEventByType(this, type, options, _removeEventListener);
                },
            },
            removeEventListenerById: {
                value: function removeEventListenerById(
                    id: string | string[],
                    options?: EventListenerOptionsParam,
                ) {
                    removeEventById(
                        this,
                        (id as any) === true ? (null as any) : id,
                        options,
                        _removeEventListener,
                    );
                },
            },
            removeAllEventListener: {
                value: function removeAllEventListener(options?: EventListenerOptionsParam) {
                    removeEventById(this, true, options, _removeEventListener);
                },
            },
        });
        return setIsEventTargetPolyfill(_EventTarget);
    }
    const dispatchEvent = function (target: EventTarget, event: Event) {
        if (event.target == null) {
            (event as any).target = target;
        }
        (event as any).currentTarget = target;
        const listeners = getEventListeners(target, event.type);
        if (!listeners || listeners.length === 0) {
            return true;
        }
        const listenerList = listeners.concat();
        for (let i = 0, l = listenerList.length, listener; i < l; i++) {
            listener = listenerList[i];
            try {
                callListener(listener, event);
            } catch (error) {
                if (globalThis && (globalThis as any).dispatchEvent) {
                    (globalThis as any).dispatchEvent(createErrorEvent(error));
                }
                console.error(error);
            }
            if (event.immediatePropagationStopped) {
                break;
            }
        }
        removeCalledOnce(listeners);
        (event as any).currentTarget = null;
        if (!(event as any).cancelBubble) {
            const { parentNode } = target as any;
            if (parentNode && parentNode != target) {
                dispatchEvent(parentNode, event);
            }
        }
        return true;
    };

    class EventTarget implements EventTarget {
        constructor() {
            Object.defineProperty(this, eventListenersName, { value: Object.create(null) });
        }
        addEventListener(
            type: string,
            callback: EventListenerParam,
            options?: EventListenerOptionsParam<AddEventListenerOptions>,
        ): typeof callback extends null ? void : string {
            const argumentsLength = arguments.length;
            if (argumentsLength < 2) {
                throw new TypeError(
                    `Failed to execute 'removeEventListener' on 'EventTarget': 2 arguments required, but only ${argumentsLength} present.`,
                );
            }
            return addEvent(this, type, callback, options, callback)?.id as any;
        }
        dispatchEvent(
            event: Event | string,
            detail?: object,
            options?: TriggerEventOptions,
        ): boolean {
            const eventObject = toEvent(event, detail, options);
            return dispatchEvent(this, eventObject);
        }
        removeEventListener(
            type: string,
            callback: EventListenerParam,
            options?: EventListenerOptionsParam,
        ): void {
            const argumentsLength = arguments.length;
            if (argumentsLength < 2) {
                throw new TypeError(
                    `Failed to execute 'removeEventListener' on 'EventTarget': 2 arguments required, but only ${argumentsLength} present.`,
                );
            }
            removeEventByCallback(this, type, callback, options);
        }

        removeEventListenerByType(
            type: string | string[],
            options?: EventListenerOptionsParam,
        ): void {
            removeEventByType(this, type, options);
        }
        removeEventListenerById(id: string | string[], options?: EventListenerOptionsParam): void {
            removeEventById(this, (id as any) === true ? (null as any) : id, options);
        }
        removeAllEventLListener(options?: EventListenerOptionsParam): void {
            removeEventById(this, true, options);
        }
    }
    return setIsEventTargetPolyfill(EventTarget);
};
export const EventTarget = EventTargetPolyfill();
export default EventTarget;
