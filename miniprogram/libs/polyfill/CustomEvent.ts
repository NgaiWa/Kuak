/**
 * @author Fa
 * @copyright Fa 2017
 */

import { Event } from './Event';
const CustomEventPolyfill = function (): CustomEventConstructor {
    const _CustomEvent = (globalThis as unknown as { CustomEvent: CustomEventConstructor })
        .CustomEvent;
    if (_CustomEvent) {
        return _CustomEvent;
    }
    class CustomEvent<T = any> extends Event {
        readonly detail!: T;
        constructor(type: string, eventInitDict?: CustomEventInit<T>) {
            super(type, eventInitDict);
            if (eventInitDict) {
                this.detail = eventInitDict.detail!;
            }
        }
        /**
         * @deprecated
         */
        initCustomEvent(type: string, bubbles?: boolean, cancelable?: boolean, detail?: T): void {
            void type;
            void bubbles;
            void cancelable;
            void detail;
        }
    }
    return CustomEvent;
};

export const CustomEvent = CustomEventPolyfill();
export default CustomEvent;
