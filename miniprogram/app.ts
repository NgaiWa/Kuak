// app.ts
import './load';

import { generateKey } from './libs/utils/string';
App.launch<IAppOption>({
    key: '',
    globalData: {},
    state: {},
    onLaunch() {
        this.key = App.store.get('appKey');
        if (!this.key) {
            App.store.set('appKey', (this.key = 'app_' + generateKey()));
        }
    },
});
