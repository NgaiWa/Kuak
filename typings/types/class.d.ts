/**
 * @author Fa
 * @copyright Fa 2017
 */
declare type GetProto<Class> = Class extends FunctionConstructor ? Class['prototype'] : unknown;
declare interface IClass<Prototype> {
    new (...args: any[]): Prototype;
    prototype: Prototype;
}
declare type TClass<
    Class,
    Prototype,
    P1 = unknown,
    P2 = unknown,
    P3 = unknown,
    P4 = unknown,
    P5 = unknown,
    P6 = unknown,
    P7 = unknown,
    P8 = unknown,
    P9 = unknown,
> = Class & IClass<Mixins<GetProto<Class> & Prototype, P1, P2, P3, P4, P5, P6, P7, P8, P9>>;

declare type Mixins<
    T,
    M1,
    M2 = unknown,
    M3 = unknown,
    M4 = unknown,
    M5 = unknown,
    M6 = unknown,
    M7 = unknown,
    M8 = unknown,
    M9 = unknown,
> = M1 & M2 & M3 & M4 & M5 & M6 & M7 & M2 & M3 & M4 & M5 & M6 & M7 & M8 & M9 & T;
declare type MixinsFunc = <
    T,
    M1,
    M2 = unknown,
    M3 = unknown,
    M4 = unknown,
    M5 = unknown,
    M6 = unknown,
    M7 = unknown,
    M8 = unknown,
    M9 = unknown,
>(
    target: T,
    m1: M1,
    m2?: M2,
    m3?: M3,
    m4?: M4,
    m5?: M5,
    m6?: M6,
    m7?: M7,
    m8?: M8,
    m9?: M9,
    ...args: any[]
) => Mixins<T, M1, M2, M3, M4, M5, M6, M7, M8, M9>;
declare type CMixins<
    C extends IConstructor = IConstructor,
    P1 = unknown,
    P2 = unknown,
    P3 = unknown,
    P4 = unknown,
    P5 = unknown,
    P6 = unknown,
    P7 = unknown,
    P8 = unknown,
    P9 = unknown,
> = C &
    IClass<
        C['prototype'] & P1 & P2 & P3 & P4 & P5 & P6 & P7 & P2 & P3 & P4 & P5 & P6 & P7 & P8 & P9
    >;

type CMixinsFunc = <
    C extends IConstructor = IConstructor,
    P1 = unknown,
    P2 = unknown,
    P3 = unknown,
    P4 = unknown,
    P5 = unknown,
    P6 = unknown,
    P7 = unknown,
    P8 = unknown,
    P9 = unknown,
>(
    target: C,
    p1: P1,
    p2?: P2,
    p3?: P3,
    p4?: P4,
    p5?: P5,
    p6?: P6,
    p7?: P7,
    p8?: P8,
    p9?: P9,
    ...args: any[]
) => CMixins<C, P1, P2, P3, P4, P5, P6, P7, P8, P9>;

declare type MultiInherit<
    C extends IConstructor = IConstructor,
    C1 extends IConstructor = IConstructor,
    C2 extends IConstructor = IConstructor,
    C3 extends IConstructor = IConstructor,
    C4 extends IConstructor = IConstructor,
    C5 extends IConstructor = IConstructor,
    C6 extends IConstructor = IConstructor,
    C7 extends IConstructor = IConstructor,
    C8 extends IConstructor = IConstructor,
    C9 extends IConstructor = IConstructor,
> = C &
    C1 &
    C2 &
    C3 &
    C4 &
    C5 &
    C6 &
    C7 &
    C8 &
    C9 &
    IClass<
        C['prototype'] &
            C1['prototype'] &
            C2['prototype'] &
            C3['prototype'] &
            C4['prototype'] &
            C5['prototype'] &
            C6['prototype'] &
            C7['prototype'] &
            C8['prototype'] &
            C9['prototype']
    >;
declare type MultiInheritFunc = <
    C extends IConstructor = IConstructor,
    C1 extends IConstructor = IConstructor,
    C2 extends IConstructor = IConstructor,
    C3 extends IConstructor = IConstructor,
    C4 extends IConstructor = IConstructor,
    C5 extends IConstructor = IConstructor,
    C6 extends IConstructor = IConstructor,
    C7 extends IConstructor = IConstructor,
    C8 extends IConstructor = IConstructor,
    C9 extends IConstructor = IConstructor,
>(
    c: C,
    c1: C1,
    c2?: C2,
    c3?: C3,
    c4?: C4,
    c5?: C5,
    c6?: C6,
    c7?: C7,
    c8?: C8,
    c9?: C9,
    ...args: IConstructor[]
) => MultiInherit<C, C1, C2, C3, C4, C5, C6, C7, C8, C9>;
