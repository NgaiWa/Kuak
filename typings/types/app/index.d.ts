/**
 * @author Fa
 * @copyright Fa 2017
 */
/// <reference path="./Miniprogram.d.ts" />
/// <reference path="./core.d.ts" />
/// <reference path="./App.d.ts" />
/// <reference path="./Base.d.ts" />
/// <reference path="./Component.d.ts" />
/// <reference path="./Page.d.ts"/>
declare let wx: Program.Lib;
declare let qq: Program.Lib;
declare let console: Program.Console;
declare let App: Program.App;
declare let Component: Program.Component;
declare let Page: Program.Page;
declare let getCurrentPages: Program.GetCurrentPages;
