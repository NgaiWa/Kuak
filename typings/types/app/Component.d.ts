/**
 * @author Fa
 * @copyright Fa 2017
 */
declare namespace Program {
    interface Component extends Program.Component.Constructor {
        Base: Kuak.ComponentConstructor;
        Components: { [n: string]: any };
        lib: Lib;
        register(Component: Kuak.ComponentConstructor): ReturnType<Component>;
    }
}
declare namespace Kuak {
    interface Component
        extends Kuak.ComponentBase,
            Program.Component.Instance<AnyObject, AnyObject, AnyObject> {
        onCreate(): any;
        onLoad(): any;
        onShow(): any;
        onReady(): any;
        onMoved(): any;
        onHide(): any;
        onUnload(): any;
        onError(error: Error): any;
        onPageShow(): any;
        onPageHide(): any;
        onPageResize(size: IPageResizeOption): any;
        onRouteDone(): any;
    }
    interface ComponentConstructor extends Kuak.ComponentBaseConstructor {
        new (...args: any[]): Component;
        prototype: Component;
        properties?: Program.Component.PropertyOption;
        register<R = ReturnType<Program.Component>>(): R;
        registerOptions: {
            properties: ComponentConstructor['properties'];
            lifetimes: {
                created(): any;
                attached(): any;
                ready(): any;
                moved(): any;
                detached(): any;
                error(error: Omit<Error, 'name'>): void;
            };
            pageLifetimes: {
                show(): any;
                hide(): any;
                resize(options: AnyObject): any;
                routeDone(): any;
            };
        };
    }
}
