/**
 * @author Fa
 * @copyright Fa 2017
 */

declare namespace Program {
    interface App {
        Page: Page;
        Pages: Page['Pages'];
        Component: Component;
        Components: Component['Components'];
        lib: Lib;
        libs: Libs;
        store: IStore;
        servers: Kuak.IServers;
        history: Kuak.History;
        router: Kuak.NavRouter;
        env: AnyObject;
        get<T extends AnyObject = IAppOption>(): Program.App.Instance<T>;
        launch<T extends AnyObject = IAppOption>(
            options: Program.App.Options<T>,
        ): Program.App.Instance<T>;
    }
}
