declare namespace Kuak {
    interface History {
        maxLimit: { webview: number; skyline: number };
        at(index: number): Page | void;
        indexOf(page: Page): number;
        getByIndex(index: number): Page | void;
        getByPath(path: string): Kuak.Page[];
        getById(id: string): Page | void;
        back(): Promise<any>;
        forward(): Promise<any>;
        go(index: number): Promise<any>;
        onPagesChange(option: { page: Kuak.Page; isNew?: boolean; isOld?: boolean }): any;
        currentOpened: Page;
        currentShowed: Page;
        currentPage: Page;
        currentPages: Page[];
        restoredPage?: Page;
        length: number;
        currentLength: number;
        getInfo(): any;
    }
    interface HistoryConstructor {
        prototype: History;
    }
}
