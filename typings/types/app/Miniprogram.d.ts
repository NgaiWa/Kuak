/**
 * @author Fa
 * @copyright Fa 2017
 */

declare namespace Program {
    type _Lib = WechatMiniprogram.Wx;
    type _Console = WechatMiniprogram.Console;
    type AnyObject = WechatMiniprogram.IAnyObject;
    interface SystemInfo extends WechatMiniprogram.SystemInfo {
        screenTop: number;
    }
    namespace App {
        type Instance<T extends AnyObject> = WechatMiniprogram.App.Instance<T>;
        type Constructor = WechatMiniprogram.App.Constructor;
        type Options<T extends AnyObject> = WechatMiniprogram.App.Options<T>;
    }

    namespace Component {
        type Constructor = WechatMiniprogram.Component.Constructor;
        type DataOption = WechatMiniprogram.Component.DataOption;
        type PropertyOption = WechatMiniprogram.Component.PropertyOption;
        type MethodOption = WechatMiniprogram.Component.MethodOption;
        type IAnyObject = WechatMiniprogram.Component.MethodOption;
        type Instance<
            TData extends DataOption,
            TProperty extends PropertyOption,
            TMethod extends Partial<MethodOption>,
            TCustomInstanceProperty extends IAnyObject = IAnyObject,
            TIsPage extends boolean = false,
        > = Omit<
            WechatMiniprogram.Component.Instance<
                TData,
                TProperty,
                TMethod,
                TCustomInstanceProperty,
                TIsPage
            >,
            'setData' | 'triggerEvent'
        >;
    }
    namespace Page {
        type DataOption = WechatMiniprogram.Page.DataOption;
        type CustomOption = WechatMiniprogram.Page.CustomOption;
        type Constructor = WechatMiniprogram.Page.Constructor;
        type Instance<
            TData extends DataOption,
            TCustom extends CustomOption,
        > = WechatMiniprogram.Page.InstanceProperties &
            Omit<WechatMiniprogram.Page.InstanceMethods<TData>, 'setData' | 'triggerEvent'> &
            WechatMiniprogram.Page.Data<TData> &
            TCustom;
    }
}
