/**
 * @author Fa
 * @copyright Fa 2017
 */
declare namespace Program {
    interface Page extends Program.Page.Constructor {
        Base: Kuak.PageConstructor;
        Pages: { [n: string]: any };
        lib: Lib;
        register(Page: Kuak.PageConstructor): ReturnType<Page>;
    }
}
declare namespace Kuak {
    interface Page extends ComponentBase, Program.Page.Instance<AnyObject, AnyObject> {
        index?: number;
        isRestoredPage: boolean;
        restoreInitPage(
            restoredPage: Partial<Page>,
            newData?: AnyObject,
            options?: { forDataObserver?: true | false },
        ): boolean;
        onRestoredPageDispose(): any;
        /**
         * @deprecated 使用 onDispose
         */
        onUnload(): any;
        onResize(options?: Kuak.IPageResizeOption): any;
        onCreate(options: AnyObject, restoredPage?: Page): any;
        onPageShow(): any;
        onPageHide(): any;
        onPageResize(options?: IPageResizeOption): any;
    }
    interface PageConstructor extends ComponentBaseConstructor {
        new (...args: any[]): Page;
        prototype: Page;
        register<R = ReturnType<Program.Page>>(): R;
        registerOptions: {
            onLoad(options: AnyObject): any;
        };
    }
}
