/**
 * @author Fa
 * @copyright Fa 2017
 */
declare namespace Node {
    type SelectorQuery = WechatMiniprogram.SelectorQuery;
    type NodesRef = WechatMiniprogram.NodesRef;
    type Fields = WechatMiniprogram.Fields;
    type FieldsCallback = WechatMiniprogram.FieldsCallback;
    type ScrollOffsetCallback = WechatMiniprogram.ScrollOffsetCallback;
    type ScrollOffsetCallbackResult = WechatMiniprogram.ScrollOffsetCallbackResult;
    type BoundingClientRectCallback = WechatMiniprogram.BoundingClientRectCallback;
    type BoundingClientRectCallbackResult = WechatMiniprogram.BoundingClientRectCallbackResult;
    type ContextCallback = WechatMiniprogram.ContextCallback;
    type ContextCallbackResult = WechatMiniprogram.ContextCallbackResult;
}

declare namespace Kuak {
    type IPageResizeOption = WechatMiniprogram.Page.IResizeOption;

    interface ComponentBase extends EventTarget {
        externalEvents: IEventMap;
        lib: Program.Lib;
        data: AnyObject;
        initData(
            data: AnyObject,
            option?: {
                /**
                 * 是否触发数据监听
                 * @default true
                 */
                forDataObserver?: true | false;
            },
        ): AnyObject;
        useStore(store: IStore, keys: string[], isReuse?: boolean): any;
        useStore(store: IStore, alias: AliasNameMap, isReuse?: boolean): any;
        setData(data: AnyObject, callback?: () => void): any;
        setData(dataPaths: string[], callback?: () => void): any;
        setData(dataPath: string, callback?: () => void): any;
        triggerEvent(type: string, detail?: object, options?: TriggerEventOptions): void;
        router: NavRouter;
        pageRouter: NavRouter;
        onDispose(): any;
        onDisposeEvents(): any;
    }
    interface ComponentBaseConstructor {
        lib: Program.Lib;
        prototype: ComponentBase;
        registerOptions: AnyObject;
        setInitialBase(proto: any): this;
        bindInstance<Instance = any>(instance: Instance): Instance;
        dataObservers?: {
            [namePathWithCommaSeparated: string]: AnyFunction;
        };
        defineStatic(descMap: any): this;
        defineProto(descMap: any): this;
        extendStatic(...statics: any[]): this;
        extendProto(...sources: any[]): this;
    }
}
