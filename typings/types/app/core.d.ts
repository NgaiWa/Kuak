/**
 * @author Fa
 * @copyright Fa 2017
 */
declare namespace Program {
    interface GetCurrentPages {
        (): Array<Kuak.Page>;
    }
    interface Lib extends _Lib {
        Promise: IPromiseConstructor;
        libs: Libs;
        [n: string]: any;
    }
    interface Libs {
        [n: string]: any;
    }
    interface Console extends _Console {
        dir: Program.Console['log'];
    }
}
