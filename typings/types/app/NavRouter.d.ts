declare namespace Kuak {
    interface NavRouter {
        navHistory: Kuak.History;
        navigateTo(option: {
            url: string;
            success?: AnyFunction;
            fail?: AnyFunction;
            complete?: AnyFunction;
        }): Promise<any>;
        navigateTo(url: string): Promise<any>;
        navigateBack(option?: {
            delta: number;
            success?: AnyFunction;
            fail?: AnyFunction;
            complete?: AnyFunction;
        }): Promise<any>;
        navigateBack(delta?: number): Promise<any>;
        redirectTo(option: {
            url: string;
            success?: AnyFunction;
            fail?: AnyFunction;
            complete?: AnyFunction;
        }): Promise<any>;
        redirectTo(url: string): Promise<any>;
        switchTab(option: {
            url: string;
            success?: AnyFunction;
            fail?: AnyFunction;
            complete?: AnyFunction;
        }): Promise<any>;
        switchTab(url: string): Promise<any>;
        reLaunch(option: {
            url: string;
            success?: AnyFunction;
            fail?: AnyFunction;
            complete?: AnyFunction;
        }): Promise<any>;
        reLaunch(url: string): Promise<any>;
        dir?: string;
    }
}
