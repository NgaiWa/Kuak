/**
 * @author Fa
 * @copyright Fa 2017
 */

declare interface ICalleeQueueItem extends AnyObject {
    callee: ICallee;
}
declare type TCalleeQueueItem<T = unknown> = ICalleeQueueItem & T;
declare type ICalleeQueue<T = unknown> = TCalleeQueueItem<T>[];
declare type IFuncQueue = AnyFunction[];
declare interface ICalleeQueuer {
    static: ICalleeQueuerConstructor;
    queue: ICalleeQueue;
    exec(...args: any[]): void;
    apply(args: any[]): void;
    dispose(): void;
    boundExec(): ICalleeQueuer['exec'];
    boundDispose(): ICalleeQueuer['dispose'];
    addItem(...item: ICalleeQueueItem[]): void;
    addItems(item: ICalleeQueueItem[]): void;
    addCallee(callee: ICallee, options?: AnyObject): void;
    addCallees(params: CalleeQueueCalleeParams[], options?: AnyObject): void;
    addFunc(func: ICallee['func'], args?: any[], once?: boolean, options?: AnyObject): any;
    addFuncs(params: CalleeQueueFuncItemParams[], options?: AnyObject): any;
}
declare type CalleeQueueFuncItemParams = [
    func: ICallee['func'],
    args?: any[],
    once?: boolean,
    options?: AnyObject,
];
declare type CalleeQueueCalleeParams = [callee: ICallee, options?: AnyObject];
declare interface ICalleeQueuerConstructor {
    new (): ICalleeQueuer;
    createQueue(): ICalleeQueue;
    createItem(callee: ICallee, options?: AnyObject): ICalleeQueueItem;
    createItems(params: CalleeQueueCalleeParams[], options?: AnyObject): ICalleeQueueItem[];
    createFuncItem(
        func: ICallee['func'],
        args?: any[],
        once?: boolean,
        options?: AnyObject,
    ): ICalleeQueueItem;
    createFuncItems(params: CalleeQueueFuncItemParams[], options?: AnyObject): ICalleeQueueItem[];
    addItem(queue: ICalleeQueue, ...items: ICalleeQueueItem[]): void;
    addItems(queue: ICalleeQueue, items: ICalleeQueueItem[]): void;
    addCallee(queue: ICalleeQueue, callee: ICallee, options?: AnyObject): void;
    addCallees(queue: ICalleeQueue, params: CalleeQueueCalleeParams[], options?: AnyObject): void;
    addFunc(
        queue: ICalleeQueue,
        func: ICallee['func'],
        args?: any[],
        once?: boolean,
        options?: AnyObject,
    ): any;
    addFuncs(queue: ICalleeQueue, params: CalleeQueueFuncItemParams[], options?: AnyObject): any;
    execQueue(queue: ICalleeQueue, ...args: any[]): void;
    applyQueue(queue: ICalleeQueue, args: any[]): void;
}
