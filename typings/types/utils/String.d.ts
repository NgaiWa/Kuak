/**
 * @author Fa
 * @copyright Fa 2017
 */
declare namespace StringUtil {
    type Replace<
        Source extends string,
        SearchValue extends string,
        ReplaceValue extends string,
        All extends boolean = false,
    > = Source extends ''
        ? Source
        : Source extends `${infer Start}${SearchValue}${infer End}`
          ? All extends false
              ? `${Start}${Source}${End}`
              : `${Start}${Replace<End, SearchValue, ReplaceValue>}`
          : Source;

    type UpperCaseLetters =
        | 'A'
        | 'B'
        | 'C'
        | 'D'
        | 'E'
        | 'F'
        | 'G'
        | 'H'
        | 'I'
        | 'J'
        | 'K'
        | 'L'
        | 'M'
        | 'N'
        | 'O'
        | 'P'
        | 'Q'
        | 'R'
        | 'S'
        | 'T'
        | 'U'
        | 'V'
        | 'W'
        | 'X'
        | 'Y'
        | 'Z';
    type LowerCaseLetters =
        | 'a'
        | 'b'
        | 'c'
        | 'd'
        | 'e'
        | 'f'
        | 'g'
        | 'h'
        | 'i'
        | 'j'
        | 'k'
        | 'l'
        | 'm'
        | 'n'
        | 'o'
        | 'p'
        | 'q'
        | 'r'
        | 's'
        | 't'
        | 'u'
        | 'v'
        | 'w'
        | 'x'
        | 'y'
        | 'z';

    type NumberCase = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';
    interface CaseAlphabet {
        A: 'a';
        a: 'A';
        B: 'b';
        b: 'B';
        C: 'c';
        c: 'C';
        D: 'd';
        d: 'D';
        E: 'e';
        e: 'E';
        F: 'f';
        f: 'F';
        G: 'g';
        g: 'G';
        H: 'h';
        h: 'H';
        I: 'i';
        i: 'I';
        J: 'j';
        j: 'J';
        K: 'k';
        k: 'K';
        L: 'l';
        l: 'L';
        M: 'm';
        m: 'M';
        N: 'n';
        n: 'N';
        O: 'o';
        o: 'O';
        P: 'p';
        p: 'P';
        Q: 'q';
        q: 'Q';
        R: 'r';
        r: 'R';
        S: 's';
        s: 'S';
        T: 't';
        t: 'T';
        U: 'u';
        u: 'U';
        V: 'v';
        v: 'V';
        W: 'w';
        w: 'W';
        X: 'x';
        x: 'X';
        Y: 'y';
        y: 'Y';
        Z: 'z';
        z: 'Z';
        0: '0';
        1: '1';
        2: '2';
        3: '3';
        4: '4';
        5: '5';
        6: '6';
        7: '7';
        8: '8';
        9: '9';
    }
    type CaseLetters = keyof CaseAlphabet;
    type ToUpperCase<Source extends string> = Source extends ''
        ? Source
        : Source extends `${infer Char}${infer Chars}`
          ? `${Char extends LowerCaseLetters ? CaseAlphabet[Char] : Char}${ToUpperCase<Chars>}`
          : Source;
    type ToLowerCase<Source extends string> = Source extends ''
        ? Source
        : Source extends `${infer Char}${infer Chars}`
          ? `${Char extends UpperCaseLetters ? CaseAlphabet[Char] : Char}${ToLowerCase<Chars>}`
          : Source;
    type ToDelimiterCase<
        Source extends string,
        Delimiter extends '_' | '-' = '_',
        SkipFirst extends boolean = false,
    > = Source extends ''
        ? Source
        : Source extends `${infer Char}${infer Chars}`
          ? SkipFirst extends true
              ? `${Char}${ToDelimiterCase<Chars, Delimiter, false>}`
              : `${Char extends UpperCaseLetters
                    ? `${Delimiter}${CaseAlphabet[Char]}`
                    : Char}${ToDelimiterCase<Chars, Delimiter, false>}`
          : Source;
    type ToSnakeCase<Source extends string, SkipFirst extends boolean = false> = ToDelimiterCase<
        Source,
        '_',
        SkipFirst
    >;
    type ToSpinalCase<Source extends string, SkipFirst extends boolean = false> = ToDelimiterCase<
        Source,
        '-',
        SkipFirst
    >;
    type ToCamelCase<Source extends string> =
        Source extends `${infer Char1}${infer Char2}${infer Chars}`
            ? `${Char1}${Char2}` extends `${'_' | '-'}${CaseLetters}`
                ? `${Char2 extends LowerCaseLetters
                      ? CaseAlphabet[Char2]
                      : Char2}${ToCamelCase<`${Chars}`>}`
                : `${Char1}${ToCamelCase<`${Char2}${Chars}`>}`
            : Source;
    type _ToPascalCase<Source extends string, IsFalse extends boolean = true> = IsFalse extends true
        ? Source extends `${infer Char1}${infer Chars}`
            ? Char1 extends LowerCaseLetters
                ? `${CaseAlphabet[Char1]}${_ToPascalCase<Chars, false>}`
                : ToCamelCase<Source>
            : ToCamelCase<Source>
        : ToCamelCase<Source>;
    type ToPascalCase<Source extends string> = _ToPascalCase<Source>;
}

type Source = StringUtil.ToPascalCase<'_abcd_e'>;
