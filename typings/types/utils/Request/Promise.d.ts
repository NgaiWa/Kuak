/**
 * @author Fa
 * @copyright Fa 2017
 */
interface IRequestPromiseRes<Config, Task> {
    id: string;
    task: Task;
    config: Config;
}
declare interface IRequestPromise<
    Response = IResponse,
    Config extends IRequestConfig = IRequestConfig,
    Task extends IRequestBaseTask = IRequestTask,
    R extends IRequestPromiseRes<Config, Task> = IRequestPromiseRes<Config, Task>,
> extends IPromise<Response, R> {
    task: this['res']['task'];
    off(name: string): void;
    offAll(): void;
    get<Name extends string>(
        name: Name,
    ): IRequestPromise<GetResponseValueByName<Response, Name>, Config, Task, R>;
    getData(): IRequestPromise<GetResponseData<Response>, Config, Task, R>;
}
