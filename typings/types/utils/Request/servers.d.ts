/**
 * @author Fa
 * @copyright Fa 2017
 */

declare namespace Kuak {
    interface IServers {
        baseServer: IRequester;
        apiServer: IRequester;
        staticServer: IRequester;
        [n: string]: IRequester;
    }
}
