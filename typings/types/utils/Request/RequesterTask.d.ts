/**
 * @author Fa
 * @copyright Fa 2017
 */

declare type IRequesterTaskCallee = (config?: IRequesterTaskConfig, task?: IRequesterTask) => void;
declare interface IRequesterTaskCanceler {
    abort: IRequesterTaskCallee;
    cancel: IRequesterTaskCallee;
    [n: string]: any;
}
interface IRequesterTaskCloneMode {
    none: 0;
    caughtBefore: 1; // catch 处理之前
    caughtAfter: 2; // catch 处理之后
}
declare interface IRequesterTaskConfig {
    cache?: boolean | number;
    delay?: number;
    one?: boolean | IRequesterTaskCanceler;
    canceler?: IRequesterTaskCanceler;
    clear?: IRequesterTaskCallee;
    clearCache?: IRequesterTaskCallee;
    caught?: (value: any) => any;
    clone?: (value: any) => any;
    cloneMode?:
        | IRequesterTaskCloneMode['none']
        | IRequesterTaskCloneMode['caughtBefore']
        | IRequesterTaskCloneMode['caughtAfter'];
    isKeepArgs?: boolean;
    [n: string]: any;
}
declare interface IRequesterTask<
    Request extends (task: IRequesterTaskCanceler, ...args: any) => any = AnyFunction,
    Config extends IRequesterTaskConfig = IRequesterTaskConfig,
> {
    res?: { task: IRequesterTaskCanceler };
    config: Config;
    request(...args: Parameters<Request>): ReturnType<Request>;
    requestBind<P extends Partial<_P>, _P extends any[] = [...Parameters<Request>]>(
        ...args: P
    ): (..._args: SliceParams<_P, P>) => ReturnType<Request>;
    cancel(): void;
    destroy(): void;
    refresh(doRequest?: boolean | null): ReturnType<Request> | null;
    clear(): void;
    clearCache(): void;
    isExpired(time?: number): void;
    checkExpiredClear(time?: number): void;
    getTask<T = IRequestTask>(): Config['delay'] extends number ? Promise<T> : T;
    off(name: string): void;
    offAll(): void;
}

declare interface IRequesterTaskConstructor {
    new <
        Request extends AnyFunction = AnyFunction,
        Config extends IRequesterTaskConfig = IRequesterTaskConfig,
    >(): IRequesterTask<Request, Config>;
}
