/**
 * @author Fa
 * @copyright Fa 2017
 */
declare enum IHttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    HEAD = 'HEAD',
    OPTIONS = 'OPTIONS',
    TRACE = 'TRACE',
    CONNECT = 'CONNECT',
}
declare enum ContentTypes {
    Json = 'application/json;charset=UTF-8',
    FormUrlEncoded = 'application/x-www-form-urlencoded;charset=UTF-8',
    FormData = 'multipart/form-data;charset=UTF-8',
}
declare type IRequestConfigHttpMethod =
    | 'GET'
    | 'POST'
    | 'PUT'
    | 'DELETE'
    | 'HEAD'
    | 'OPTIONS'
    | 'TRACE'
    | 'CONNECT';
declare type IRequestConfigHttpMethodLower =
    | 'get'
    | 'post'
    | 'put'
    | 'delete'
    | 'head'
    | 'options'
    | 'trace'
    | 'connect';

declare interface IRequestHeader {
    [n: string]: any;
}
declare interface IResponseHeader {
    [n: string]: any;
}
declare interface IRequestCookies extends Iterable<string> {}
declare interface IRequestProfile extends WechatMiniprogram.RequestProfile {}
declare interface IRequestReason {
    errMsg: string; // 错误原因
    errno: string; // 错误码
    status: number;
    code: number;
}
declare interface IRequestException {
    retryCount: number; // 本次请求底层重试次数
    reasons: number; // 本次请求底层重试次数
}
declare interface IDefaultRequestHooks {
    config(config: IRequestConfig): IRequestConfig;
    header(header: IRequestHeader, config: IRequestConfig): IRequestHeader;
    params(params: any, config: IRequestConfig): any;
    data(data: any, config: IRequestConfig): any;
    url(url: string, config: IRequestConfig): string;
}
declare interface IDefaultResponseHooks {
    response(response: IResponse): IResponse;
    header(header: IResponseHeader, response: IResponse): IResponseHeader;
    data(data: any, response: IResponse): any;
    status(status: number, response: IResponse): number;
    code(code: number, response: IResponse): number;
    successful(successful: boolean, response: IResponse): boolean;
    message(message: string, response: IResponse): string;
}
declare interface IRequestConfig {
    // 条件值
    isDownload?: boolean;
    isUpload?: boolean;
    // 请求的公共属性
    url: string;
    baseUrl?: string;
    urlAlias?: { [n: string]: string };
    header?: IRequestHeader;
    timeout?: number;
    // 普通请求可选属性
    method?: IRequestConfigHttpMethod;
    params?: string | AnyObject;
    data?: string | AnyObject | ArrayBuffer;
    contentType?: ContentTypes;
    dataType?: 'json' | '其他';
    responseType?: 'text' | 'arraybuffer';
    enableHttp2?: false | true;
    enableQuic?: false | true;
    enableCache?: false | true;
    enableHttpDNS?: false | true;
    httpDNSServiceId?: string;
    enableChunked?: false | true;
    forceCellularNetwork?: false | true;
    on?: {
        headersReceived?: AnyFunction;
        chunkReceived?: AnyFunction;
        progressUpdate?: AnyFunction;
        [n: string]: ICallee | AnyFunction | undefined;
    };
    retryStatus?: number[];
    retryCount?: number[];
    requestHooks?: {
        config?(
            config: IRequestConfig,
            defaultHook: IDefaultRequestHooks['config'],
        ): IRequestConfig;
        header?(
            header: IRequestHeader,
            config: IRequestConfig,
            defaultHook: IDefaultRequestHooks['header'],
        ): IRequestHeader;
        params?(
            params: any,
            config: IRequestConfig,
            defaultHook: IDefaultRequestHooks['params'],
        ): any;
        data?(data: any, config: IRequestConfig, defaultHook: IDefaultRequestHooks['data']): any;
        url?(url: string, config: IRequestConfig, defaultHook: IDefaultRequestHooks['url']): string;
    };
    responseHooks?: {
        response?(response: IResponse, defaultHook: IDefaultResponseHooks['response']): IResponse;
        header?(
            header: IResponseHeader,
            response: IResponse,
            defaultHook: IDefaultResponseHooks['header'],
        ): IResponseHeader;
        data?(data: any, response: IResponse, defaultHook: IDefaultResponseHooks['data']): any;
        status?(
            status: number,
            response: IResponse,
            defaultHook: IDefaultResponseHooks['status'],
        ): number;
        code?(
            status: number,
            response: IResponse,
            defaultHook: IDefaultResponseHooks['code'],
        ): number;
        successful?(
            successful: boolean,
            response: IResponse,
            defaultHook: IDefaultResponseHooks['successful'],
        ): boolean;
        message?(
            message: string,
            response: IResponse,
            defaultHook: IDefaultResponseHooks['message'],
        ): string;
    };

    codes?: {
        success: number[];
        timeout: number[];
        invalidLogin: number[];
    };
    actions?: {
        login?(response: IResponse, config: IRequestConfig): boolean;
        alert?(response: IResponse, config: IRequestConfig): void;
        [n: string]: AnyFunction | undefined;
    };
    auth?: {
        getToken(): string;
        scheme: string;
    };
    [n: string]: any;
}
declare type IRequestPartialConfig = Partial<IRequestConfig>;

declare enum RequestTaskStatus {
    UnRequest = 0,
    Requesting = 1,
    Requested = 2,
    Cancel = 3,
}
declare interface IRequestBaseTask {
    status: RequestTaskStatus;
    // set Task.status to RequestTaskStatus.Cancel and call Task.abort
    abort(): void;
    cancel(): void;
}
declare interface IResponse<Data = AnyObject> {
    header?: IResponseHeader;
    task?: IRequestTask | IUploadTask | IDownloadTask;
    config?: IRequestConfig;
    isError?: boolean;
    isCancel?: boolean;
    errorMessage?: string;
    successful: boolean;
    redirected?: boolean;
    status?: number;
    statusText?: string;
    data: Data;
    code: number;
    message: string;
}
declare interface IRequestTask extends IRequestBaseTask, WechatMiniprogram.RequestTask {}
declare interface IUploadTask extends IRequestBaseTask, WechatMiniprogram.UploadTask {}
declare interface IDownloadTask extends IRequestBaseTask, WechatMiniprogram.DownloadTask {}

declare type GetResponseData<Response> = Response extends { data: infer Data } ? Data : undefined;
declare type GetResponseValueByName<Response, Name extends string> = Response extends {
    [name in Name]: infer Value;
}
    ? Value
    : undefined;
