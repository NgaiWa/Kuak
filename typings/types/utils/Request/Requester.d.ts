/**
 * @author Fa
 * @copyright Fa 2017
 */
declare interface IRequester<DefaultConfig extends IRequestPartialConfig = IRequestConfig> {
    config: DefaultConfig;
    request<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config>;
    get<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config>;
    get<Response = unknown>(
        url: string,
    ): IRequestPromise<Response, DefaultConfig & { url: string }>;
    post<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config>;
    put<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config>;
    delete<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config>;
    delete<Response = unknown>(
        url: string,
    ): IRequestPromise<Response, DefaultConfig & { url: string }>;
    upload<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config, IUploadTask>;
    download<Response = unknown, Config extends IRequestConfig = IRequestConfig>(
        config: Config,
    ): IRequestPromise<Response, DefaultConfig & Config, IDownloadTask>;
}

declare interface IRequesterConstructor {
    new <DefaultConfig extends IRequestPartialConfig>(
        defaultConfig: DefaultConfig,
    ): IRequester<DefaultConfig>;
    prototype: IRequester;
    createTask<
        Request extends (...args: any) => IRequestPromise,
        Config extends IRequesterTaskConfig = IRequesterTaskConfig,
    >(
        request: Request,
        config: Config,
    ): IRequesterTask<Request, Config>;
    getTask<T = IRequestTask>(res: IRequestPromise['res']): T;
    off(res: IRequestPromise['res'], name: string): void;
    offAll(res: IRequestPromise['res']): void;
}
