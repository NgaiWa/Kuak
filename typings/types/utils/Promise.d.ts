/**
 * @author Fa
 * @copyright Fa 2017
 */

interface Promise<T> {
    caught(): this & Promise<T>;
    caughtVoid(): this & Promise<void>;
}

type GetPromiseRes<T, R = unknown> = T extends IPromise<any, any> ? T['res'] & R : R;
type GetPromiseMultiRes<
    T extends readonly unknown[] | readonly [unknown],
    R = unknown,
> = R extends unknown
    ? {
          -readonly [P in keyof T]: GetPromiseRes<T[P]>;
      }
    : R;
declare interface IPromise<T = unknown, R = GetPromiseRes<T>> extends Promise<T> {
    // executor return value
    res: R;
    finally(onfinally?: (() => void) | undefined | null): IPromise<T, R>;
    then<TResult1 = T, TResult2 = never>(
        onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
        onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
    ): this & IPromise<TResult1 | TResult2, R>;
    catch<TResult = never>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
    ): this & IPromise<T | TResult, R>;

    thenR<TResult = T, TR = R>(
        onfulfilled?: ((value: T) => TResult | PromiseLike<TResult>) | undefined | null,
        res?: TR,
    ): this & IPromise<TResult, TR>;
    catchR<TResult = T, TR = R>(
        onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
        res?: TR,
    ): this & IPromise<TResult, TR>;
    // thenR<TResult = T>(
    //     onfulfilled?: ((value: T) => TResult | PromiseLike<TResult>) | undefined | null,
    // ): this & IPromise<TResult, GetPromiseRes<TResult, R>>;

    // thenR<TResult1 = T, TResult2 = never>(
    //     onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null,
    //     onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null,
    // ): this & IPromise<TResult1 | TResult2, GetPromiseRes<TResult1 | TResult2, R>>;

    // catchR<TResult = T>(
    //     onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null,
    // ): this & IPromise<TResult, GetPromiseRes<TResult, R>>;
}

declare interface IPromiseConstructor extends PromiseConstructor {
    fn: { [n: string]: any };
    readonly prototype: IPromise<any>;
    new <T, R = GetPromiseRes<T>>(
        executor: (
            resolve: (value: T | PromiseLike<T>) => void,
            reject: (reason?: any) => void,
        ) => R,
    ): IPromise<T, R>;
    allR<T extends readonly unknown[] | readonly [unknown], R = unknown>(
        values: T,
        res?: R,
    ): IPromise<
        {
            -readonly [P in keyof T]: T[P] extends PromiseLike<infer U> ? U : T[P];
        },
        GetPromiseMultiRes<T, R>
    >;
    allSettledR<T extends readonly unknown[] | readonly [unknown], R = unknown>(
        values: T,
        res?: R,
    ): IPromise<
        {
            -readonly [P in keyof T]: PromiseSettledResult<
                T[P] extends PromiseLike<infer U> ? U : T[P]
            >;
        },
        GetPromiseMultiRes<T, R>
    >;
    raceR<T, R = GetPromiseRes<T>>(
        values: Iterable<T>,
        res?: R,
    ): IPromise<T extends PromiseLike<infer U> ? U : T, R>;
    rejectR<T, R = GetPromiseRes<T>>(reason?: T, res?: R): IPromise<T, R>;
    resolveR<T, R = GetPromiseRes<T>>(value: T, res?: R): IPromise<T, R>;
}
