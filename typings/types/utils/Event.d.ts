/**
 * @author Fa
 * @copyright Fa 2017
 */

type HandleID = string;
declare interface IEventListenInfo {
    target: EventTarget;
    id: HandleID;
    [n: string]: any;
}
declare interface IEventMap {
    [id: string]: IEventListenInfo;
}
