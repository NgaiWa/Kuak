/**
 * @author Fa
 * @copyright Fa 2017
 */

declare type SliceParams<
    Source extends any[],
    Splice extends any[],
    Result extends any[] = [],
> = Source extends [infer S, ...infer O]
    ? Result['length'] extends Splice['length']
        ? Source
        : SliceParams<O, Splice, [...Result, S]>
    : [];
declare type SliceParamsByIndex<
    Source extends any[],
    StartIndex extends number = 0,
    Result extends any[] = [],
> = Source extends [infer S, ...infer O]
    ? Result['length'] extends StartIndex
        ? Source
        : SliceParamsByIndex<O, StartIndex, [...Result, S]>
    : [];
