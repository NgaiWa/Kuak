/**
 * @author Fa
 * @copyright Fa 2017
 */
interface IStorageInfo {
    keys: string[];
    currentSize: number;
    limitSize: number;
}
declare interface IStorage extends EventTarget {
    get<R = false>(
        name: string,
        returnMap?: R,
    ): R extends true ? IResult<{ [n: string]: IResult }> : IResult;
    get<R = false>(
        names: string[],
        returnMap?: R,
    ): R extends true ? IResult<{ [n: string]: any }> : IResult<any[]>;
    set(name: string, value: any): IResult;
    set(data: AnyObject): IResult;
    remove(name: string): IResult;
    remove(names: string[]): IResult;
    clear(): IResult;
    getInfo(): IResult<IStorageInfo>;
    isSafeSize(inputSize?: number): boolean;
    asyncGet(name: string): Promise<any>;
    asyncGet(names: string[]): Promise<any>;
    getDecrypt(name: string): Promise<any>;
    getDecrypt(names: string[]): Promise<any>;
    asyncSet(name: string, value: any): Promise<any>;
    asyncSet(data: AnyObject): Promise<any>;
    setEncrypt(name: string, value: any): Promise<any>;
    setEncrypt(data: AnyObject): Promise<any>;
    asyncRemove(name: string): Promise<any>;
    asyncRemove(names: string[]): Promise<any>;
    asyncClear(): Promise<any>;
    asyncGetInfo(): Promise<IStorageInfo>;
    asyncIsSafeSize(inputSize?: number): Promise<boolean>;
    addEventListener(
        type: 'change',
        listener: ((event: IStorageEvent) => void) | EventListenerObject | null,
    ): string;
}
declare interface IStore<Data = AnyObject> extends EventTarget {
    readonly id: string;
    readonly data: Data;
    readonly options: {
        name?: string;
        storage?: IStorage;
        storageKeys?: string[];
        init?(store: IStore<Data>, storageDataRead: AnyObject): AnyObject;
        read?(store: IStore<Data>, storageDataRead: AnyObject): typeof storageDataRead;
        write?(store: IStore<Data>, storageDataToWritten: AnyObject): typeof storageDataToWritten;
    };
    reset(data: any): any;
    restore(): any;
    set(name: string, value: any): any;
    get(name: string | string[], returnMap?: boolean): any;
    set(data: AnyObject): any;
    addEventListener(
        type: 'change',
        listener: ((event: IStoreEvent) => void) | EventListenerObject | null,
    ): string;
}
declare interface IStoreConstructor {
    supportedEventTypes?: ['change'];
    new (options?: IStore['options']): IStore;
}

declare interface IStorageEvent extends Event {
    target: IStorage;
    detail: { data: any; keys: string[]; isSet?: boolean; isRemove?: boolean; isClear?: boolean };
}
declare interface IStoreEvent extends Event {
    target: IStore;
    detail: { data: AnyObject };
}
