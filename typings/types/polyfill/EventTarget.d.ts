/**
 * @author Fa
 * @copyright Fa 2017
 * @override Typescript declare EventTarget
 */

declare interface EventListener {
    (evt: Event): void;
}

interface EventListenerObject<T extends EventListener = EventListener> {
    handleEvent: T;
}
declare type EventListenerOrEventListenerObject<T extends EventListener = EventListener> =
    | T
    | EventListenerObject<T>;
declare type EventListenerParam<T extends EventListener = EventListener> =
    EventListenerOrEventListenerObject<T> | null;
declare type EventListenerOptionsParam<T extends EventListenerOptions = EventListenerOptions> =
    | T
    | boolean;
declare interface TriggerEventOptions extends EventInit {
    /**
     * 事件是否冒泡
     * @default false
     */
    bubbles?: boolean;
    /**
     * 事件是否可以穿越组件边界
     * 为false时，事件将只能在引用组件的节点树上触发，不进入其他任何组件内部
     * @default false
     */
    composed?: boolean;
    /**
     * 事件是否拥有捕获阶段
     * @default false
     */
    capturePhase?: boolean;
}

declare interface EventListenerOptions {
    capture?: boolean;
}
declare interface AddEventListenerOptions extends EventListenerOptions {
    once?: boolean;
    passive?: boolean;
    signal?: AbortSignal;
}
declare interface AbortSignalEventMap {
    abort: Event;
}
declare interface AbortSignal extends EventTarget {
    /**
     * Returns true if this AbortSignal's AbortController has signaled to abort, and false otherwise.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/AbortSignal/aborted)
     */
    readonly aborted: boolean;
    /** [MDN Reference](https://developer.mozilla.org/docs/Web/API/AbortSignal/abort_event) */
    onabort: ((this: AbortSignal, ev: Event) => any) | null;
    /** [MDN Reference](https://developer.mozilla.org/docs/Web/API/AbortSignal/reason) */
    readonly reason: any;
    /** [MDN Reference](https://developer.mozilla.org/docs/Web/API/AbortSignal/throwIfAborted) */
    throwIfAborted(): void;
    addEventListener<K extends keyof AbortSignalEventMap>(
        type: K,
        listener: (this: AbortSignal, ev: AbortSignalEventMap[K]) => any,
        options?: EventListenerOptionsParam<AddEventListenerOptions>,
    ): any;
    addEventListener(
        type: string,
        listener: EventListenerOrEventListenerObject,
        options?: EventListenerOptionsParam<AddEventListenerOptions>,
    ): any;
    removeEventListener<K extends keyof AbortSignalEventMap>(
        type: K,
        listener: (this: AbortSignal, ev: AbortSignalEventMap[K]) => any,
        options?: EventListenerOptionsParam,
    ): void;
    removeEventListener(
        type: string,
        listener: EventListenerOrEventListenerObject,
        options?: EventListenerOptionsParam,
    ): void;
}

declare interface EventTarget {
    /**
     * Appends an event listener for events whose type attribute value is type. The callback argument sets the callback that will be invoked when the event is dispatched.
     *
     * The options argument sets listener-specific options. For compatibility this can be a boolean, in which case the method behaves exactly as if the value was specified as options's capture.
     *
     * When set to true, options's capture prevents callback from being invoked when the event's eventPhase attribute value is BUBBLING_PHASE. When false (or not present), callback will not be invoked when event's eventPhase attribute value is CAPTURING_PHASE. Either way, callback will be invoked if event's eventPhase attribute value is AT_TARGET.
     *
     * When set to true, options's passive indicates that the callback will not cancel the event by invoking preventDefault(). This is used to enable performance optimizations described in § 2.8 Observing event listeners.
     *
     * When set to true, options's once indicates that the callback will only be invoked once after which the event listener will be removed.
     *
     * If an AbortSignal is passed for options's signal, then the event listener will be removed when signal is aborted.
     *
     * The event listener is appended to target's event listener list and is not appended if it has the same type, callback, and capture.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/EventTarget/addEventListener)
     */
    addEventListener(
        type: string,
        callback: EventListenerOrEventListenerObject | null,
        options?: EventListenerOptionsParam<AddEventListenerOptions>,
    ): typeof callback extends null ? void : string;
    /**
     * Dispatches a synthetic event event to target and returns true if either event's cancelable attribute value is false or its preventDefault() method was not invoked, and false otherwise.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/EventTarget/dispatchEvent)
     */
    dispatchEvent(event: Event): boolean;
    /**
     * Removes the event listener in target's event listener list with the same type, callback, and options.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/EventTarget/removeEventListener)
     */
    removeEventListener(
        type: string,
        callback: EventListenerOrEventListenerObject | null,
        options?: EventListenerOptionsParam,
    ): void;
    dispatchEvent(type: string, detail?: object, options?: TriggerEventOptions): boolean;
    /**
     *
     * @param type
     * @param capture capture 为 null 时 删除所有相应事件，其他与 removeEventListener 相同
     */
    removeEventListenerByType(type: string | string[], options?: EventListenerOptionsParam): void;
    /**
     *
     * @param type
     * @param capture capture 为 null 时 删除所有相应事件，其他与 removeEventListener 相同
     */
    removeEventListenerById(id: string | string[], options?: EventListenerOptionsParam): void;
    /**
     *
     * @param type
     * @param capture capture 为 null 时 删除所有相应事件，其他与 removeEventListener 相同
     */
    removeAllEventLListener(options?: EventListenerOptionsParam): void;
}

declare interface EventTargetConstructor {
    prototype: EventTarget;
    new (): EventTarget;
}
