/**
 * @author Fa
 * @copyright Fa 2017
 * @override Typescript declare CustomEvent
 */

declare interface CustomEvent<T = any> extends Event {
    /**
     * Returns any custom data event was created with. Typically used for synthetic events.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CustomEvent/detail)
     */
    readonly detail: T;
    /**
     * @deprecated
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CustomEvent/initCustomEvent)
     */
    initCustomEvent(type: string, bubbles?: boolean, cancelable?: boolean, detail?: T): void;
}
declare interface EventInit {
    bubbles?: boolean;
    cancelable?: boolean;
    composed?: boolean;
}
declare interface CustomEventInit<T = any> extends EventInit {
    detail?: T;
}
declare interface CustomEventConstructor {
    prototype: CustomEvent;
    new <T>(type: string, eventInitDict?: CustomEventInit<T>): CustomEvent<T>;
}
