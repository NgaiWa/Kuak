/**
 * @author Fa
 * @copyright Fa 2017
 */
declare interface IAppOption {
    key: string;
    globalData: {
        userInfo?: WechatMiniprogram.UserInfo;
    };
    userInfoReadyCallback?: WechatMiniprogram.GetUserInfoSuccessCallback;
    [n: string]: any;
}
