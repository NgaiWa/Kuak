/**
 * @author Fa
 * @copyright Fa 2017
 */
declare type IObject = AnyObject;
declare type AnyValue = any;
declare interface IConstructor<T = AnyObject> {
    new (...args: any): T;
    prototype: T;
}

declare type NewFunction = new <T = unknown>(...args: any) => T;
declare interface ObjectConstructor {
    hasOwn(o: object, k: PropertyKey): boolean;
    eq(o1: any, o2: any): boolean;
    same(o1: any, o2: any): boolean;
}
declare interface Array<T> {
    at(index: number): T;
}
declare interface ICallee {
    thisArg?: any;
    name?: string;
    func?: AnyFunction;
    args?: any[];
    invalid?: boolean;
    called?: boolean;
    value?: any;
    once?: boolean;
}
declare interface IResult<V = any, E = Error, S extends boolean = boolean> {
    state: S;
    value: V;
    error?: E;
}
type AliasName = string;
declare interface AliasNameMap {
    [name: string]: AliasName;
}

declare type ToValue = (value: any, key?: string | number, data?: any) => any;
declare type GetValue = (data: any, key: string) => any;
declare type SetAliasBy = (target: any, alias: string, source: string) => any;
declare type PickAliasMap = {
    [n: string]: string | SetAliasBy | { key: string; toValue: ToValue };
};

declare type TData<D> = AnyObject & D;
