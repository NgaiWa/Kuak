/*
 * Eslint config file
 * Documentation: https://eslint.org/docs/user-guide/configuring/
 * Install the Eslint extension before using this feature.
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const prettierrc = require('./.prettierrc');
module.exports = {
    env: {
        es6: true,
        browser: true,
        node: true,
    },
    // ecmaFeatures: {
    //     modules: true,
    // },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        parser: '@typescript-eslint/parser',
        ecmaVersion: 'latest',
        sourceType: 'module',
        allowImportExportEverywhere: true,
        ecmaFeatures: {
            modules: true,
        },
    },
    globals: {
        wx: true,
        App: true,
        Page: true,
        getCurrentPages: true,
        getApp: true,
        Component: true,
        requirePlugin: true,
        requireMiniProgram: true,
    },
    // extends: 'eslint:recommended',
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:prettier/recommended',
    ],
    plugins: ['@typescript-eslint', 'simple-import-sort'],
    rules: {
        // 该规则是可自动修复的 -- 如果你使用该标志运行，你的代码将根据样式进行格式化。eslint --fix prettier
        'prettier/prettier': [
            'error',
            {
                // 一个对象，表示将传递到 prettier 中的选项。
                // 此选项将合并并覆盖任何带有文件的配置集.prettierrc
                ...prettierrc,
            },
            {
                // usePrettierrc：启用加载 Prettier 配置文件（默认值：true）。如果您使用多个相互冲突的工具，或者不希望将 ESLint
                // 设置与 Prettier 配置混合使用，则可能很有用。
                // 此外，还可以通过 CLI 的 --no-config 选项或通过调用 prettier.format（） 通过 API 运行 prettier，
                // 而无需通过调用 resolveConfig 生成的选项来运行 prettier。
                usePrettierrc: false,

                // fileInfoOptions：传递给 prettier.getFileInfo 以决定是否需要格式化文件的选项。例如，可用于选择不忽略位于目录中的文件.node_modules
                // fileInfoOptions: {
                //     withNodeModules: true,
                // },
            },
        ],
        // '@typescript-eslint/ban-types': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-this-alias': 'off',
        '@typescript-eslint/triple-slash-reference': 'off',
        '@typescript-eslint/consistent-type-imports': 'error',
        '@typescript-eslint/ban-ts-comment': 'off',
        'simple-import-sort/imports': 'error',
        'simple-import-sort/exports': 'error',
        curly: 'error',
        'spaced-comment': [
            'error',
            'always',
            {
                line: {
                    markers: ['/'],
                    exceptions: ['-', '+'],
                },
                block: {
                    markers: ['!'],
                    exceptions: ['*'],
                    balanced: true,
                },
            },
        ],
    },
    overrides: [
        {
            // https://github.com/typescript-eslint eslint-recommended
            files: ['*.ts'],
            rules: {
                'constructor-super': 'off', // ts(2335) & ts(2377)
                'getter-return': 'off', // ts(2378)
                'no-const-assign': 'off', // ts(2588)
                'no-dupe-args': 'off', // ts(2300)
                'no-dupe-class-members': 'off', // ts(2393) & ts(2300)
                'no-dupe-keys': 'off', // ts(1117)
                'no-func-assign': 'off', // ts(2539)
                'no-import-assign': 'off', // ts(2539) & ts(2540)
                'no-new-symbol': 'off', // ts(2588)
                'no-obj-calls': 'off', // ts(2349)
                'no-redeclare': 'off', // ts(2451)
                'no-setter-return': 'off', // ts(2408)
                'no-this-before-super': 'off', // ts(2376)
                'no-undef': 'off', // ts(2304)
                'no-unreachable': 'off', // ts(7027)
                'no-unsafe-negation': 'off', // ts(2365) & ts(2360) & ts(2358)
                'no-var': 'error', // ts transpile let/const to var, so no need for vars any more
                'prefer-const': 'error', // ts provides better types with const
                'prefer-rest-params': 'error', // ts provides better types with rest args over arguments
                'prefer-spread': 'error', // ts transpile spread to apply, so no need for manual apply
                'valid-typeof': 'off', // ts(2367)
            },
        },
    ],
};
