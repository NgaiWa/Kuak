// eslint-disable-next-line @typescript-eslint/no-var-requires
const prettierrc = require('./.prettierrc.js');

module.exports = {
    defaultSeverity: 'error',
    ignoreFiles: ['*.min.css', ...`*.js *.jpg *.woff *.ts *.wxml`.split(/\s+/).filter(Boolean)],
    extends: [
        'stylelint-prettier/recommended',
        'stylelint-config-standard',
        // 'stylelint-config-standard-less',
        'stylelint-config-standard-scss',
        'stylelint-config-clean-order',
    ],
    // plugins: ['stylelint-less', 'stylelint-scss', 'stylelint-order'],
    plugins: ['stylelint-scss', 'stylelint-order'],
    rules: {
        'prettier/prettier': [true, { ...prettierrc, printWidth: 180 }],
        'no-duplicate-selectors': null,
        'block-no-empty': null,
        'selector-class-pattern': null,
        'declaration-block-no-redundant-longhand-properties': [
            true,
            { ignoreShorthands: ['/flex/'] },
        ],
        'custom-property-pattern': null,
        'keyframes-name-pattern': null,
        'no-empty-source': null,
        'font-family-no-missing-generic-family-keyword': [
            true,
            {
                ignoreFontFamilies: ['PingFangSC-Regular', 'PingFangSC-Medium', 'iconFont'],
            },
        ],
        'unit-no-unknown': [true, { ignoreUnits: ['rpx'] }],
        'function-url-quotes': null,
        'max-line-length': null,
        'at-rule-empty-line-before': ['always', { ignore: ['after-comment', 'first-nested'] }], // ["after-same-name", "inside-block", "blockless-after-same-name-blockless", "blockless-after-blockless", "first-nested"]
        'declaration-colon-newline-after': null,
        'declaration-block-no-duplicate-properties': null,
        'no-descending-specificity': null,
        'selector-type-no-unknown': null,
        'color-function-notation': 'legacy',
        'value-keyword-case': null,
        'property-no-unknown': [true, { checkPrefixed: true }],
        'import-notation': 'string',
        'selector-pseudo-class-no-unknown': [
            true,
            {
                ignorePseudoClasses: ['deep'],
            },
        ],
        'scss/double-slash-comment-whitespace-inside': null,
        'scss/dollar-variable-pattern': null,
        'scss/operator-no-newline-after': null,
    },
    overrides: [
        // {
        //     files: ['**/*.wxml'],
        //     customSyntax: 'postcss-html',
        // },
        {
            files: ['**/*.wxss'],
            customSyntax: 'postcss',
        },
        // {
        //     files: ['**/*.less'],
        //     customSyntax: 'postcss-less',
        // },
        {
            files: ['**/*.scss'],
            customSyntax: 'postcss-scss',
        },
    ],
};
